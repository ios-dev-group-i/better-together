# Better Together

Betteer Together is an iOS cooperative mobile game. <br>
This project is conducted from a part of the course **iOS Application Development** at RWTH Aachen. <br>

#### Demo video
![](./better-together-demo.mp4)

## Project meeting log
### Meeting 01 (12.01.2021)

1. Our own idea beyond the Heave Ho
    1. Considering interaction on mobile 
        - how to improve direct interaction on mobile (touch screen)
    2. More improvement controller for example using 
        - Gyroscope, gesture
        - Haptic
2. Multiplayer mode is the most important thing for our goal
    -  Try to build first multi player mode is working
3. Game Center -> P2P over connection
    - interaction in physics -> delay occurs because it is for more about turn taking mode
4. Open for using local network connection


### Meeting 02 (19.01.2021)
1. Problem: Occurence of delay between server and client device
    1. Use UDP -> .inreliable
    2. In client side: brings all cached data (backlog)
    3. (Optional) Prediction: Send only position and velocity data, and interpolate the timestamp gap between the packets
        - “assume” what will happen physically
    4. (Optional) calculate same thing from the server, client and mixing them based on ping
    5. wifi only? bluetooth only? or both?
    6. Maybe convert data to bytes array 

### Meeting 03 (26.01.2021)
1. Grab interaction
    1. force other players to grab if one player sends a grab action
2. Think about level design
3. Surprising feature: onboarding
    - adding a customization feature for character (color or accessary)

### Attribution Statements
1. Level 1 background image <br>
<a href="https://www.freepik.com/vectors/watercolor">Watercolor vector created by pikisuperstar - www.freepik.com</a>

2. Level 2 background image <br>
<a href="https://www.freepik.com/vectors/background">Background vector created by katemangostar - www.freepik.com</a>

3. TutorialScene flag icon <br>
<a href="https://www.clipartmax.com/middle/m2i8A0K9N4i8K9b1_racing-flag-sports-icons-png-checkered-flag-icon-png/" target="_blank">Racing Flag Sports Icons Png - Checkered Flag Icon Png @clipartmax.com</a>
