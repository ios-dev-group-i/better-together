//
//  RoundedRectangleButton.swift
//  Better Together
//
//  Cumtom button with rounded rectangle style
//
//  Created by Eunae Jang on 2021/01/02.
//

import UIKit

@IBDesignable
class RoundedRectangleButton: UIButton {

    // Set corner radius
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
}
