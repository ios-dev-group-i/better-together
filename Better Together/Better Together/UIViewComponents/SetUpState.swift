//
//  SetUpState.swift
//  Better Together
//
//  Created by Lukas Woyke on 06.02.21.
//

import Foundation

enum SetUpState: String {
    case waiting = "Waiting for other players ..."
    case receivingConfigs = "Receiving customizations..."
    
    case sendingLevels = "Received all customizations! Sending level... "
    case receivingLevels = "Receiving level confimation... "
    
    case sendingPlayerInfo = "Levels confimred! Sync player info... "
    case receivingPlayerInfo = "Receiving player info confirmation... "
        
    case setUpComplete = "Set up complete!"
}
