//
//  LevelCollectionViewCell.swift
//  Better Together
//
//  Created by Eunae Jang on 2021/02/05.
//

import UIKit

class LevelCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var levelButton: RoundedRectangleButton!
}
