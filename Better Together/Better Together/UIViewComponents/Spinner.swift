//
//  Spinner.swift
//  Better Together
//
//  Created by Eunae Jang on 2021/02/05.
//

import UIKit

class Spinner: UIView {

    static var currentSpinnerView: UIView?
    
    /// show loading spinner
    static func show(onView: UIView) -> UIView {
        
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        let activityIndicatorView = UIActivityIndicatorView(style: .large)
        activityIndicatorView.color = UIColor.white
        activityIndicatorView.startAnimating()
        activityIndicatorView.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(activityIndicatorView)
            onView.addSubview(spinnerView)
        }
        
        currentSpinnerView = spinnerView
        return spinnerView
    }
    
    /// remove loading spinner
    static func dismiss() {
        DispatchQueue.main.async {
            self.currentSpinnerView?.removeFromSuperview()
            self.currentSpinnerView = nil
        }
    }

}
