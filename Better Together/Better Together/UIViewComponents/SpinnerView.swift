//
//  SpinnerView.swift
//  Better Together
//
//  Created by Lukas Woyke on 06.02.21.
//

import UIKit

/// SpinnerView is the waiting room whihc is presented on server side, while host is waiting for the other players.
class SpinnerView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    var maxProgress = 1 {
        didSet { maxProgress = (maxProgress == 0) ? 1 : maxProgress }
    }
    var currentState: SetUpState =  .waiting

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }
    
    func setUp() {
        Bundle.main.loadNibNamed("SpinnerView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
    }
    
    
    /// Start the anomation, by entering the number of maximum set up setps.
    /// - Parameter maxProgress: The maximum steps until set up pocess is complete.
    func start(maxProgress: Int) {
        activityIndicator.startAnimating()
        DispatchQueue.main.async {
            self.progressBar.setProgress(0, animated: false)
        }
        self.maxProgress = maxProgress
    }
    
    func updateProgress(progressCount: Int) {
        let actualProgress: Float  = Float(progressCount / maxProgress)
        let percentage = actualProgress * 100
        DispatchQueue.main.async { [self] in
            self.infoLabel.text = "\(currentState.rawValue) \(percentage)%"
            self.progressBar.setProgress(actualProgress, animated: true)
        }
    }
     
    func dismissView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, animations: {
                self.alpha = 0
            })
        }
    }
}
