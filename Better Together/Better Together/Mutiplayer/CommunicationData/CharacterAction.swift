//
//  GameAction.swift
//  Better Together
//
//  Created by Eunae Jang on 2021/01/25.
//

import Foundation
import UIKit

/// Define character's actions related to game play
enum CharacterAction {
    case physics(CharacterPhysics)
    case grabInfo(GrabInfo)
    case reachesGoal(Bool)
    case dropped(Bool)
    
    enum Keys: CodingKey {
        case physics
        case grabInfo
        case reachesGoal
        case dropped
    }
}

extension CharacterAction: Codable {
    enum CharacterActionTypeError: Error {
        case decoding(String)
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: Keys.self)
        if let physicsInfo = try? values.decode(CharacterPhysics.self, forKey: .physics) {
            self = .physics(physicsInfo)
        } else if let grabInfo = try? values.decode(GrabInfo.self, forKey: .grabInfo) {
            self = .grabInfo(grabInfo)
        } else if let reachesGoal = try? values.decode(Bool.self, forKey: .reachesGoal) {
            self =  .reachesGoal(reachesGoal)
        } else if let dropped = try? values.decode(Bool.self, forKey: .dropped) {
            self = .dropped(dropped)
        } else {
            throw CharacterActionTypeError.decoding("CharacterActionTypeError: \(values)")
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Keys.self)
        switch self {
        case .physics(let physicsInfo):
            try container.encode(physicsInfo, forKey: .physics)
        case .grabInfo(let grabInfo):
            try container.encode(grabInfo, forKey: .grabInfo)
        case .reachesGoal(let reachesGoal):
            try container.encode(reachesGoal, forKey: .reachesGoal)
        case .dropped(let dropped):
            try container.encode(dropped, forKey: .dropped)
        }
    }
}

/// Define character physics info
struct CharacterPhysics: Codable {
    var bodyPositionX: CGFloat
    var bodyPositionY: CGFloat
    var leftUpperArmPositionX : CGFloat
    var leftUpperArmPositionY : CGFloat
    var leftLowerArmPositionX : CGFloat
    var leftLowerArmPositionY : CGFloat
    var leftHandPositionX: CGFloat
    var leftHandPositionY: CGFloat
    var rightUpperArmPositionX : CGFloat
    var rightUpperArmPositionY : CGFloat
    var rightLowerArmPositionX : CGFloat
    var rightLowerArmPositionY : CGFloat
    var rightHandPositionX: CGFloat
    var rightHandPositionY: CGFloat
    var vector : CGVector
    var playerID : Int
}

/// Define characters' grab action info
struct GrabInfo: Codable {
    var grabHand: Character.BodyPart
    var grabPosition : CGPoint?
    var touchStatus: Character.TouchStatus
    var makeSound : Bool
    var soundMakeCharacterIndex : Int?
    var fromClient : Bool
    var grabbedObstacle : Bool
    var grabbedCharacter : Bool
    var forChangingHandAppearance : Bool
}
