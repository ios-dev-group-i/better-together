//
//  CodableHelper.swift
//  Better Together
//
//  Created by Eunae Jang on 2021/01/25.
//

import Foundation
import UIKit

/// Custom color struct to make color data codable
struct BodyColor: Codable {
    var red: CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0
    var alpha: CGFloat = 0.0
    var uiColor: UIColor {
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    init(color: UIColor) {
        color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
    }
}
