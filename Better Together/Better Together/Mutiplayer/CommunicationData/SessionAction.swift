//
//  GameAction.swift
//  Better Together
//
//  Created by Eunae Jang on 2021/01/25.
//

import Foundation

/// Define all actions that are communicated via session
enum SessionAction {
    case setup(SetupAction)
    case gamePhysics(CharacterActionData)
    case syncAllCharacters(SyncData)    // only used by Server
    
    enum Key: CodingKey {
        case setup
        case gamePhysics
        case syncAllCharacters
    }
}

extension SessionAction: Codable {
    
    enum SessionActionTypeError: Error {
        case decoding(String)
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: Key.self)
        if let setup = try? values.decode(SetupAction.self, forKey: .setup) {
            self = .setup(setup)
        } else if let gamePhysics = try? values.decode(CharacterActionData.self, forKey: .gamePhysics) {
            self = .gamePhysics(gamePhysics)
        } else if let syncAllCharacters = try? values.decode(SyncData.self, forKey: .syncAllCharacters) {
            self = .syncAllCharacters(syncAllCharacters)
        } else {
            throw SessionActionTypeError.decoding("SessionActionTypeError: \(values)")
        }
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        switch self {
        case .gamePhysics(let gamePhysics):
            try container.encode(gamePhysics, forKey: .gamePhysics)
        case .setup(let connectionSetup):
            try container.encode(connectionSetup, forKey: .setup)
        case .syncAllCharacters(let syncPhysics):
            try container.encode(syncPhysics, forKey: .syncAllCharacters)
        }
    }
}

/// All actions for settings before game starts
enum SetupAction {
    case clientsConfiguration([String]) // Client sends is configuration to the host.
    case selectedLevel(Int)
    case responseForLevelSetting(Bool)
    case syncCharacterIndexForGameScene([CodablePlayersInfo])
    case responseForCharacterInfoSyncDone(Bool)
    case startGame(Bool)
    case gameSceneLoaded(Bool)
    case gameSuccess(Bool)
    case dismissedInvitation(Bool)
    case gameOver(Bool)
    
    enum Key: CodingKey {
        case selectedLevel
        case responseForLevelSetting
        case configuration
        case syncCharacterIndexForGameScene
        case responseForCharacterInfoSyncDone
        case startGame
        case gameSceneLoaded
        case gameSuccess
        case dismissedInvitation
        case gameOver
    }
}

extension SetupAction: Codable {
    
    enum SetupActionTypeError: Error {
        case decoding(String)
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: Key.self)
        if let selectedLevel = try? values.decode(Int.self, forKey: .selectedLevel) {
            self = .selectedLevel(selectedLevel)
        } else if let responseForLevelSetting = try? values.decode(Bool.self, forKey: .responseForLevelSetting) {
            self = .responseForLevelSetting(responseForLevelSetting)
        } else if let syncCharacterIndexForGameScene = try? values.decode([CodablePlayersInfo].self, forKey: .syncCharacterIndexForGameScene) {
            self = .syncCharacterIndexForGameScene(syncCharacterIndexForGameScene)
        } else if let responseForCharacterInfoSyncDone = try? values.decode(Bool.self, forKey: .responseForCharacterInfoSyncDone) {
            self = .responseForCharacterInfoSyncDone(responseForCharacterInfoSyncDone)
        } else if let startGame = try? values.decode(Bool.self, forKey: .startGame) {
            self = .startGame(startGame)
        } else if let gameSceneLoaded = try? values.decode(Bool.self, forKey: .gameSceneLoaded) {
            self = .gameSceneLoaded(gameSceneLoaded)
        } else if let gameSuccess = try? values.decode(Bool.self, forKey: .gameSuccess) {
            self = .gameSuccess(gameSuccess)
        } else if let gameOver = try? values.decode(Bool.self, forKey: .gameOver) {
            self = .gameOver(gameOver)
        } else if let configuration = try? values.decode([String].self, forKey: .configuration){
            self = .clientsConfiguration(configuration)
        } else if let receiviedDismissed = try? values.decode(Bool.self, forKey: .dismissedInvitation){
            self = .dismissedInvitation(receiviedDismissed)
        } else {
            throw SetupActionTypeError.decoding("SetupActionTypeError: \(values)")
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        switch self {
        case .selectedLevel(let level):
            try container.encode(level, forKey: .selectedLevel)
        case .responseForLevelSetting(let response):
            try container.encode(response, forKey: .responseForLevelSetting)
        case .syncCharacterIndexForGameScene(let syncCharacterIndexForGameScene):
            try container.encode(syncCharacterIndexForGameScene, forKey: .syncCharacterIndexForGameScene)
        case .responseForCharacterInfoSyncDone(let response):
            try container.encode(response, forKey: .responseForCharacterInfoSyncDone)
        case .startGame(let startGame):
            try container.encode(startGame, forKey: .startGame)
        case .gameSceneLoaded(let gameSceneLoaded):
            try container.encode(gameSceneLoaded, forKey: .gameSceneLoaded)
        case .gameSuccess(let gameSuccess):
            try container.encode(gameSuccess, forKey: .gameSuccess)
        case .gameOver(let gameOver):
            try container.encode(gameOver, forKey: .gameOver)
        case .clientsConfiguration(let config):
            try container.encode(config, forKey: .configuration)
        case .dismissedInvitation(let dismissed):
            try container.encode(dismissed, forKey: .dismissedInvitation)
        }
    }
}
