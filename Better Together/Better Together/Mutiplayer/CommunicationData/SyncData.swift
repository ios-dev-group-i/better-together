//
//  SyncData.swift
//  Better Together
//
//  Created by Eunae Jang on 2021/01/25.
//

import Foundation

typealias CharacterIndex = Int

/// Define sync data that has list of characterActionData
class SyncData: Codable {
    var data: [CharacterActionData]
    
    init() {
        self.data = []
    }
    
    init(syncData: [CharacterActionData]) {
        self.data = syncData
    }
}

/// Define data for action of a specific character
class CharacterActionData: Codable {
    var characterIndex: CharacterIndex
    var actions: [CharacterAction] = []
    
    init(characterIndex: CharacterIndex) {
        self.characterIndex = characterIndex
    }
}
