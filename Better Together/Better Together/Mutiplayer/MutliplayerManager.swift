//
//  MultiplayerManager.swift
//  Better Together
//
//  Created by Lukas Woyke on 02.01.21.
//
// MARK: TODO: Remove this file?

import UIKit
import GameKit

// How to set up MultipayerManager:
// 1. Adopt class to Multiplayer delegate protocol.
// 2. Set the MultiplayerManager.sharedinstance.viewController = self
// 3. Let player authenticate himself in gamecenter MultiplayerManager.sharedinstance.authenticatePlayer()
// 4. Find a match with MultiplayerManager.sharedinstance.findMatch(for number: Int) with paramter number of players.
// 5. After a match was found one player can start the game by sending the "started" game state.

protocol MultiplayerDelegate: class {
    func authenticationChanged(isAutherised: Bool)
    func playerPositionChanged(to info: BTPlayerInfo, for player: GKPlayer)
    func gameStateChanged(to state: GameState, for player: GKPlayer)
}

enum GameState: Int, CaseIterable {
    case started    // One Player started the game, now waiting for others. (Game automatically then starts after recieving started from all 4 players, and game shows waiting screen until then.)
    case paused     // One Player paused the game.
    case finished   // One Player detected that the team finished the game.
    case exit       // One Player exit the game or lost connection.
}
// ******************* Example for state life cycle  *******************
//
//     ┌────────────────────────┐
//     │ User is authenticated  │
//     │   and ready to play    │
//     └────────────────────────┘
//                  │
//                  ├───────────────────────┐
//                  │User presses start and │
//                  │   sends Game State    │
//                  ├───────────────────────┘
//                  │
//                  ▼
//      ┌────────────────────────┐
//      │     Waiting screen     │
//      └───────────┬────────────┘
//                  ├──────────────────────┐
//                  │  Received "started"  │
//                  │    from all other    │
//                  │       players.       │
//                  ├──────────────────────┘
//                  │
//                  ▼
//      ┌────────────────────────┐
//      │   Game automatically   │
//      │         starts         │
//      └────────────────────────┘
//                  │
//                  ├──────────────────────┐
//                  │ Received "finished"  │
//                  │   from one player.   │
//                  ├──────────────────────┘
//                  │
//                  │
//                  ▼
//      ┌────────────────────────┐
//      │  Show winner or loser  │
//      │        screen.         │
//      └────────────────────────┘
//
// **************************************************************************

class MultiplayerManager: NSObject, GKMatchmakerViewControllerDelegate, GKMatchDelegate {
    
    // MARK: - Proprties
    static let sharedInstance =  MultiplayerManager()
    var viewController: UIViewController?
    var delegate: MultiplayerDelegate?
    
    // The actual match.
    private var match: GKMatch?
    
    var isAuthenticated: Bool {
      return GKLocalPlayer.local.isAuthenticated
    }
    
    var connectionState: ConnectionState = .none
    enum ConnectionState {
        case none, authorized, matchFound, gameStarted
    }
    
    override init() {
        super.init()
    }
    
    
    // MARK: - Player Authentication
    func authenticatePlayer() {
        GKLocalPlayer.local.authenticateHandler = { gameCenterAuthVC, error in
            
            if GKLocalPlayer.local.isAuthenticated {
                // Notify the view Controller.
                self.delegate?.authenticationChanged(isAutherised: GKLocalPlayer.local.isAuthenticated)
                NotificationCenter.default.post(name: .authenticationChanged, object: GKLocalPlayer.local.isAuthenticated)
                print("Authenticated to Game Center!")
            } else  if let authenticationVC = gameCenterAuthVC {
                // Present the view controller so the player can sign in
                self.viewController?.present(authenticationVC, animated: true)
                return
            }
            
            if error != nil {
                // Player could not be authenticated
                // Disable Game Center in the game
                print("Error authentication to GameCenter: " +
                  "\(error?.localizedDescription ?? "none")")
                
                if let _error = error as? GKError {
                    if _error.code == GKError.Code.notAuthenticated {
                        // TODO: Show pop up with link to settings
                    }
                }
                return
            }
                
            // Player was successfully authenticated
            // Check if there are any player restrictions before starting the game

            if GKLocalPlayer.local.isMultiplayerGamingRestricted {
                // Disable multiplayer game features
            }
        }
    }
    
    // MARK: - Match Finding
    
    func findMatch() {
        showMatchmaker()
    }
    
    /// Show the match maker game center view controller to find a match.
    /// - Parameter numberOfPlayers: Number of players
    private func showMatchmaker() {
        guard GKLocalPlayer.local.isAuthenticated else { return }
        
        let matchRequest = GKMatchRequest()
        matchRequest.minPlayers = 2
        matchRequest.maxPlayers = 4
        matchRequest.inviteMessage = "Would you like to play the awesome game Better Together?"
        
        guard let matchVC = GKMatchmakerViewController(matchRequest: matchRequest) else { return }
        matchVC.matchmakerDelegate = self
        viewController?.present(matchVC, animated: true)
    }

    
    // MARK: - Match Maker Delegate
    func matchmakerViewControllerWasCancelled(_ viewController: GKMatchmakerViewController) {
        viewController.dismiss(animated: true)
    }
    
    func matchmakerViewController(_ viewController: GKMatchmakerViewController, didFailWithError error: Error) {
        print("Matchmaker did fail with error: \(error.localizedDescription).")
        viewController.dismiss(animated: true, completion: {
            // Notify the user.
            let alert = UIAlertController(title: "Ups!", message: "Could not find a match!", preferredStyle: .alert)
            self.viewController?.present(alert, animated: true, completion: nil)
        })
    }
    
    func matchmakerViewController(_ viewController: GKMatchmakerViewController, didFind match: GKMatch) {
        viewController.dismiss(animated: true, completion: nil)
        self.match = match
        match.delegate = self
        NotificationCenter.default.post(name: .gameFound, object: nil)
        print("Ready to start the game.")
    }
    
    // MARK: - GKMatch Delegate
    // Exchange data.
    // Maximum size of a message. For saving in 4 UIInt64, the positions x and y of the arms left and right.
    // Storing the game state in a UInt32. Assuming that a Bool needs UInt32 to store.
    private static var dataCheckSize: Int = (64 * 4 + 32 + 32 * 2)
    func match(_ match: GKMatch, didReceive data: Data, fromRemotePlayer player: GKPlayer) {
        // Check length.
        guard data.count <= MultiplayerManager.dataCheckSize else {
            print("Error: Invalid message size!")
            return
        }
        
        // Decode the message.
        let decoder = JSONDecoder()
        do {
            let info = try decoder.decode(BTPlayerInfo.self, from: data)

            if info.gameState < 4,
               let newState = GameState(rawValue: Int(info.gameState)) {
                delegate?.gameStateChanged(to: newState, for: player)
            }
            
            delegate?.playerPositionChanged(to: info, for: player)
        } catch {
            print(error)
        }
    }
    
    // Maybe later used for retrieveing specific data, but until now
    // func match(_ match: GKMatch, didReceive data: Data, forRecipient recipient: GKPlayer, fromRemotePlayer player: GKPlayer) { }
    
    
    // MARK: - Events
    func gameStateChanged(to state: GameState, error completion: ((Error) -> Void)?) {
        let encoder = JSONEncoder()
        let info = BTPlayerInfo()
        do {
            let data = try encoder.encode(info)
            try match?.sendData(toAllPlayers: data, with: .unreliable)
        } catch {
            print(error)
            guard completion != nil else { return }
            completion!(error)
        }
    }
    
    func playerMoved(to position: CGPoint) {
        
    }
}

// Communication to view controller using notfication and observer. For more see https://developer.apple.com/documentation/swift/cocoa_design_patterns
extension Notification.Name {
  static let gameFound = Notification.Name(rawValue: "gameFound")
    static let authenticationChanged = Notification.Name(rawValue: "authenticationChanged")
}
