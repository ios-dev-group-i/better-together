//
//  BTPlayerInfo.swift
//  Better Together
//
//  Created by Lukas Woyke on 05.01.21.
//
// MARK: TODO: Remove this file??

import Foundation



/// This class contains information about player movement into
class BTPlayerInfo: Codable, NSSecureCoding {
    
    static var supportsSecureCoding: Bool {
        return true }
    
    // The new position of the player.
    var xRightPosition: Int64 = 0
    var yRightPosition: Int64 = 0
    var xLeftPosition: Int64 = 0
    var yLeftPosition: Int64 = 0
    var leftIsAttached: Bool = true
    var rightIsAttached: Bool = true
    
    // Has value between 0 adn 3 depending on current game state.
    var gameState: Int32 = 0
    
    enum CodingKeys: String, CodingKey {
        case xRightPosition = "xRightPosition"
        case yRightPosition = "yRightPosition"
        case xLeftPosition = "xLeftPosition"
        case yLeftPosition = "yLeftPosition"
        case leftIsAttached = "leftIsAttached"
        case rightIsAttached = "rightIsAttached"
        case gameState = "gameState"
    }
    
    init() {
    }
    
    required init?(coder: NSCoder) {
        xRightPosition = coder.decodeInt64(forKey: "xRightPosition")
        yRightPosition = coder.decodeInt64(forKey: "yRightPosition")
        xLeftPosition = coder.decodeInt64(forKey: "xLeftPosition")
        yLeftPosition = coder.decodeInt64(forKey: "yLeftPosition")
        leftIsAttached = coder.decodeBool(forKey: "leftIsAttached")
        rightIsAttached = coder.decodeBool(forKey: "rightIsAttached")
        gameState = coder.decodeInt32(forKey: "gameState")
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(xRightPosition, forKey: "xRightPosition")
        coder.encode(yRightPosition, forKey: "yRightPosition")
        coder.encode(xLeftPosition, forKey: "xLeftPosition")
        coder.encode(xLeftPosition, forKey: "xLeftPosition")
        coder.encode(leftIsAttached, forKey: "leftIsAttached")
        coder.encode(leftIsAttached, forKey: "leftIsAttached")
        coder.encode(gameState, forKey: "gameState")
    }
    
    
}
