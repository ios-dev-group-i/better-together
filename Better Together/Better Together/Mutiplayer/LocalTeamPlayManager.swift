//
//  LocalTeamPlayManager.swift
//  Better Together
//
//  Created by Lukas Woyke on 12.01.21.
//

import UIKit
import SpriteKit
import MultipeerConnectivity
import Network

protocol LocalTeamPlayDelegate {
    func receivedNewCustomization(config : [String], for player: MCPeerID)
    func syncCharacterIndexForGameScene(playersInfo: [CodablePlayersInfo])
    func playerSyncDoneReceived(for peerID: MCPeerID, isDone: Bool)
    func playersGameSceneLoadStateReceived(for player: MCPeerID, isReady: Bool)
    func allChracterActionsReceived(charactersActions: [CharacterActionData])
    func characterActionReceived(characterActionData: CharacterActionData)
    func receivedLevelConfirmation(for player: MCPeerID)
}

class LocalTeamPlayManager: NSObject, MCNearbyServiceAdvertiserDelegate, MCBrowserViewControllerDelegate {
    
    // MARK: - Properties
    static let sharedInstance = LocalTeamPlayManager()
    var delegate: LocalTeamPlayDelegate? = nil
    var viewController: UIViewController? = nil
    private var session: MCSession? = nil
    
    private var characterIndex: CharacterIndex = 0      // use for making codablePlayersInfo from server side
    var myself: Player
    var host: Player?
    var playersInfo: [PlayersInfo] = []
    var codablePlayersInfo: [CodablePlayersInfo] = []
    var isConnected = true
    
    private let service = "bettertogether"
    private var nearbyServiceAdvertiser: MCNearbyServiceAdvertiser?  = nil
    
    var allPlayersPeerIDs: [MCPeerID]?{
        return session?.connectedPeers
    }
    
    // MARK: - Inizilisation
    override init() {
        self.myself = UserDefaults.standard.mySelf
        super.init()
        session = MCSession(peer: myself.peerID, securityIdentity: nil, encryptionPreference: .required)
        session?.delegate = self
        setUpAvailableForInvitations()
    }
    
    // MARK: - handle receiving data
    // call delegate depends on session action type
    func receive(data: Data, from peerID: MCPeerID) {
        let decoder = JSONDecoder()
        do {
            let action = try decoder.decode(SessionAction.self, from: data)
            switch action {
            case .setup(.selectedLevel(let level)):
                NotificationCenter.default.post(name: .levelSelectedFromHost, object: nil, userInfo: ["level": level])
            case .setup(.responseForLevelSetting( _)):
                self.delegate?.receivedLevelConfirmation(for: peerID)
            case .setup(.syncCharacterIndexForGameScene(let playersInfo)):
                self.delegate?.syncCharacterIndexForGameScene(playersInfo: playersInfo)
            case .setup(.responseForCharacterInfoSyncDone(let done)):
                self.delegate?.playerSyncDoneReceived(for: peerID, isDone: done)
            case .setup(.startGame(let start)):
                NotificationCenter.default.post(name: .startGame, object: nil, userInfo: ["playerInfoSync": start])
            case .setup(.gameOver(_)):
                NotificationCenter.default.post(name: .gameResultReceived, object: nil, userInfo: ["result": "fail"])
            case .setup(.gameSceneLoaded(let isReady)):
                self.delegate?.playersGameSceneLoadStateReceived(for: peerID, isReady: isReady)
            case .setup(.gameSuccess(_)):
                NotificationCenter.default.post(name: .gameResultReceived, object: nil, userInfo: ["result": "success"])
            case .gamePhysics(let characterActionData):
                delegate?.characterActionReceived(characterActionData: characterActionData)
            case .syncAllCharacters(let syncData):
                delegate?.allChracterActionsReceived(charactersActions: syncData.data)
            case .setup(.clientsConfiguration(let config)):
                    let index = self.allPlayersPeerIDs!.firstIndex(of: peerID)!
                CharacterManager.sharedInstance.addNewConfigurationServer(config: config, for: peerID, for: index)
                    self.delegate?.receivedNewCustomization(config: config, for: peerID)
            case .setup(.dismissedInvitation(let value)):
                NotificationCenter.default.post(name: .dimissedInvitationBrowserClient , object: nil, userInfo: ["value": value])
            }
        } catch {
            debugPrint("receive - decoding error: \(error)")
        } 
    }
    
    // MARK: - Receiving Invitations
    private func setUpAvailableForInvitations() {
        nearbyServiceAdvertiser = MCNearbyServiceAdvertiser(peer: myself.peerID,
            discoveryInfo: nil, serviceType: service)
        nearbyServiceAdvertiser?.delegate = self
        makeAvailableForInvitations()
    }
    
    func stopAvailablilityForInvitaions() {
        nearbyServiceAdvertiser?.stopAdvertisingPeer()
    }
    func makeAvailableForInvitations() {
        nearbyServiceAdvertiser?.startAdvertisingPeer()
    }
    
    func leaveSession() {
        self.session?.disconnect()
        // Reset all game sessino dependend values
        self.delegate = nil
        self.viewController = nil
        characterIndex = 0
        playersInfo = []
        codablePlayersInfo = []
    }
    
    // - MCNearby Servie Delegate
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        // Use context for data to access received data with the invitation.
        // For exaple exchanging custom player name or game map data.
        // guard let context = context else { return }
        
        // Show an alert to
        let title = "Game Invitation from \(peerID.displayName)"
        let message = "\(peerID.displayName) invites you to a new better together session!"
        let invitationAlert = UIAlertController(title: title, message: message,
          preferredStyle: .alert)
        
        let declineAction = UIAlertAction( title: "Cancel",style: .cancel) { _ in invitationHandler(false, nil)}
        let acceptAction = UIAlertAction( title: "Accept",style: .default) { _ in
            invitationHandler(true, self.session)
            NotificationCenter.default.post(name: .acceptedInvitation, object: nil)
        }
        
        invitationAlert.addAction(declineAction)
        invitationAlert.addAction(acceptAction)
        viewController?.present(invitationAlert, animated: true)
        
        //set this device to client, since it was invited from server
        //MARK: maybe there is a better place to set it as a client
        host = Player(peerID: peerID)   // save host information
        GameScene.isServer = false
        GameScene.multiplayerMode = true
    }
    
    // MARK: - Make Invitation
    func showInvitationSelection() {
        guard session != nil else {
            print("Error: Could not show invitation browser, because session is not instaiatet!")
            return
        }
        stopAvailablilityForInvitaions()
        let browserVC = MCBrowserViewController(serviceType: service, session: session!)
        browserVC.delegate = self
        browserVC.maximumNumberOfPeers = 4
        browserVC.minimumNumberOfPeers = 1
        viewController?.present(browserVC, animated: true, completion: nil)
        
        GameScene.isServer = true
        // add self as host into playersInfo
        addPlayersInfo(index: characterIndex, player: self.myself)       // Host is always index 0
        GameScene.multiplayerMode = true
    }
    
    /// sync selected level from host
    func sendSelectedLevel() {
        sendToAll(sessionAction: SessionAction.setup(SetupAction.selectedLevel(LevelSelectionViewController.selectedLevel)), isReliable: true)
    }
    
    /// maps all connected peers into unique character index for GameScene
    func setClientPlayers() {
        allPlayersPeerIDs?.forEach{
            characterIndex += 1
            addPlayersInfo(index: characterIndex , player: Player(peerID: $0))
        }
        print("After addPlayersInfo: ", playersInfo)
        syncCharacterIndex()
    }
    
    /// sync characterIndex for all players so that they can find their own character index for game scene
    func syncCharacterIndex() {
        var codablePlayersInfo: [CodablePlayersInfo] = []
        for info in playersInfo {
            codablePlayersInfo.append(
                CodablePlayersInfo(
                    characterIndex: info.characterIndex,
                    displayName: info.player.displayName,
                    characterBodyColor: info.player.bodyColor!,
                    otherPlayersConfig: CharacterManager.sharedInstance.getConfigurationForServer()))}
        self.codablePlayersInfo = codablePlayersInfo
        print("After making sync character info: ", codablePlayersInfo)
        sendToAll(sessionAction: SessionAction.setup(SetupAction.syncCharacterIndexForGameScene(codablePlayersInfo)), isReliable: true)
    }
    
    /// add  player to playersInfo to manage all player with unique characterIndex
    func addPlayersInfo(index: CharacterIndex, player: Player) {
        self.playersInfo.append(PlayersInfo(characterIndex: index, player: player))
    }
    
    // MARK: - MC Browser Delegate
    // Is used for inviting other players.
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        // Show the game scene and wait until other players are ready.
        NotificationCenter.default.post(name: .dimissedInvitationBrowser, object: nil)
        browserViewController.dismiss(animated: true, completion: nil)
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        // Disconnect from all peers.
        leaveSession()
        browserViewController.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Sending data
    /// send SessionAction to peers
    func sendToAll(sessionAction: SessionAction, isReliable : Bool) {
        guard session != nil else { return }
        guard isConnected else { return }
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(sessionAction)
            try session?.send(data, toPeers: allPlayersPeerIDs!, with: isReliable ? .reliable : .unreliable)
        } catch {
            debugPrint("ERROR: SessionAction - \(sessionAction) :send to all - \(error)")
        }
    }
    
    func sendToHost(sessionAction: SessionAction, isReliable : Bool) {
        guard session != nil else { return }
        guard isConnected else { return }
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(sessionAction)
            try session?.send(data, toPeers: [host!.peerID], with: isReliable ? .reliable : .unreliable)
        } catch {
            debugPrint("ERROR: SessionAction - \(sessionAction) :send to Host - \(error)")
        }
    }
    
    func sendCharacterReady(with configuration: [String]?){
        guard session?.connectedPeers != nil else { return }
        guard session != nil else { return }
        guard configuration != nil else {
            debugPrint("Could not send my configuration to server, bacuse configuration is nil!")
            return
        }
        let encoder = JSONEncoder()
        let info = SessionAction.setup(SetupAction.clientsConfiguration(configuration!))
        do {
            let data = try encoder.encode(info)
            try session?.send(data, toPeers: [host!.peerID], with: .reliable)
        } catch {
            print("Sending configuration could not be completed!")
            print(error)
        }
    }
}

extension LocalTeamPlayManager: MCSessionDelegate {
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        debugPrint("receive stream \(streamName) from \(peerID).")
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        debugPrint("start to receive resource \(resourceName) from \(peerID).")
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        debugPrint("finish to receive resource \(resourceName) from \(peerID).")
    }
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .connected:
            return
        case .connecting:
            return
        case .notConnected:
            NotificationCenter.default.post(name: .gameResultReceived, object: nil, userInfo: ["result": "disconnected"])
        @unknown default:
            return
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        receive(data: data, from: peerID)
    }
}

// Communication to view controller using notfication and observer. For more see https://developer.apple.com/documentation/swift/cocoa_design_patterns
extension Notification.Name {
    static let dimissedInvitationBrowser = Notification.Name(rawValue: "dismissedInvitation")
    static let acceptedInvitation = Notification.Name(rawValue: "acceptedInvitation")
    static let levelSelectedFromHost = Notification.Name(rawValue: "levelSelectedFromHost")
    static let startGame = Notification.Name(rawValue: "startGame")
    static let gameResultReceived = Notification.Name(rawValue: "gameResultReceived")
    static let dimissedInvitationBrowserClient = Notification.Name(rawValue: "dismissedInvitationClient")
}


/* ************************************************** Setup process in multiplayer mode ************************************************** */
//
//
//      ┌────────────────────────────┐                            ┌────────────────────────────┐
//      │           Server           │                            │           Client           │
//      └────────────────────────────┘                            └────────────────────────────┘
//                     │                  ┌────────────────────┐                 │
//                     │                  │  Send Invitation   │                 │
//                     ├──────────────────┴────────────────────┴────────────────▶│
//                     │                  ┌────────────────────┐                 │
//                     │                  │ Accept invitation  │                 │
//                     │◀─────────────────┴────────────────────┴─────────────────┼─
//      ┌──────────────┤                                                         │
//      │ Wait for all │                                                         │
//      │   clients    │              ┌─────────────────────────────┐            │
//      └──────────────┤              │  send Invitation completed  │            │
//                     ├──────────────┴─────────────────────────────┴───────────▶│
//                     │              ┌────────────────────────────┐             │
//                     │              │ Send clients customization │             │
//                     │◀─────────────┴────────────────────────────┴─────────────┤
//                     │              ┌────────────────────────────┐             │
//                     │              │    Send selected level     │             │
//                     ├──────────────┴────────────────────────────┴────────────▶│
//                     │              ┌────────────────────────────┐             │
//                     │              │       Confirm level        │             │
//                     │◀─────────────┴────────────────────────────┴─────────────│
//      ┌──────────────┤                                                         │
//      │ Wait for all │                                                         │
//      │   clients    │              ┌────────────────────────────┐             │
//      └──────────────┤              │      Send player info      │             │
//                     ├──────────────┴────────────────────────────┴────────────▶│
//                     │              ┌────────────────────────────┐             │
//                     │              │        Confirm info        │             │
//                     │◀─────────────┴────────────────────────────┴─────────────┤
//      ┌──────────────┤                                                         │
//      │ Wait for all │                                                         │
//      │   clients    │              ┌────────────────────────────┐             │
//      └──────────────┤              │      Send start game       │             │
//                     ├──────────────┴────────────────────────────┴────────────▶│
//                     │              ┌────────────────────────────┐             │
//                     │              │   Send game scene loaded   │             │
//                     │◀─────────────┴────────────────────────────┴─────────────┤
//      ┌──────────────┤                                                         │
//      │ Wait for all │                                                         │
//      │   clients    │                                                         │
//      └──────────────┤              ┌────────────────────────────┐             │
//                     │              │     Send game started      │             │
//                     ├──────────────┴────────────────────────────┴────────────▶│
//                     │                                                         │
//                     │                                                         │
//                     │                                                         │
//                     │                                                         │
//                     │                                                         │
//                     │                                                         │
//                     │                                                         │
//                     ▼                                                         ▼
//
//
/* ********************************************************************************************************************************** */
