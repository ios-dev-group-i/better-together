//
//  LocalBTPlayerInfo.swift
//  Better Together
//
//  Created by Lukas Woyke on 12.01.21.
//

import Foundation
import MultipeerConnectivity
import UIKit

struct PlayersInfo {
    var characterIndex: CharacterIndex
    var player: Player
}

/// This is for sending minimal playersInfo as codable instance
struct CodablePlayersInfo: Codable {
    var characterIndex: CharacterIndex
    var displayName: String
    var characterBodyColor: BodyColor
    var otherPlayersConfig: [[String]]
}

struct Player {
    var peerID: MCPeerID
    var displayName: String {
        get {
            return peerID.displayName
        }
    }
    var isHost: Bool = false
    var bodyColor: BodyColor?
    
    init(peerID: MCPeerID, bodyColor: BodyColor? = BodyColor(color: UIColor.random)) {
        self.peerID = peerID
        self.bodyColor = bodyColor
    }
    
    init(userName: String, bodyColor: BodyColor? = BodyColor(color: UIColor.random)) {
        self.peerID = MCPeerID(displayName: userName)
        self.bodyColor = bodyColor
    }
}

extension Player: Hashable {
    static func == (lhs: Player, rhs: Player) -> Bool {
        return lhs.peerID == rhs.peerID
    }
    
    func hash(into hasher: inout Hasher) {
        peerID.hash(into: &hasher)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}

extension UserDefaults {
    /// save mySelf and its property bodyColor
    var mySelf: Player {
        get {
            if let data = UserDefaults.standard.data(forKey: "bodyColor"),
               let bodyColor = try? JSONDecoder().decode(BodyColor.self, from: data) {
                return Player(userName: UIDevice.current.name, bodyColor: bodyColor)
            }
            let player = Player(userName: UIDevice.current.name)
            let newData = try? JSONEncoder().encode(player.bodyColor)
            set(newData, forKey: "bodyColor")
            return player
        }
        set {
            let data = try? JSONEncoder().encode(newValue.bodyColor)
            set(data, forKey: "bodyColor")
        }
    }
}
