//
//  HapticResponseController.swift
//  Better Together
//
//  Created by Lukas Woyke on 26.01.21.
//

import UIKit
import CoreHaptics

class HapticResponseController {
    // Feedback for grabbing.
     static func characterCustomizationExeeded() {
        let generator = UIImpactFeedbackGenerator(style: .rigid)
        generator.impactOccurred()
     }
    
    static func grabSucceedFeedback() {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
    }    
}
