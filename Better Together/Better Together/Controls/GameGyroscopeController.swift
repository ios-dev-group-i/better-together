//
//  GameGyroscopeController.swift
//  Better Together
//
//  Created by Lukas Woyke on 24.01.21.
//

import UIKit
import CoreMotion
import simd

/// Converts data from the gyroscpe and converts into direction movement
class GameGyroscopeController {
    
    // MARK: - Properties
    static var sharedInstance:GameGyroscopeController = GameGyroscopeController()
    
    private var motionManager = CMMotionManager()
    private var lastMovement = CGVector(dx: 0, dy: 0)
    
    // For using Motion data
    private var resetOrientationActive = true
    
    // Maybe use them later to adjust the
    private var tolerance: Double {
        return 0.1 * maxMovementTolerance
    }
    private let maxMovementTolerance: Double  = 100 //206
    
    init() {}
    
    func getControllerLikeVector() -> CGVector {
        guard let accData = motionManager.accelerometerData else { return CGVector(dx: 0, dy: 0) }
        let acceleration: SIMD3<Double> = SIMD3<Double>([accData.acceleration.x,
                                                     accData.acceleration.y,
                                                     accData.acceleration.z])
        var accVector = self.calculateAccVector(newAcc: acceleration)
        if (abs(accVector.dx) > 600 && abs(accVector.dx) < 1000) {
            let sign: FloatingPointSign = accVector.dx.sign
            accVector.dx = sign == FloatingPointSign.minus ? -400 : 400
        } else {
            accVector.dx = 0
        }
        if (abs(accVector.dy) > 600 && abs(accVector.dy) < 1000) {
            let sign: FloatingPointSign = accVector.dx.sign
            accVector.dy = sign == FloatingPointSign.minus ? -400 : 400
        } else {
            accVector.dy = 0
        }
        lastMovement = accVector
        return lastMovement
    }
    
    func getlatestRotation() -> CGVector {
        
        
        guard let accData = motionManager.accelerometerData else { return CGVector(dx: 0, dy: 0) }
        let acceleration: SIMD3<Double> = SIMD3<Double>([accData.acceleration.x,
                                                     accData.acceleration.y,
                                                     accData.acceleration.z])
        let accVector = self.calculateAccVector(newAcc: acceleration)
        lastMovement = accVector
        return lastMovement
    }
    
    func start() {
        startAcc()
    }
    func stop() {
        stopAcc()
    }
    
    /// Restarts the
    func resetOrientation() {
        resetOrientationActive = true
    }
    
    // MARK: - Private Helper Methods
    
    // Accelerometer
    /// Start reading from the acceleration sensor
    private func startAcc() {
        if motionManager.isAccelerometerAvailable {
            
            let updateInterval: Float = 0.000001
            motionManager.accelerometerUpdateInterval = TimeInterval(updateInterval)
            motionManager.startAccelerometerUpdates()
            
        }
    }
    
    /// Stop reading from the acceleration sensor
    private func stopAcc() {
        guard motionManager.isAccelerometerAvailable else { return }
        motionManager.stopAccelerometerUpdates()
    }
    
    // Because the game can be played in different oirentations, the movement has to be calculatetd relative to the starting position.
    private var initialZ: Double = 0.0
    private var initialY: Double = 0.0
    private var initialX: Double = 0.0
    private var initalOrientation: UIDeviceOrientation?  = nil
    private var initialTimeStamp: Date? = nil
    
    /// Caclulates the vector for controlling the character, by using the input
    private func calculateAccVector(newAcc: SIMD3<Double>) -> CGVector {
        
        // Sets the initial position, with the first reading attempt.
        if resetOrientationActive {
            initialZ = newAcc.z
            initialX = newAcc.x
            initialY = newAcc.y
            
            resetOrientationActive = false
        }
        
        var dx = -(newAcc.y - initialY)
        var dy = newAcc.x - initialX
        
        // Adjust movement when user olds device in LandscapeRight orientation.
        if newAcc.x > 0{
            dx *= (-1)
            dy *= (-1)
        }
        
        // Adjust this value, to adjust the speed of physical character in the gamescene.
        dx *= 600
        dy *= 1000
        
        let vector = CGVector(dx: dx, dy: dy)
        return vector
    }
    
    // If user holds device longer then 10 seconds in another oreintation reset the inial values.
    private func checkReorientation() {
        if initialTimeStamp == nil || initalOrientation == nil {
            initialTimeStamp = Date()
            initalOrientation = UIDevice.current.orientation
        }
        
        if initalOrientation != UIDevice.current.orientation {
            let interval = Date().timeIntervalSince(initialTimeStamp!)
            if interval > 8 {
                resetOrientation()
                initalOrientation = UIDevice.current.orientation
            }
        }  else {
           initialTimeStamp = Date()
        }
    }
    
}
