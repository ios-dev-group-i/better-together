//
//  CharacterManager.swift
//  Better Together
//
//  Created by Lukas Woyke on 04.02.21.
//

import UIKit
import MultipeerConnectivity
import SpriteKit

/// Character Manager manages the preperation of customizations for loading, saving and sending.
class CharacterManager {
    
    private static let bodies = ["BTBodiesImages.001","BTBodiesImages.002","BTBodiesImages.003","BTBodiesImages.004","BTBodiesImages.005", "BTBodiesImages.006", "BTBodiesImages.007"]
    private static let arms = ["BTArmsImages.001","BTArmsImages.002","BTArmsImages.003","BTArmsImages.004","BTArmsImages.005","BTArmsImages.006", "BTArmsImages.007"]
    private static let hands = ["BTHandsImages.001","BTHandsImages.002","BTHandsImages.003","BTHandsImages.004","BTHandsImages.005","BTHandsImages.006"]
    private static let grabHands = ["BTHandsImagesClosed.001","BTHandsImagesClosed.002","BTHandsImagesClosed.003","BTHandsImagesClosed.004","BTHandsImagesClosed.005","BTHandsImagesClosed.006"]
    private static let rightHands =  ["BTHandsRightImages.001","BTHandsRightImages.002","BTHandsRightImages.003","BTHandsRightImages.004","BTHandsRightImages.005","BTHandsRightImages.006"]
    private static let rightGrabHands =  ["BTHandsRightImagesClosed.001","BTHandsRightImagesClosed.002","BTHandsRightImagesClosed.003","BTHandsRightImagesClosed.004","BTHandsRightImagesClosed.005","BTHandsRightImagesClosed.006"]
    private static let flippedArms = ["BTArmsImagesFlipped.002","BTArmsImagesFlipped.003","BTArmsImagesFlipped.004"]
    
    private static let nintyDegrees: Float = 1.5708
    static let sharedInstance: CharacterManager = CharacterManager()
    
    // Contains the configuration of all other players, without the servers one.
    // Use  getConfigurationForServer() to get all players configurations including the servers ones.
    // Used on server side (PeerID, Configuration, Index of Config Arrival)
    var allPlayersConfiguration =  [(MCPeerID, [String], Int)]()
    
    // used on client side, to save the configuration of all players.
    var allOtherConfigurations = [[String]]()
    
    // the preloaded textures, both client and server use the same array to store textures.
    var preloadedTextures = [SKTexture]()
    
    
    // MARK: - Save and load configuration of my own character
    private let handKey = "handKey"
    private let armKey = "ArmKey"
    private let bodyKey = "bodyKey"
    func saveHand(with imageName: String) {
        UserDefaults.standard.setValue(imageName, forKey: handKey)
    }
    func saveArm(with imageName: String) {
        UserDefaults.standard.setValue(imageName, forKey: armKey)
    }
    func saveBody(with imageName: String) {
        UserDefaults.standard.setValue(imageName, forKey: bodyKey)
    }
    
    func saveAll(bodyName: String, armName: String, handName: String) {
        saveBody(with: bodyName)
        saveArm(with: armName)
        saveHand(with: handName)
    }
    
    
    // MARK: - Preperation of data for saving, loading and sending.
    func getMyConfigText() -> [String]? {
        guard let body = UserDefaults.standard.value(forKey: bodyKey) as? String  else { return nil }
        guard let arm = UserDefaults.standard.value(forKey: armKey) as? String else { return nil }
        guard let hand = UserDefaults.standard.value(forKey: handKey) as? String  else { return nil }
        return [body, arm, hand]
    }
    
    
    /// Return the image index of my configuration images.
    func getMyConfigAsIndex() -> [Int]? {
        guard let body = UserDefaults.standard.value(forKey: bodyKey) as? String  else { return nil }
        guard let arm = UserDefaults.standard.value(forKey: armKey) as? String else { return nil }
        guard let hand = UserDefaults.standard.value(forKey: handKey) as? String  else { return nil }
        if let bodyIndex = Int(body.suffix(1)),
           let armIndex = Int(arm.suffix(1)),
           let handIndex = Int(hand.suffix(1)) {
            return [bodyIndex, armIndex, handIndex]
        }
        return nil
    }
    
    private static let numberOfBodyParts = 9
    /// Returns the images for a complete customization (whihc consist of 9 Strings, one image name for each body part)
    /// - Parameter customizationKey: The image names of a customization
    /// - Returns: The translated images
    static func getCustomization(from  customizationKey: [String]?) -> [UIImage]? {
        guard  customizationKey != nil else { return nil }
        guard  customizationKey!.count == numberOfBodyParts else { return nil }
        
        let _imageName7 =  customizationKey![7].replacingOccurrences(of: "BTHandsImages", with: "BTHandsImagesClosed")
        let _imageName8 =  customizationKey![8].replacingOccurrences(of: "BTHandsImages", with: "BTHandsRightImagesClosed")
        let _imageName6 =  customizationKey![6].replacingOccurrences(of: "BTHandsImages", with: "BTHandsRightImages")
        
        if let Body = UIImage(named:  customizationKey![0]),
           let LeftUpperArm = UIImage(named:  customizationKey![1]),
           var LeftLowerArm = UIImage(named:  customizationKey![2]),
           let LeftHand = UIImage(named:  customizationKey![3]),
           let RightUpperArm = UIImage(named:  customizationKey![4])?.rotate(radians: nintyDegrees * 2),
           var RightLowerArm = UIImage(named:  customizationKey![5])?.rotate(radians: nintyDegrees * 2),
           let RightHand = UIImage(named: _imageName6),
           let LeftHandClosed = UIImage(named: _imageName7),
           let RightHandClosed = UIImage(named: _imageName8) {
            
            if  customizationKey![1] == arms[2] {
                LeftLowerArm = UIImage(named: arms[6])!
                RightLowerArm = UIImage(named: arms[6])!.rotate(radians: nintyDegrees * 2)!
            }
            return [Body, LeftUpperArm, LeftLowerArm, LeftHand, RightUpperArm, RightLowerArm, RightHand, LeftHandClosed, RightHandClosed]
        }
        return nil
    }
    
    private static let numberOfSendImageNames = 3
    // In multiplayer mode we only exchange the name of 3 images (body,arm,hand) beacuse of perfomance reasons.
    // This function reconstructs the howl cusomozation (which includes images for 9 body parts) out of the 3 image names.
    /// This function reconstructs the howl cusomozation (which includes images for 9 body parts) out of the 3 image names.
    /// - Parameter config: the 3 images names received
    /// - Returns: The 9 image names for each body part.
    static func getCompleteCustomization(for config: [String]?) -> [String]? {
        guard config != nil else { return nil }
        guard config!.count == numberOfSendImageNames else { return nil }
        let Body = config![0]
        let LeftUpperArm = config![1]
        let LeftLowerArm = config![1]
        let LeftHand = config![2]
        let RightUpperArm = config![1]
        let RightLowerArm = config![1]
        let RightHand = config![2]
        let LeftHandClosed = config![2]
        let RightHandClosed = config![2]
        return [Body, LeftUpperArm, LeftLowerArm, LeftHand, RightUpperArm, RightLowerArm, RightHand, LeftHandClosed, RightHandClosed]
    }
    
    /// Constructs the howl customization of images from a basic configuration, which contains the 3 (body,arm,hands) image names.
    static func getImages(for config: [String]?) -> [UIImage]? {
        return getCustomization(from: getCompleteCustomization(for: config))
    }
    
    /// Preload the textures for host side. Stored in preloadedTextures.
    func preloadConfigurationTexturesServer(completion: @escaping () -> Void) {
        let allConfigs = getConfigurationForServer()
        for config in allConfigs { // Iterate through all characters configurations.
            guard let allTextureNames = CharacterManager.getCompleteCustomization(for:  config) else { return }
            for i in 0 ..< allTextureNames.count {
                var textureName = allTextureNames[i] // Image name for one bodypart texture.
                switch i {
                case 4:
                    if let index = textureName.last?.wholeNumberValue, [2,3].contains(index) {
                        textureName = CharacterManager.flippedArms[index - 2]
                    }
                case 5:
                    if let index = textureName.last?.wholeNumberValue, [2,3].contains(index) {
                        textureName = CharacterManager.flippedArms[index - 2]
                    }
                case 6: textureName = textureName.replacingOccurrences(of: "BTHandsImages", with: "BTHandsRightImages")
                case 7: textureName = textureName.replacingOccurrences(of: "BTHandsImages", with: "BTHandsImagesClosed")
                case 8: textureName = textureName.replacingOccurrences(of: "BTHandsImages", with: "BTHandsRightImagesClosed")
                default: break
                }
                // Flip arms when using the bodybuilding texture.
                if allTextureNames[2] == CharacterManager.arms[2] && i == 2 {
                    textureName = CharacterManager.arms[6]
                } else if allTextureNames[2] == CharacterManager.arms[2] && i == 5 {
                    textureName = CharacterManager.flippedArms[2]
                }
                preloadedTextures.append(SKTexture(imageNamed: textureName))
            }
        }
        SKTexture.preload(preloadedTextures, withCompletionHandler: completion)
    }
    
    /// Preload the textures for client side. Stored in preloadedTextures.
    func preloadConfigurationTextureClient(completion: @escaping () -> Void) {
        for config in allOtherConfigurations {
            guard let completeConfiguration = CharacterManager.getCompleteCustomization(for: config) else { return }
            for i in 0 ..< completeConfiguration.count {
                var name = completeConfiguration[i]
                switch i {
                case 4:
                    if let index = name.last?.wholeNumberValue, [2,3].contains(index) {
                        name = CharacterManager.flippedArms[index - 2]
                    }
                case 5:
                    if let index = name.last?.wholeNumberValue, [2,3].contains(index) {
                        name = CharacterManager.flippedArms[index - 2]
                    }
                case 6: name = name.replacingOccurrences(of: "BTHandsImages", with: "BTHandsRightImages")
                case 7: name = name.replacingOccurrences(of: "BTHandsImages", with: "BTHandsImagesClosed")
                case 8: name = name.replacingOccurrences(of: "BTHandsImages", with: "BTHandsRightImagesClosed")
                default: break
                }
                if completeConfiguration[2] == CharacterManager.arms[2] && i == 2 {
                    name = CharacterManager.arms[6]
                } else if completeConfiguration[2] == CharacterManager.arms[2] && i == 5 {
                    name = CharacterManager.flippedArms[2]
                }
                preloadedTextures.append(SKTexture(imageNamed: name))
            }
        }
        SKTexture.preload(preloadedTextures, withCompletionHandler: completion)
    }
    
    
    /// Adds a newly received configuration of remote players.
    /// - Parameters:
    ///   - config: the configuration of the remote player
    ///   - player: the player MCPeerID
    ///   - index: the index for order arrival in which the player accepted the invitation.
    func addNewConfigurationServer(config: [String], for player: MCPeerID, for index: Int) {
        guard config.count == 3 else { return }
        allPlayersConfiguration.append((player, config, index + 1))
    }
    
    // Gives server character configurations of remote players in correct order, including his own configuratoin.
    func getConfigurationForServer() -> [[String]] {
        var allConfigurations = [(MCPeerID,[String],Int)].init(repeating: (MCPeerID(displayName: "Dummy"),[String](), 0), count: allPlayersConfiguration.count + 1) // +1 because of server himself, which gets added in setp 2.
        
        // 1. Reorder the elements
        for config in allPlayersConfiguration {
            allConfigurations[config.2] = config
        }
        
        // 2 .First insert my own (servers) configuration.
        guard let myConfig = self.getMyConfigText() else { return [[String]]() }
        let myID = LocalTeamPlayManager.sharedInstance.myself.peerID
        allConfigurations[0] = (myID, myConfig,0)
        
        return allConfigurations.map({ $0.1 })
    }
    
    func resetConfigarations() {
        allPlayersConfiguration = []
        allOtherConfigurations = []
        preloadedTextures = []
    }
}
