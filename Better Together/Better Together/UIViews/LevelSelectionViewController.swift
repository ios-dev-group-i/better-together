//
//  LevelSelectionViewController.swift
//  Better Together
//
//  Created by Lukas Woyke on 11.01.21.
//

import UIKit

class LevelSelectionViewController: UIViewController {

    var gamePlayMode: PlayMode? = nil
    var mySelf: Player?
    
    @IBOutlet weak var playerNameDisplay: UIBarButtonItem!
    @IBOutlet weak var levelCollectionView: UICollectionView!
    
    let levels = [1, 2]
    var cellScale: CGFloat = 0.6
    static var selectedLevel = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light

        // Remove back button text
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.tintColor = UIColor(red: 0.44, green: 0.50, blue: 0.59, alpha: 1.00)
        
        // Load mySelf information
        self.mySelf = UserDefaults.standard.mySelf
        playerNameDisplay.title = self.mySelf?.displayName
        
        // set level selection carousel view
        let screenSize = UIScreen.main.bounds.size
        let cellWidth = floor(screenSize.width * cellScale)
        let cellHeight = floor(screenSize.height * cellScale)
        let insetX: CGFloat = (view.bounds.width - cellWidth) * 0.2
        let insetY: CGFloat = (view.bounds.height - cellHeight) * 0.2
        let layout = levelCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        levelCollectionView.contentInset = UIEdgeInsets(top: insetY, left: insetX, bottom: insetY, right: insetX)
        levelCollectionView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showGame" {
            guard let gameVC = segue.destination as? GameViewController else { return }
            gameVC.gamePlayMode = gamePlayMode
        }
    }

    
    // MARK: - Action
    @IBAction func levelOneSelected(_ sender: UIButton) {
        selectedLevel(with: 1)
    }
    
    func selectedLevel(with number: Int) {
        switch gamePlayMode {
        case .singlePlay:
            GameScene.multiplayerMode = false
            self.performSegue(withIdentifier: "showGame", sender: self)
        case .teamPlay:
            MultiplayerManager.sharedInstance.viewController = self
            MultiplayerManager.sharedInstance.findMatch()
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.foundMatch),
                                                   name: .gameFound,
                                                   object: nil)
        case .teamPlayLocalHost:
            // Get notified when the invition screen is dismissed.
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.dismissedInvitation(_:)),
                                                   name: .dimissedInvitationBrowser,
                                                   object: nil)
            // Show the invitation screen after selecting a level.
            LocalTeamPlayManager.sharedInstance.viewController = self
            LocalTeamPlayManager.sharedInstance.showInvitationSelection()
        default:
            self.performSegue(withIdentifier: "showGame", sender: self)
            return
        }
    }
    
    // MARK: - Game Center Team Play
    @objc func foundMatch(_ notifictaion: Notification) {
        LocalTeamPlayManager.sharedInstance.stopAvailablilityForInvitaions()
        self.performSegue(withIdentifier: "showGame", sender: self)
    }
    
    // MARK: - Multipeer Connectivity Team Play
    @objc func dismissedInvitation(_ notifictaion: Notification) {
        LocalTeamPlayManager.sharedInstance.stopAvailablilityForInvitaions()
        // MARK: TODO: remove this part?
        /*
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let gameVC = storyboard.instantiateViewController(withIdentifier: "gameViewControler") as? GameViewController else { return }
        gameVC.gamePlayMode = self.gamePlayMode
        gameVC.modalPresentationStyle = .fullScreen
        DispatchQueue.main.async {
            self.present(gameVC, animated: true, completion: nil)
        }
         */
         self.performSegue(withIdentifier: "showGame", sender: self)
    }
}

/// CollectionViewDataSourceDelegation
extension LevelSelectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return levels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReusableCollectionCell", for: indexPath) as! LevelCollectionViewCell
        let level = levels[indexPath.item]
        cell.levelButton.setTitle("Level \(level)", for: .normal)
        cell.levelButton.setBackgroundImage(UIImage(named: "level\(level)"), for: .normal)
        cell.levelButton.tag = level
        cell.levelButton.addTarget(self, action: #selector(levelSelected), for: .touchUpInside)
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    @IBAction func levelSelected(_ sender: UIButton) {
        LevelSelectionViewController.selectedLevel = sender.tag
        selectedLevel(with: sender.tag)
    }
}
