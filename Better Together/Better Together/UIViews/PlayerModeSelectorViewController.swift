//
//  PlayerModeSelectorViewController.swift
//  Better Together
//
//  Created by Eunae Jang on 2021/01/02.
//

import UIKit
import GameKit

class PlayerModeSelectorViewController: UIViewController {

    var gamePlayMode: PlayMode? = nil
    @IBOutlet weak var fakeLaunchScreen: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light
        
        // Remove back button text
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.tintColor = UIColor(red: 0.44, green: 0.50, blue: 0.59, alpha: 1.00)
        navigationController?.navigationBar.isHidden = false
        dismissLaunchScreen()
        
        // Set up multipeer team play and make available for invitations.
        LocalTeamPlayManager.sharedInstance.viewController = self
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.acceptedInvitation(_:)),
                                               name: .acceptedInvitation,
                                               object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // When returning to the playmode view controller (for e.g. after a game), show myself again for invitations.
        LocalTeamPlayManager.sharedInstance.makeAvailableForInvitations()
        LocalTeamPlayManager.sharedInstance.viewController = self
        navigationController?.navigationBar.isHidden = false
        CharacterManager.sharedInstance.resetConfigarations()
    }
    

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "tutorialMode":
            guard let gameViewController = segue.destination as? GameViewController else { return }
            gameViewController.gamePlayMode = .tutorial
            LocalTeamPlayManager.sharedInstance.stopAvailablilityForInvitaions()
        case "showCharacterSelection":
            guard let characterViewController = segue.destination as? CharacterControlSettingViewController else { return }
            characterViewController.gamePlayMode = self.gamePlayMode
            LocalTeamPlayManager.sharedInstance.stopAvailablilityForInvitaions()
        case "showGameFromPlayerMode":
            guard let gameViewController = segue.destination as? GameViewController else { return }
            gameViewController.gamePlayMode = self.gamePlayMode
            LocalTeamPlayManager.sharedInstance.stopAvailablilityForInvitaions()
        default:
            return
        }
    }
    
    
    @IBAction func singePlaySelected(_ sender: UIButton) {
        gamePlayMode = .singlePlay
        showMultiplayerGameSelection()
    }
    
    @IBAction func teamModeSelected(_ sender: UIButton) {
        self.gamePlayMode = .teamPlayLocalHost
        showMultiplayerGameSelection()
    }
    
    // Let the player return to menu when exiting the game.
    @IBAction func unwind( _ seg: UIStoryboardSegue) {
        DispatchQueue.main.async {
            LocalTeamPlayManager.sharedInstance.leaveSession()
        }
    }
    
    private func dismissLaunchScreen() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
                self.fakeLaunchScreen.alpha = 0
            })
        }
    }
    
    // MARK: - Game Center Team Play
    private func showAuthenticationViaGameCenter() {
        if MultiplayerManager.sharedInstance.isAuthenticated {
            self.showMultiplayerGameSelection()
        } else {
            NotificationCenter.default.addObserver(
                self, selector: #selector(self.authenticationChanged(_:)),
                name: .authenticationChanged, object: nil)
            MultiplayerManager.sharedInstance.viewController = self
            MultiplayerManager.sharedInstance.authenticatePlayer()
        }
    }
    
    private func showMultiplayerGameSelection() {
        self.performSegue(withIdentifier: "showCharacterSelection", sender: self)
    }
    
    @objc private func authenticationChanged(_ notification: Notification)  {
        if MultiplayerManager.sharedInstance.isAuthenticated {
            showMultiplayerGameSelection()
        }
    }
    
    // MARK: - Multipeer Connection Team Play
    @objc private func acceptedInvitation(_ notification: Notification)  {
        gamePlayMode = .teamPlayLocalJoin
        LocalTeamPlayManager.sharedInstance.stopAvailablilityForInvitaions()
        DispatchQueue.main.async {
            // If client accepts the invitation, navigate to Character Customization.
            self.performSegue(withIdentifier: "showCharacterSelection", sender: self)
        }
    }
    
}
