//
//  CharacterControlSettingViewController.swift
//  Better Together
//
//  Created by Eunae Jang on 2021/01/02.
//

import UIKit
import MultipeerConnectivity

class CharacterControlSettingViewController: UIViewController {

    // MARK: - Properties
    var mySelf: Player?
    var gamePlayMode: PlayMode? = nil
    
    @IBOutlet weak var backgroundSelectorView: UIView!
    @IBOutlet weak var playerNameDisplay: UIBarButtonItem!
    @IBOutlet weak var instructionLabel: UILabel!
    private var clientSpinnerView: UIView?
    private var goingForwards = false
    
    // ImageViews for CHaracter
    @IBOutlet weak var bodyImageView: UIImageView!
    @IBOutlet weak var leftHandImageView: UIImageView!
    @IBOutlet weak var leftArmImageView: UIImageView!
    @IBOutlet weak var rightArmImageView: UIImageView!
    @IBOutlet weak var rightHandImageView: UIImageView!
    
    private var instructionsShown = false
    
    private let bodies = ["BTBodiesImages.001","BTBodiesImages.002","BTBodiesImages.003","BTBodiesImages.004","BTBodiesImages.005","BTBodiesImages.006", "BTBodiesImages.007"]
    private let arms = ["BTArmsImages.001","BTArmsImages.002","BTArmsImages.003","BTArmsImages.004","BTArmsImages.005","BTArmsImages.006", "BTArmsImages.007"]
    private let hands = ["BTHandsImages.001","BTHandsImages.002","BTHandsImages.003","BTHandsImages.004","BTHandsImages.005","BTHandsImages.006"]
    
    private var editingPart: Character.BodyPart? = nil
    private var bodyCounter = 0 {
        didSet {
            bodyImageView.image = UIImage(named: bodies[bodyCounter])
        }
    }
    private var armCounter = 0 {
        didSet {
            leftArmImageView.image = UIImage(named: arms[armCounter])
            var rightArm = UIImage(named: arms[armCounter])
            rightArm = rightArm?.withHorizontallyFlippedOrientation()
            rightArmImageView.image = rightArm
        }
    }
    private var handCounter = 0 {
        didSet {
            leftHandImageView.image = UIImage(named: hands[handCounter])?.rotate(radians: -1.5708)
            rightHandImageView.image = UIImage(named: hands[handCounter])?.rotate(radians: 1.5708)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light
        
        // Remove back button text
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.tintColor = UIColor(red: 0.44, green: 0.50, blue: 0.59, alpha: 1.00)
        
        navigationItem.title = "Design Your Character"
        let textAttributes = [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.2310673594, green: 0.2809102535, blue: 0.3539133668, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        self.mySelf = UserDefaults.standard.mySelf
        playerNameDisplay.title = self.mySelf?.displayName
        
        if gamePlayMode! == .teamPlayLocalJoin {
            NotificationCenter.default.addObserver(self, selector: #selector(self.levelSelectedAndNavigateGameViewController(_:)),name: .levelSelectedFromHost, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.receivedServerDismissedBrowser(_:)),name: .dimissedInvitationBrowserClient, object: nil)
            clientSpinnerView = Spinner.show(onView: self.view)
        }
        
        setUp()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        loadMyConfig()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        // Check if the user exits the character selection and goes back to the menu.
        if !goingForwards {
            if gamePlayMode! == .teamPlayLocalJoin {
                // MARK: TODO - what is this?
               // LocalTeamPlayManager.sharedInstance.leaveSession()
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        goingForwards = true
        if segue.identifier == "showLevelSelection" {
            guard let destVC = segue.destination as? LevelSelectionViewController else { return }
            destVC.gamePlayMode = gamePlayMode
        } else if segue.identifier == "showGameFromCharacter" {
            guard let destVC = segue.destination as? GameViewController else { return }
            destVC.gamePlayMode = gamePlayMode
        }
    }
    
    @IBAction func continueTapped(_ sender: UIButton) {
        CharacterManager.sharedInstance.saveAll(bodyName: bodies[bodyCounter], armName: arms[armCounter], handName: hands[handCounter])
        if gamePlayMode == .teamPlayLocalJoin {
            // In case player only joined a session.
            // Tell the other peers that my player is ready and send my customization.
            LocalTeamPlayManager.sharedInstance.sendCharacterReady(with: CharacterManager.sharedInstance.getMyConfigText())
            _ = Spinner.show(onView: self.view)
        } else {
            self.performSegue(withIdentifier: "showLevelSelection", sender: self)
        }
    }
    
    @objc private func levelSelectedAndNavigateGameViewController(_ notification: Notification) {
        DispatchQueue.main.async {
        LevelSelectionViewController.selectedLevel = notification.userInfo?["level"] as! Int
        Spinner.dismiss()
        self.performSegue(withIdentifier: "showGameFromCharacter", sender: self)
        }
    }
    
    // MARK: - Action
    @objc func downSwiped(_ sender: UISwipeGestureRecognizer) {
        switch editingPart {
        case .Body:
            if bodyCounter > 0 {
                bodyCounter -= 1
            } else {
                HapticResponseController.characterCustomizationExeeded()
            }
        case .LeftHand:
            if handCounter > 0 {
                handCounter -= 1
            } else {
                HapticResponseController.characterCustomizationExeeded()
            }
        case.LeftUpperArm:
            if armCounter > 0 {
                armCounter -= 1
            } else {
                HapticResponseController.characterCustomizationExeeded()
            }
        default: return
        }
        save()
        hideInstructions()
    }
    
    @objc func upSwiped(_ sender: UISwipeGestureRecognizer) {
        switch editingPart {
        case .Body:
            if bodyCounter < bodies.count - 1{
                bodyCounter += 1
            } else {
                HapticResponseController.characterCustomizationExeeded()
            }
        case .LeftHand:
            if handCounter < hands.count - 1 {
                handCounter += 1
            } else {
                HapticResponseController.characterCustomizationExeeded()
            }
        case.LeftUpperArm:
            if armCounter < arms.count - 2 {
                armCounter += 1
            } else {
                HapticResponseController.characterCustomizationExeeded()
            }
        default:
            return
        }
        hideInstructions()
        save()
    }
    
    @objc func receivedServerDismissedBrowser(_ notification: Notification) {
        DispatchQueue.main.async {
            self.clientSpinnerView?.removeFromSuperview()
            self.clientSpinnerView = nil
        }
    }
    
    
    // MARK: - Private Helper Methods
    private func setUp() {
        backgroundSelectorView.layer.cornerRadius = 16
        backgroundSelectorView.isUserInteractionEnabled = true
        backgroundSelectorView.translatesAutoresizingMaskIntoConstraints = false
        bodyImageView.translatesAutoresizingMaskIntoConstraints = false
        bodyCounter = 3
        armCounter = 5
        handCounter = 2
        
        // TODO: - Load configuration from userdefaults
        let tapGestureBody = UITapGestureRecognizer(target: self, action: #selector(self.bodySelected(_:)))
        bodyImageView.addGestureRecognizer(tapGestureBody)
        
        let tapGestureHands = UITapGestureRecognizer(target: self, action: #selector(self.handSelected(_:)))
        leftHandImageView.addGestureRecognizer(tapGestureHands)
        let tapGestureHands2 = UITapGestureRecognizer(target: self, action: #selector(self.handSelected(_:)))
        rightHandImageView.addGestureRecognizer(tapGestureHands2)
        
        let tapGestureArms = UITapGestureRecognizer(target: self, action: #selector(self.armSelected(_:)))
        leftArmImageView.addGestureRecognizer(tapGestureArms)
        let tapGestureArms2 = UITapGestureRecognizer(target: self, action: #selector(self.armSelected(_:)))
        rightArmImageView.addGestureRecognizer(tapGestureArms2)
        
        
        let views = [bodyImageView,leftArmImageView,leftHandImageView,rightArmImageView,rightHandImageView]
        for _view in views {
            let swipeUpGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.upSwiped(_:)))
            swipeUpGesture.direction = .up
            let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.downSwiped(_:)))
            swipeDownGesture.direction = .down
            _view?.addGestureRecognizer(swipeDownGesture)
            _view?.addGestureRecognizer(swipeUpGesture)
        }
        
        loadMyConfig()
        save()
        animateInstructions()
    }
    
    @objc func bodySelected(_ sender: UITapGestureRecognizer) {
        editingPart = .Body
        stopAllAnimations()
        animateJumping(view: bodyImageView)
    }
    
    @objc func handSelected(_ sender: UITapGestureRecognizer) {
        editingPart = .LeftHand
        stopAllAnimations()
        animateJumping(view: leftHandImageView)
        animateJumping(view: rightHandImageView)
    }
    
    @objc func armSelected(_ sender: UITapGestureRecognizer) {
        editingPart = .LeftUpperArm
        stopAllAnimations()
        animateJumping(view: leftArmImageView)
        animateJumping(view: rightArmImageView)
    }
    
    // MARK: - Animations
    private func animateInstructions() {
        let animation = CABasicAnimation.init(keyPath: #keyPath(CALayer.opacity))
        animation.autoreverses = true
        animation.repeatCount = .infinity
        animation.fromValue = bodyImageView.layer.opacity
        animation.toValue = 0
        animation.duration = 0.5
        CATransaction.begin()
        CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear))
        instructionLabel.layer.add(animation, forKey: #keyPath(CALayer.opacity))
        CATransaction.commit()
    }
    
    private func stopAllAnimations() {
        bodyImageView.layer.removeAllAnimations()
        leftHandImageView.layer.removeAllAnimations()
        rightHandImageView.layer.removeAllAnimations()
        leftArmImageView.layer.removeAllAnimations()
        rightArmImageView.layer.removeAllAnimations()
        if !instructionsShown {
            switchInstructions()
            instructionsShown = true
        } else {
            hideInstructions()
        }
    }
    
    private func switchInstructions() {
        instructionLabel.text = "Swipe to up or down to change the style"
    }
    
    private func hideInstructions() {
        instructionLabel.layer.removeAllAnimations()
        UIView.animate(withDuration: 0.2, animations: {
            self.view.updateConstraintsIfNeeded()
            self.instructionLabel.alpha = 0
            self.view.updateConstraintsIfNeeded()
        })
    }
    
    private func animateJumping(view: UIView) {
        let animation = CABasicAnimation.init(keyPath: #keyPath(CALayer.position))
        animation.autoreverses = true
        animation.repeatCount = .infinity
        animation.fromValue = view.layer.position
        animation.toValue = CGPoint(x: view.layer.position.x, y: view.layer.position.y - 10)
        animation.duration = 0.7
        CATransaction.begin()
        CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear))
        view.layer.add(animation, forKey: #keyPath(CALayer.position))
        CATransaction.commit()
    }
    
    private func save() {
        CharacterManager.sharedInstance.saveAll(bodyName: bodies[bodyCounter], armName: arms[armCounter], handName: hands[handCounter])
    }
    
    private func loadMyConfig() {
        if let previousConfiguration = CharacterManager.sharedInstance.getMyConfigAsIndex() {
            bodyCounter = (previousConfiguration[0] - 1)
            armCounter = (previousConfiguration[1] - 1)
            handCounter = (previousConfiguration[2] - 1)
            setBody()
            setArms()
            setHands()
        }
    }
    
    private func setHands() {
        leftHandImageView.image = UIImage(named: hands[handCounter])?.rotate(radians: -1.5708)
        rightHandImageView.image = UIImage(named: hands[handCounter])?.rotate(radians: 1.5708)
    }
    
    private func setArms() {
        leftArmImageView.image = UIImage(named: arms[armCounter])
        var rightArm = UIImage(named: arms[armCounter])
        rightArm = rightArm?.withHorizontallyFlippedOrientation()
        rightArmImageView.image = rightArm
    }
    
    private func setBody() {
        bodyImageView.image = UIImage(named: bodies[bodyCounter])
    }
}

// https://stackoverflow.com/questions/27092354/rotating-uiimage-in-swift/29753437
extension UIImage {
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
    
    func maskRoundedImage(radius: CGFloat) -> UIImage {
           let imageView: UIImageView = UIImageView(image: self)
           let layer = imageView.layer
           layer.masksToBounds = true
           layer.cornerRadius = radius
           UIGraphicsBeginImageContext(imageView.bounds.size)
           layer.render(in: UIGraphicsGetCurrentContext()!)
           let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()
           return roundedImage!
       }
}
