//
//  PlayMode.swift
//  Better Together
//
//  Created by Lukas Woyke on 11.01.21.
//

import Foundation

enum PlayMode {
    case tutorial
    case singlePlay
    case teamPlay
    case teamPlayLocalHost
    case teamPlayLocalJoin
}
