//
//  GameViewController.swift
//  Better Together
//
//  Created by Chan Yong Lee on 01.01.21.
//

import UIKit
import SpriteKit
import GameplayKit
import MultipeerConnectivity

/// View Controller that manages the game scene in single play mode, as well as in mulitplayer mode.
class GameViewController: UIViewController, LocalTeamPlayDelegate {
    
    var gamePlayMode: PlayMode? = nil
    
    var gameScene: GameScene!
    
    var clientsPlayerInfoSyncCount = 0
    var clientsReadyCount = 0
    var playerReadyWaitingSpinner: UIView?
    
    @IBOutlet weak var spinnerView: SpinnerView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var gyroscopeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light
        
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.isHidden = true
        self.exitButton.layer.cornerRadius = 16
        self.gyroscopeButton.layer.cornerRadius = 16
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.startGame(_:)),
                                               name: .startGame,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.gameResultReceived(_:)),
                                               name: .gameResultReceived,
                                               object: nil)
        
        // SKScene
        if let view = self.view as! SKView? {
            
            let gameScene = SKScene(fileNamed: "GameScene") as! GameScene
            gameScene.scaleMode = .aspectFill
            self.gameScene = gameScene

            GameGyroscopeController.sharedInstance.start()
            
            // select presented scene according to gamePlayModes
            switch gamePlayMode {
            case .tutorial:
                let tutorialScene = SKScene(fileNamed: "TutorialScene");
                tutorialScene?.scaleMode = .aspectFill
                view.presentScene(tutorialScene)
                spinnerView.alpha = 0
            case .teamPlayLocalHost:
                LocalTeamPlayManager.sharedInstance.delegate = self
                view.presentScene(gameScene)
                LocalTeamPlayManager.sharedInstance.sendToAll(sessionAction: .setup(.dismissedInvitation(true)), isReliable: true)
                // Show the spinner
                showServerSpinner()
            case .teamPlayLocalJoin:
                LocalTeamPlayManager.sharedInstance.delegate = self
                // after open gameViewController, send to host that level is set from client side
                LocalTeamPlayManager.sharedInstance.sendToHost(sessionAction: SessionAction.setup(SetupAction.responseForLevelSetting(true)), isReliable: true)
                playerReadyWaitingSpinner = Spinner.show(onView: self.view)
                // Dismiss the server spinner because we are on client side.
                spinnerView.alpha = 0
            case .singlePlay:
                self.spinnerView.alpha = 0
                view.presentScene(gameScene)
            default:
                break
            }
            
            view.ignoresSiblingOrder = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        gameScene = nil
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    deinit {
        debugPrint("Deintilized game view controller")
    }
    
    // MARK: - Local Team Play Delagete
    
    /*
     * this function ensures that every player in the game starts sending when every other player is ready
     * if this is not ensured, and the game scene of one player(let's say player 1) loads earlier than the game scene from others',
     * player 1 starts to send packets, that can't yet be executed by other players, because they don't have there game scene loaded yet
     * this is a big source of synchronization delay
     */
    /// For Client: recieve my character index for rendering in game scene from server
    func syncCharacterIndexForGameScene(playersInfo: [CodablePlayersInfo]) {
        DispatchQueue.main.async {
            // update my information (as client)
            if let myInfo = playersInfo.filter({ $0.displayName == UserDefaults.standard.mySelf.displayName }).first {
                self.gameScene.myCharacterIndex = myInfo.characterIndex
                UserDefaults.standard.mySelf.bodyColor = myInfo.characterBodyColor
                
                // Save all players configurations
                CharacterManager.sharedInstance.allOtherConfigurations = myInfo.otherPlayersConfig
                
                // After receiving the configurations, preload the textures first and continue setup after that.
                CharacterManager.sharedInstance.preloadConfigurationTextureClient {
                    // all playerInfo
                    LocalTeamPlayManager.sharedInstance.codablePlayersInfo = playersInfo
                    LocalTeamPlayManager.sharedInstance.sendToHost(sessionAction: SessionAction.setup(SetupAction.responseForCharacterInfoSyncDone(true)), isReliable: true)
                }
            }
        }
    }
    
    /// For Server and Client: receive clients' game scene loading state
    func playersGameSceneLoadStateReceived(for peerID: MCPeerID, isReady: Bool) {
        DispatchQueue.main.async { [self] in 
            if isReady {
                if GameScene.isServer && !gameScene.gameStarted {
                    clientsReadyCount += 1
                    if clientsReadyCount == LocalTeamPlayManager.sharedInstance.playersInfo.count - 1 {
                        // All players' game scenes are loaded
                        LocalTeamPlayManager.sharedInstance.sendToAll(sessionAction: SessionAction.setup(SetupAction.gameSceneLoaded(true)), isReliable: true)
                        gameScene.addCharacters()
                        debugPrint("playersGameSceneLoadStateReceived(Server): all clients' game scenes are loaded!")
                        gameScene.gameStarted = true
                        spinnerView.dismissView()
                    }
                } else if !GameScene.isServer && !gameScene.gameStarted {
                    if peerID == LocalTeamPlayManager.sharedInstance.host?.peerID {
                        gameScene.addCharacters()
                        gameScene.gameStarted = true
                        debugPrint("playersGameSceneLoadStateReceived(Server): game start!")
                    }
                }
            }
        }
    }
    
    /// For Server: receive character's action data and update its physics
    func characterActionReceived(characterActionData: CharacterActionData) {
        guard gameScene.gameStarted else { return }
        for action in characterActionData.actions {
            switch action {
            case .physics(let physics):
                gameScene.characterMovementData[characterActionData.characterIndex] = physics.vector
            case .reachesGoal(let reachesGoal):
                gameScene.characterInGoalList[characterActionData.characterIndex] = reachesGoal
                gameScene.characters[characterActionData.characterIndex].reachesGoal = reachesGoal
            case .grabInfo(let grabInfo):
                gameScene.characters[characterActionData.characterIndex].applyReceivedGrabInfo(grabInfo: grabInfo, playerIndex: characterActionData.characterIndex)
            case .dropped(let isDropped):
                if isDropped {
                    NotificationCenter.default.post(name: .gameResultReceived, object: nil, userInfo: ["result": "fail"])
                    LocalTeamPlayManager.sharedInstance.sendToAll(sessionAction: SessionAction.setup(SetupAction.gameOver(true)), isReliable: true)
                }
            }
        }
        
    }
    
    /// For Client: triggered when all characters action data is received from server
    func allChracterActionsReceived(charactersActions: [CharacterActionData]) {
        gameScene.queueReceivedCharactersAction(data: charactersActions)
    }
    
    /// Trigger when the result is received
    @objc func gameResultReceived(_ notification: Notification) {
        DispatchQueue.main.async {
            self.showResultDialog(onView: self.view, result: notification.userInfo!["result"] as! String)
        }
    }
    
    /// For Client: after the server send "gameStart" action, present the gameScene
    @objc func startGame(_ notifiaction: Notification) {
        DispatchQueue.main.async { [self] in
            if let view = self.view as! SKView? {
                playerReadyWaitingSpinner?.removeFromSuperview()
                view.presentScene(self.gameScene)
                LocalTeamPlayManager.sharedInstance.sendToHost(sessionAction: SessionAction.setup(SetupAction.gameSceneLoaded(true)), isReliable: true)
            }
        }
    }
    
    @IBAction func quitButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "Sure you want to leave the game?", message: nil, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let exit = UIAlertAction(title: "Exit", style: .destructive, handler: { _ in self.exitGame() })
        alert.addAction(cancel)
        alert.addAction(exit)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func gyroscopeTapped(_ sender: UIButton) {
        HapticResponseController.grabSucceedFeedback()
        GameGyroscopeController.sharedInstance.resetOrientation()
        gameScene.showCalibrationMessage = true
    }
    
    /// For Exiting the game by my self.
    private func exitGame() {
        // Let all other players know.
        if gamePlayMode! == .teamPlayLocalHost || gamePlayMode! == .teamPlayLocalJoin {
            LocalTeamPlayManager.sharedInstance.sendToAll(sessionAction: SessionAction.setup(SetupAction.gameOver(true)), isReliable: false)
            LocalTeamPlayManager.sharedInstance.leaveSession()
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /// When another player exited the game.
    func playerExitGame() {
        LocalTeamPlayManager.sharedInstance.leaveSession()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - Set up Progress server side
    var maxValueOfSetps = 0
    private var progressCounter = 0
    
    /// 1. For Server: Show the waiting view for server
    func showServerSpinner() {
        if let _maxValue = LocalTeamPlayManager.sharedInstance.allPlayersPeerIDs?.count {
            maxValueOfSetps = _maxValue * 5
            // For each character there have to be 5 messages to be exchanged (customization, level, levelResponse, index, indexConfirmation)
            spinnerView.start(maxProgress: maxValueOfSetps)
            
            // Update the progress if server already received configurations.
            progressCounter = CharacterManager.sharedInstance.allPlayersConfiguration.count
            if progressCounter != 0 {
                spinnerView.currentState = .receivingConfigs
            }
            spinnerView.updateProgress(progressCount: progressCounter)
            
            // Check if maybe already all customizations arrived while waiting in the invitations browser.
            checkCustomizationsComplete()
        } else {
            fatalError("Could not get peerIDs")
        }
    }
    
    /// 2. For Server: Receiving Customizations and send level
    func receivedNewCustomization(config: [String], for player: MCPeerID) {
        DispatchQueue.main.async { [self] in
            progressCounter += 1
            spinnerView.updateProgress(progressCount: progressCounter)
            checkCustomizationsComplete()
        }
    }
    
    /// Checks if maybe already all customizations arrived while waiting in the invitations browser.
    private func checkCustomizationsComplete() {
        // When all configurations are received, ...
        if CharacterManager.sharedInstance.allPlayersConfiguration.count == LocalTeamPlayManager.sharedInstance.allPlayersPeerIDs?.count {
            // Send the level to all Players.
            let selectedLevel = LevelSelectionViewController.selectedLevel
            LocalTeamPlayManager.sharedInstance.sendToAll(sessionAction: SessionAction.setup(SetupAction.selectedLevel(selectedLevel)), isReliable: true)
            // Update the progress Counter
            spinnerView.currentState = .sendingLevels
            progressCounter = LocalTeamPlayManager.sharedInstance.allPlayersPeerIDs!.count * 2 // Is number of messages exchanged in the setup process, if this state is reached. 2 messages exchanged for each player.
            spinnerView.updateProgress(progressCount: progressCounter)
        }
    }
    
    /// 3. For Server: Receiving level confimartions
    func receivedLevelConfirmation(for player: MCPeerID) {
        DispatchQueue.main.async {
            self.progressCounter += 1
            self.spinnerView.updateProgress(progressCount: self.progressCounter)
            self.checkLevelComplete()
        }
    }
    
    private func checkLevelComplete() {
        // Check if all clients are ready and received their Customization.
        if progressCounter == LocalTeamPlayManager.sharedInstance.allPlayersPeerIDs!.count * 3 {
            
            // After levels send, load the textures
            CharacterManager.sharedInstance.preloadConfigurationTexturesServer {
                // When loading the configurations is done, load continue with setup.
                // MARK: Sync players index.
                LocalTeamPlayManager.sharedInstance.setClientPlayers()
                
                // Upadte the progress view.
                self.spinnerView.currentState = .sendingPlayerInfo
                self.progressCounter = LocalTeamPlayManager.sharedInstance.allPlayersPeerIDs!.count * 4  // Is number of messages exchanged in the setup process, if this state is reached. 4 messages exchanged for each player.
                self.spinnerView.updateProgress(progressCount: self.progressCounter)
            }
        }
    }
    
    /// 4 . For Server: receive clients are done with playerinfo sync
    func playerSyncDoneReceived(for peerID: MCPeerID, isDone: Bool) {
        DispatchQueue.main.async {
            if isDone {
                self.clientsPlayerInfoSyncCount += 1
                
                self.progressCounter += 1
                self.spinnerView.currentState = .receivingPlayerInfo
                
                if self.clientsPlayerInfoSyncCount == LocalTeamPlayManager.sharedInstance.playersInfo.count - 1 {
                    self.spinnerView.currentState = .setUpComplete
                    
                    // All players are done with playerinfo sync
                    LocalTeamPlayManager.sharedInstance.sendToAll(sessionAction: SessionAction.setup(SetupAction.startGame(true)), isReliable: true)
                }
                self.spinnerView.updateProgress(progressCount: self.progressCounter)
            }
        }
    }
}

extension GameViewController {
    func showResultDialog(onView: UIView, result: String) {
        let dialogView = UIView.init(frame: onView.bounds)
        
        let backgroundView = UIView.init(frame: dialogView.bounds)
        backgroundView.backgroundColor = UIColor(red: 0.58, green: 0.81, blue: 0.82, alpha: 0.8)
            
        let label = UILabel()
        label.font = label.font.withSize(30.0)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.frame = CGRect(origin: CGPoint(x: dialogView.frame.minX, y:dialogView.frame.minY),
                             size: CGSize(width: dialogView.frame.width, height: dialogView.frame.height - 16))
        
        let button = RoundedRectangleButton()
        button.cornerRadius = 16
        button.frame = CGRect(origin: CGPoint(x: dialogView.frame.width * 0.5 - 50, y: dialogView.frame.height * 0.5 + 16), size: CGSize(width: 100, height: 50))
        button.backgroundColor = UIColor(red: 0.35, green: 0.42, blue: 0.53, alpha: 1.00)
        button.setTitle("Quit", for: .normal)
        button.titleLabel?.font = button.titleLabel?.font.withSize(24.0)
        button.addTarget(self, action: #selector(quitGame(_:)), for: .touchUpInside)
            
        switch result {
        case "success":
            label.text = "Level clear!"
        case "disconnected":
            label.text = "A player has exited the game!"
        default:
            label.text = "Game over.."
        }
            
        dialogView.addSubview(backgroundView)
        dialogView.addSubview(label)
        dialogView.addSubview(button)
        onView.addSubview(dialogView)
    }
    
    @objc func quitGame(_ sender: UIButton) {
        playerExitGame()
    }
}
