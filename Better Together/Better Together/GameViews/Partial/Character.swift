//
//  CharacterBody.swift
//  Better Together
//
//  Created by Chan Yong Lee on 03.01.21.
//

import UIKit
import SpriteKit

class Character : SKNode {
    
    // player index
    var playerIndex = 0
    private var isMyCharacter: Bool = false
    var reachesGoal: Bool = false
    
    // flags for grab actions
    let lowerArmLowFriction : CGFloat = 0.01
    let lowerArmHighFriction : CGFloat  = 1.0
    // Hand textures
    var leftHandOpenTexture = SKTexture(imageNamed: "lefthand.png")
    var leftHandCloseTexture = SKTexture(imageNamed: "lefthandGrab.png")
    var righttHandOpenTexture = SKTexture(imageNamed: "righthand.png")
    var rightHandCloseTexture = SKTexture(imageNamed: "righthandGrab.png")
    
    //MARK: this flag should be set in contact delegate
    var leftHandGrabbing = false {
        didSet {
            // whenever at least one hand is grabbing , the mass is set to original mass, and the
            // forceDiagonal is also strengthend correspondingly, so that the character can be moved
            // reset the mass so that the character can move again
            if leftHandGrabbing {
                bodyParts[.Body]!.physicsBody!.mass = originalMass_body
                forceDiagonal = groundMovingForce
                // set the body parts that have to be moved
                if rightHandGrabbing{
                    // when both hands are grabbing, shake body
                    bodyPartsToMove = [BodyPart.Body]
                } else {
                    // else, move right hand
                    bodyPartsToMove = [BodyPart.RightHand]
                }
                
                // allow that the arm can get bent again when trying to move left or right
                joints[.jointLeftUpperLeftLower]!.frictionTorque = lowerArmLowFriction
                leftArmsGotClosureAgain = false
                
                // play grab sound
                if GameScene.multiplayerMode {
                    if GameScene.isServer && isMyCharacter { self.run(grabSound) }
                    if !GameScene.isServer { self.run(grabSound) }
                } else {
                    self.run(grabSound)
                }
    
            } else {
                // both arms are not grabbing --> set the mass to heavymass so that the character can't be moved
                if !rightHandGrabbing {
                    bodyParts[.Body]!.physicsBody!.mass = heavyMass
                    forceDiagonal = groundStuckForce
                    bodyPartsToMove = [BodyPart.RightHand, BodyPart.LeftHand]
                } else {
                    bodyParts[.Body]!.physicsBody!.mass = originalMass_body
                    forceDiagonal = groundMovingForce
                    bodyPartsToMove = [BodyPart.LeftHand]
                }
            }
        }
    }
    //MARK: this flag should be set in contact delegate
    var rightHandGrabbing = false {
        didSet {
            if rightHandGrabbing {
                
                bodyParts[.Body]!.physicsBody!.mass = originalMass_body
                forceDiagonal = groundMovingForce
                
                if leftHandGrabbing {
                    bodyPartsToMove = [BodyPart.Body]
                } else {
                    bodyPartsToMove = [BodyPart.LeftHand]
                }
                
                joints[.jointRightUpperRightLower]!.frictionTorque = lowerArmLowFriction
                rightArmsGotClosureAgain = false
                
                if GameScene.multiplayerMode {
                    if GameScene.isServer && isMyCharacter { self.run(grabSound) }
                    if !GameScene.isServer { self.run(grabSound) }
                } else {
                    self.run(grabSound)
                }
            } else {
                if !leftHandGrabbing {
                    bodyParts[.Body]!.physicsBody!.mass = heavyMass
                    forceDiagonal = groundStuckForce
                    bodyPartsToMove = [BodyPart.RightHand, BodyPart.LeftHand]
                } else {
                    bodyParts[.Body]!.physicsBody!.mass = originalMass_body
                    forceDiagonal = groundMovingForce
                    bodyPartsToMove = [BodyPart.RightHand]
                }
            }
        }
    }
    
    //MARK: this flags are used for grab interaction
    var leftHandJustGrabbed = false
    var rightHandJustGrabbed = false
    var leftHandContacted: Bool = false
    var rightHandContacted: Bool = false
    
    // categories for contact detection
    enum ContactType: UInt32 {
        case ObstacleInteraction = 1
        case OtherBodyParts = 2
        case defaultCollision = 0xFFFFFFFF
        case collisionAvoidance = 3
    }
    
    //enums
    enum Direction: Int{
        case None = 0
        case Up = 1
        case Down = 2
        case Right = 3
        case Left = 4
    }
    
    enum TouchStatus: Int, Hashable, Codable {
        case TouchBegan = 0
        case TouchEnded = 1
        case None = 2
    }
    
    enum BodyPart: Int, CaseIterable, Hashable, Codable {
        case Body = 0
        case LeftUpperArm = 1
        case LeftLowerArm = 2
        case LeftHand = 3
        case RightUpperArm = 4
        case RightLowerArm = 5
        case RightHand = 6
    }
    
    enum Joints: Int{
        case jointBodyLeftUpperArm = 0
        case jointLeftUpperLeftLower = 1
        case jointLeftLowerLeftHand = 2
        case jointBodyRightUpperArm = 3
        case jointRightUpperRightLower = 4
        case jointRightLowerRightHand = 5
    }
    
    // body parts and joints
    var bodyParts : [BodyPart : SKNode] = [:]
    var joints : [Joints : SKPhysicsJointPin] = [:]
    
    //vertical obstacle minX, minY - needed to distinguish whether a character wants to reach an obstacle from below
    static var obstacleLowPoints : [CGPoint] =  []
    
    // MARK: - limits the applied force
    var forceDiagonal : CGFloat = 0
    var groundStuckForce : CGFloat = 0 // force  applied when characters is on a surface without grabbin anything -> user shouldn't be able to move character
    var groundMovingForce : CGFloat = 0 // force applied when character has grabbed on a ground -> user should be able to move character
    var reachObjectForce : CGFloat = 0 // force applied when character is near an object which is above him. the character should have enough force to lift himself up a bit and reach the obstacle
    
    // MARK: - used to let characters not able to move when on the ground
    var originalMass_body : CGFloat = 0
    var heavyMass : CGFloat = 0
    let massSkalar : CGFloat = 10 // used to calculate heavy mass from original mass
    var bodyPartsToMove : [BodyPart] = [BodyPart.LeftHand, BodyPart.RightHand]
    
    // MARK: - used to keep the arms straight while swinging
    var armDistance : CGFloat = 0 // distance between upper arm middle point and lower arm middle point
    var upperArmBodyDistanceLB : CGFloat = 0 // distance between body and upper arm middle point
    var leftArmsGotClosureAgain = true
    var rightArmsGotClosureAgain = true
    var maxUpperArmHandDistance : CGFloat = 0 // distance between an upper arm and hand on the opposite side
    var handToHandDistance : CGFloat = 0
    
    //MARK: - flags to distinguish several situation for adequate motion apply
    var leftHandGrabbingAt : SKShapeNode?
    var rightHandGrabbingAt : SKShapeNode?
    
    var isDropped = false {
        didSet {
            if isDropped {
                if GameScene.multiplayerMode {
                    if isMyCharacter { self.run(dropSound)}
                    
                } else {
                    self.run(dropSound)
                }
            }
        }
    }

    //MARK: - for tutorial
    var leftHandFirstLineContact = false
    var rightHandFirstLineContact = false
    
    var isTutorial = false
    var canGetSwingForce = false
    
    // MARK: - SoundEffectPlayer
    let grabSound = SKAction.playSoundFileNamed("audio_grab.mp3", waitForCompletion: false)
    let dropSound = SKAction.playSoundFileNamed("audio_drop.mp3", waitForCompletion: false)
    
    //MARK: - for multiplayer grab interaction
    // these flags should be save in each character's instance, since master is processing all the game logics
    var leftGrabButtonPressed = false
    var rightGrabButtonPressed = false
    var leftHandGrabbedCharacter = false
    var rightHandGrabbedCharacter = false
    
    let localTeamPlayManager = LocalTeamPlayManager.sharedInstance
    
    // A charater should be created using ChracterCreator
    override init() {
        super.init()
    }
    
    /* initializes an instance of Character with given properties
    * @param    scene: the scene where the character will be placed at
    *           color: color of the character body
    *           bodyRadius: determines the size of the character
    *           position: determines the position of the character, in scene's coordinate
    */
    init(scene : SKScene, color : BodyColor, bodyRadius : CGFloat, position : CGPoint, playerIndex : Int, isMyCharacter: Bool, isTutorial: Bool) {
        super.init()
        
        // add the character instance as a child of a scene
        scene.addChild(self)
        // set the position
        self.position = position
        
        self.playerIndex = playerIndex
        self.isMyCharacter = isMyCharacter
        
        self.isTutorial = isTutorial
        
        // add body and arms
        // size of the body parts are calculated relative to the size of the body
        initializeBodyParts(bodyRadius: bodyRadius)
        
        // located body parts in right positions
        locateBodyParts()
        
        // add body parts as children of character node (self)
        addBodyPartsAsChildrenOfCharaterBody()
        
        // set color
        if isTutorial {
            setCharacterColor(bodyColor: color)
        } else {
            setCharacterColor(bodyColor: BodyColor.init(color: UIColor.white))
        }
        
        
        // add physics body to the body parts
        addPhysicsBodyToBodyParts(radius: bodyRadius)
        
        // create joints between body parts
        setJoints(scene: scene)
        
    }
    
    //MARK:is this initializer correctly written?
    required init?(coder aDecoder: NSCoder) {
        bodyParts[.Body] = aDecoder.decodeObject(forKey: "body") as? SKShapeNode
        bodyParts[.LeftUpperArm] = aDecoder.decodeObject(forKey: "leftUpperArm") as? SKShapeNode
        bodyParts[.LeftLowerArm] = aDecoder.decodeObject(forKey: "leftLowerArm") as? SKShapeNode
        bodyParts[.LeftHand]  = aDecoder.decodeObject(forKey: "leftHand") as? SKShapeNode
        bodyParts[.RightUpperArm] = aDecoder.decodeObject(forKey: "rightUpperArm") as? SKShapeNode
        bodyParts[.RightLowerArm] = aDecoder.decodeObject(forKey: "rightLowerArm") as? SKShapeNode
        bodyParts[.RightHand]  = aDecoder.decodeObject(forKey: "rightHand") as? SKShapeNode
        super.init(coder: aDecoder)
    }
    
    /* Function which initilaizes all the body parts
     * @param   radius: radius of the body
     */
    func initializeBodyParts(bodyRadius radius: CGFloat) {
      
        // calculate size of the arms and hands relatively to the size of the body
        let upperArmWidth = radius * 2
        var armHeight = radius * 0.9
        if isTutorial {
            armHeight = radius * 0.25
        }
        
        let lowerArmWidth = radius * 2
        let handRadius = radius * 1.3
        //set all the necessary forces
        forceDiagonal = radius * 4  // can be changed to adjust force applied to character
        groundStuckForce = forceDiagonal * 0.8
        groundMovingForce = forceDiagonal * 2.0
        if isTutorial {
            groundStuckForce *= 1.25
        }
        reachObjectForce = groundMovingForce * 1.5
        
        
        
        if !isTutorial {
            bodyParts[.Body] = SKShapeNode(circleOfRadius: radius)
            bodyParts[.LeftUpperArm] = SKSpriteNode(color: UIColor.clear, size: CGSize(width: upperArmWidth, height: armHeight))
            bodyParts[.LeftLowerArm] = SKSpriteNode(color: UIColor.clear, size: CGSize(width: lowerArmWidth, height: armHeight))
            bodyParts[.LeftHand] = SKSpriteNode(imageNamed: "lefthand.png") // make hands as sprite node
            bodyParts[.RightUpperArm] = SKSpriteNode(color: UIColor.clear, size: CGSize(width: upperArmWidth, height: armHeight))
            bodyParts[.RightLowerArm] = SKSpriteNode(color: UIColor.clear, size: CGSize(width: lowerArmWidth, height: armHeight))
            bodyParts[.RightHand] = SKSpriteNode(imageNamed: "righthand.png") // make hands as sprite node
            
            bodyParts[.RightHand]?.setScale(0.8)
            bodyParts[.LeftHand]?.setScale(0.8)
        } else {
            bodyParts[.Body] = SKShapeNode(circleOfRadius: radius)
            bodyParts[.LeftUpperArm] = SKShapeNode(rectOf: CGSize(width: upperArmWidth, height: armHeight))
            bodyParts[.LeftLowerArm] = SKShapeNode(rectOf: CGSize(width: lowerArmWidth, height: armHeight))
            bodyParts[.LeftHand] = SKSpriteNode(imageNamed: "lefthand.png") // make hands as sprite node
            bodyParts[.RightUpperArm] = SKShapeNode(rectOf: CGSize(width: upperArmWidth, height: armHeight))
            bodyParts[.RightLowerArm] = SKShapeNode(rectOf: CGSize(width: lowerArmWidth, height: armHeight))
            bodyParts[.RightHand] = SKSpriteNode(imageNamed: "righthand.png") // make hands as sprite node
            
            bodyParts[.RightHand]?.setScale(0.7)
            bodyParts[.LeftHand]?.setScale(0.7)
        }
        
        
        // make hands don't rotate
        let offsetRotation = CGFloat( 90 * Double.pi / 180)
        let leftHandLookAtContraint = SKConstraint.orient(to: bodyParts[.LeftLowerArm]!, offset: .init(lowerLimit: offsetRotation, upperLimit: offsetRotation))
        let rightHandLookAtContraint = SKConstraint.orient(to: bodyParts[.RightLowerArm]!, offset: .init(lowerLimit: offsetRotation, upperLimit: offsetRotation))
        bodyParts[.LeftHand]?.constraints = [leftHandLookAtContraint]
        bodyParts[.RightHand]?.constraints = [rightHandLookAtContraint]
        
        // set all the necessary distances to distinguish situations
        armDistance = ((upperArmWidth + lowerArmWidth) / 2) * 1.1 // doesn't have to be changed
        maxUpperArmHandDistance = (radius + upperArmWidth * 2 + lowerArmWidth) * 1.1 // doesn't have to be changed
        handToHandDistance = radius * 2 + lowerArmWidth * 2 + upperArmWidth * 2 + handRadius * 2
        upperArmBodyDistanceLB = radius * 1.6
    }
    
    /* Function which sets color of the character
     * @param   color: desired color of character
     */
    func setCharacterColor(bodyColor : BodyColor) {
        let color = bodyColor.uiColor
        
        if !isTutorial {
            (bodyParts[.Body] as! SKShapeNode).fillColor = color
            (bodyParts[.LeftUpperArm] as! SKSpriteNode).color = color
            (bodyParts[.LeftLowerArm] as! SKSpriteNode).color = color
            (bodyParts[.RightUpperArm] as! SKSpriteNode).color = color
            (bodyParts[.RightLowerArm] as! SKSpriteNode).color = color
            (bodyParts[.Body] as! SKShapeNode).strokeColor = color
            (bodyParts[.LeftUpperArm] as! SKSpriteNode).color = color
            (bodyParts[.LeftLowerArm] as! SKSpriteNode).color = color
            (bodyParts[.RightUpperArm] as! SKSpriteNode).color = color
            (bodyParts[.RightLowerArm] as! SKSpriteNode).color = color
            (bodyParts[.Body] as! SKShapeNode).lineWidth = CGFloat(4.0)
        } else {
            (bodyParts[.Body] as! SKShapeNode).fillColor = color
            (bodyParts[.LeftUpperArm] as! SKShapeNode).fillColor = color
            (bodyParts[.LeftLowerArm] as! SKShapeNode).fillColor = color
            (bodyParts[.RightUpperArm] as! SKShapeNode).fillColor = color
            (bodyParts[.RightLowerArm] as! SKShapeNode).fillColor = color
            (bodyParts[.Body] as! SKShapeNode).strokeColor = color
            (bodyParts[.LeftUpperArm] as! SKShapeNode).strokeColor = color
            (bodyParts[.LeftLowerArm] as! SKShapeNode).strokeColor = color
            (bodyParts[.RightUpperArm] as! SKShapeNode).strokeColor = color
            (bodyParts[.RightLowerArm] as! SKShapeNode).strokeColor = color
            (bodyParts[.Body] as! SKShapeNode).lineWidth = CGFloat(4.0)
        }
    }
    
    
    /* Function which adds body part nodes to the ChracterBody node as children
     *
     */
    func addBodyPartsAsChildrenOfCharaterBody() {
        self.addChild(bodyParts[.Body]!)
        self.addChild(bodyParts[.LeftUpperArm]!)
        self.addChild(bodyParts[.LeftLowerArm]!)
        self.addChild(bodyParts[.LeftHand]!)
        self.addChild(bodyParts[.RightUpperArm]!)
        self.addChild(bodyParts[.RightLowerArm]!)
        self.addChild(bodyParts[.RightHand]!)
    }
    
    /* Function which locates each body parts
     *
     */
    func locateBodyParts() {
        // body will be located at the origin
        bodyParts[.Body]!.position = .zero
        bodyParts[.LeftUpperArm]!.position = CGPoint(x: bodyParts[.Body]!.position.x -  bodyParts[.Body]!.frame.width / 2 - bodyParts[.LeftUpperArm]!.frame.width / 2, y: bodyParts[.Body]!.position.y)
        bodyParts[.LeftLowerArm]!.position = CGPoint(x: bodyParts[.LeftUpperArm]!.position.x - bodyParts[.LeftLowerArm]!.frame.width / 2 - bodyParts[.LeftUpperArm]!.frame.width / 2, y: bodyParts[.Body]!.position.y)
        bodyParts[.LeftHand]!.position = CGPoint(x: bodyParts[.LeftLowerArm]!.position.x - bodyParts[.LeftLowerArm]!.frame.width / 2 , y : bodyParts[.Body]!.position.y)
        bodyParts[.RightUpperArm]!.position = CGPoint(x: bodyParts[.Body]!.position.x + bodyParts[.Body]!.frame.width / 2 + bodyParts[.RightUpperArm]!.frame.width / 2, y: bodyParts[.Body]!.position.y)
        bodyParts[.RightLowerArm]!.position = CGPoint(x: bodyParts[.RightUpperArm]!.position.x + bodyParts[.RightUpperArm]!.frame.width / 2 + bodyParts[.RightLowerArm]!.frame.width / 2, y: bodyParts[.Body]!.position.y)
        bodyParts[.RightHand]!.position = CGPoint(x: bodyParts[.RightLowerArm]!.position.x  + bodyParts[.RightLowerArm]!.frame.width / 2, y : bodyParts[.Body]!.position.y)
        
    }
    
    /* Function which adds physicsBody to each body parts
     *
     */
    func addPhysicsBodyToBodyParts(radius : CGFloat) {
        bodyParts[.Body]!.physicsBody = SKPhysicsBody(circleOfRadius: radius)
        bodyParts[.LeftUpperArm]!.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: radius * 2, height: radius * 0.9))
        bodyParts[.LeftLowerArm]!.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: radius * 2, height: radius * 0.9))
        // note that they have rectangle physics body
        bodyParts[.LeftHand]!.physicsBody = SKPhysicsBody(circleOfRadius: radius * 0.7)
        bodyParts[.RightUpperArm]!.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: radius * 2, height: radius * 0.9))
        bodyParts[.RightLowerArm]!.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: radius * 2, height: radius * 0.9))
        bodyParts[.RightHand]!.physicsBody = SKPhysicsBody(circleOfRadius: radius * 0.7)

        
        // set contact bit masks of the hands to detect the contact with obstacles
        bodyParts[.Body]!.physicsBody?.contactTestBitMask = ContactType.ObstacleInteraction.rawValue
        bodyParts[.LeftHand]!.physicsBody?.contactTestBitMask = ContactType.ObstacleInteraction.rawValue
        bodyParts[.RightHand]!.physicsBody?.contactTestBitMask = ContactType.ObstacleInteraction.rawValue
        bodyParts[.LeftUpperArm]!.physicsBody?.contactTestBitMask = ContactType.ObstacleInteraction.rawValue
        bodyParts[.LeftLowerArm]!.physicsBody?.contactTestBitMask = ContactType.ObstacleInteraction.rawValue
        bodyParts[.RightUpperArm]!.physicsBody?.contactTestBitMask = ContactType.ObstacleInteraction.rawValue
        bodyParts[.RightLowerArm]!.physicsBody?.contactTestBitMask = ContactType.ObstacleInteraction.rawValue
        
        originalMass_body = bodyParts[.Body]!.physicsBody!.mass
        heavyMass = bodyParts[.Body]!.physicsBody!.mass * massSkalar
        bodyParts[.Body]!.physicsBody!.mass =  heavyMass
        bodyParts[.Body]!.physicsBody!.restitution = 0
    }
    
    /* Function which initilaizes all the joints
     * @param   scene: a node with the physicsWorld, to which the joints are going to be added to
     */
    func setJoints(scene : SKScene) {
        // define physic joints that connect body parts
        // the joint positions are relatve to the body parts' position
        // their positions should be converted into scene's coordinate
        joints[.jointBodyLeftUpperArm] = SKPhysicsJointPin.joint(withBodyA: bodyParts[.Body]!.physicsBody!, bodyB: bodyParts[.LeftUpperArm]!.physicsBody!, anchor: convert(CGPoint(x: bodyParts[.Body]!.position.x - bodyParts[.Body]!.frame.width / 2, y: bodyParts[.Body]!.position.y), to: scene))
        joints[.jointLeftUpperLeftLower] = SKPhysicsJointPin.joint(withBodyA: bodyParts[.LeftUpperArm]!.physicsBody!, bodyB: bodyParts[.LeftLowerArm]!.physicsBody!, anchor: convert(CGPoint(x: bodyParts[.LeftUpperArm]!.position.x - bodyParts[.LeftUpperArm]!.frame.width / 2, y: bodyParts[.LeftUpperArm]!.position.y), to: scene))
        joints[.jointLeftLowerLeftHand] = SKPhysicsJointPin.joint(withBodyA: bodyParts[.LeftLowerArm]!.physicsBody!, bodyB: bodyParts[.LeftHand]!.physicsBody!, anchor: convert(CGPoint(x: bodyParts[.LeftLowerArm]!.position.x - bodyParts[.LeftLowerArm]!.frame.width / 2, y: bodyParts[.LeftLowerArm]!.position.y), to: scene))
        joints[.jointBodyRightUpperArm] = SKPhysicsJointPin.joint(withBodyA: bodyParts[.Body]!.physicsBody!, bodyB: bodyParts[.RightUpperArm]!.physicsBody!, anchor: convert(CGPoint(x: bodyParts[.Body]!.position.x + bodyParts[.RightUpperArm]!.frame.width / 2, y: bodyParts[.Body]!.position.y), to: scene))
        joints[.jointRightUpperRightLower] = SKPhysicsJointPin.joint(withBodyA: bodyParts[.RightUpperArm]!.physicsBody!, bodyB: bodyParts[.RightLowerArm]!.physicsBody!, anchor: convert(CGPoint(x: bodyParts[.RightUpperArm]!.position.x + bodyParts[.RightUpperArm]!.frame.width / 2, y: bodyParts[.RightUpperArm]!.position.y), to: scene))
        joints[.jointRightLowerRightHand] = SKPhysicsJointPin.joint(withBodyA: bodyParts[.RightLowerArm]!.physicsBody!, bodyB: bodyParts[.RightHand]!.physicsBody!, anchor: convert(CGPoint(x: bodyParts[.RightLowerArm]!.position.x + bodyParts[.RightLowerArm]!.frame.width / 2, y: bodyParts[.RightLowerArm]!.position.y), to: scene))
        
        // limit the lotation angle
        joints[.jointBodyLeftUpperArm]!.shouldEnableLimits = true
        joints[.jointBodyLeftUpperArm]!.lowerAngleLimit = CGFloat(-Double.pi/2)
        joints[.jointBodyLeftUpperArm]!.upperAngleLimit = CGFloat(Double.pi/2)
        joints[.jointBodyRightUpperArm]!.shouldEnableLimits = true
        joints[.jointBodyRightUpperArm]!.lowerAngleLimit = CGFloat(-Double.pi/2)
        joints[.jointBodyRightUpperArm]!.upperAngleLimit = CGFloat(Double.pi/2)
        // let lower arms rotate 360 degrees
        joints[.jointLeftUpperLeftLower]!.shouldEnableLimits = false
        joints[.jointLeftUpperLeftLower]!.lowerAngleLimit = CGFloat(-Double.pi)
        joints[.jointLeftUpperLeftLower]!.upperAngleLimit = CGFloat(Double.pi)
        joints[.jointRightUpperRightLower]!.shouldEnableLimits = false
        joints[.jointRightUpperRightLower]!.lowerAngleLimit = CGFloat(-Double.pi)
        joints[.jointRightUpperRightLower]!.upperAngleLimit = CGFloat(Double.pi)
        
        //MARK: physics adjustment
        // controlls how fast/slow/smooth the actions(movements) of body parts are
        // set torque friction
        joints[.jointBodyLeftUpperArm]!.frictionTorque = lowerArmLowFriction
        joints[.jointBodyRightUpperArm]!.frictionTorque = lowerArmLowFriction
        joints[.jointLeftUpperLeftLower]!.frictionTorque = lowerArmLowFriction
        joints[.jointLeftUpperLeftLower]!.frictionTorque = lowerArmLowFriction
        
        
        // add joints to scene's physicsWorld
        scene.physicsWorld.add(joints[.jointBodyLeftUpperArm]!)
        scene.physicsWorld.add(joints[.jointLeftUpperLeftLower]!)
        scene.physicsWorld.add(joints[.jointLeftLowerLeftHand]!)
        scene.physicsWorld.add(joints[.jointBodyRightUpperArm]!)
        scene.physicsWorld.add(joints[.jointRightUpperRightLower]!)
        scene.physicsWorld.add(joints[.jointRightLowerRightHand]!)
    }

    /* function that moves character according to the vector given
     *
     */
    func moveCharacter(vector: CGVector) {
        
        //MARK: logic for keeping the arms straight
        // check if arms are close to each other again
        if checkLeftArmsClose() {
            leftArmsGotClosureAgain = true
        }
        if checkRightArmsClose() {
            rightArmsGotClosureAgain = true
        }
        
        for bp in bodyPartsToMove {
            // let the arms straight once they get straight
            switch bp {
            case BodyPart.LeftHand:
                if vector != .zero {
                    // keep the arms straight only if got close once again
                    if checkLeftArmsStraight() && leftArmsGotClosureAgain {
                        joints[.jointLeftUpperLeftLower]!.frictionTorque = lowerArmHighFriction
                    } else {
                        joints[.jointLeftUpperLeftLower]!.frictionTorque = lowerArmLowFriction
                    }
                } else {
                    // the straightened arms should be able to be bended again
                    leftArmsGotClosureAgain = false
                    joints[.jointLeftUpperLeftLower]!.frictionTorque = lowerArmLowFriction
                }
            case BodyPart.RightHand:
                if vector != .zero {
                    if checkRightArmsStraight() && rightArmsGotClosureAgain {
                        joints[.jointRightUpperRightLower]!.frictionTorque = lowerArmHighFriction
                    } else {
                        joints[.jointRightUpperRightLower]!.frictionTorque = lowerArmLowFriction
                    }
                } else {
                    rightArmsGotClosureAgain = false
                    joints[.jointRightUpperRightLower]!.frictionTorque = lowerArmLowFriction
                }
            default:
                break
            }
        }
        
        // used to let the arms bent again if the character is at the corner of an object and somehow needs to "wrap around" the corner
        if rightHandGrabbing && distanceBetweenPoints(start: bodyParts[.Body]!.position, end: bodyParts[.LeftUpperArm]!.position) < upperArmBodyDistanceLB {
            leftArmsGotClosureAgain = false
            joints[.jointLeftUpperLeftLower]!.frictionTorque = lowerArmLowFriction
        }
        if leftHandGrabbing && distanceBetweenPoints(start: bodyParts[.Body]!.position, end: bodyParts[.RightUpperArm]!.position) < upperArmBodyDistanceLB {
            rightArmsGotClosureAgain = false
            joints[.jointRightUpperRightLower]!.frictionTorque = lowerArmLowFriction
        }
        
        
        //MARK: logic for varying forces
        if GameScene.level == 1 {
            if leftHandGrabbing || rightHandGrabbing {
                forceDiagonal = groundMovingForce
            } else {
                forceDiagonal = groundStuckForce
            }
        }
   
        if GameScene.level == 2 {
            if characterWantsToReachObs() && !isTutorial {
                forceDiagonal = reachObjectForce
                //debugPrint("character wants to reach object, reach object force")
            } else {
                if leftHandGrabbing || rightHandGrabbing {
                    forceDiagonal = groundMovingForce
                } else {
                    forceDiagonal = groundStuckForce
                }
            }
        }
        
        if leftHandGrabbedCharacter || rightHandGrabbedCharacter {
            forceDiagonal = groundMovingForce * 2
        }
        
        // limit the force that can be applied to the hands
        let newVec = normalizeForce(vector: vector)
         
        guard !bodyPartsToMove.isEmpty else { return }
        let copyBodyPartsToMove = bodyPartsToMove
        for bp in copyBodyPartsToMove {
            let action = SKAction.applyForce(newVec, duration: 1/60)
            // hands are SKSpriteNodes, body is SKShapeNode
            if bp == .Body {
                (bodyParts[bp] as! SKShapeNode).run(action)
            }
            else {
                (bodyParts[bp] as! SKSpriteNode).run(action)
            }
        }
        
    }
    
    
    /*
     * these functions are used to keep the arms straight while moving
     */
    func checkLeftArmsStraight() -> Bool {
        if distanceBetweenPoints(start: bodyParts[.LeftLowerArm]!.position, end: bodyParts[.LeftUpperArm]!.position) > armDistance {
            return true
        }
        return false
    }
    
    func checkRightArmsStraight() -> Bool {
        if distanceBetweenPoints(start: bodyParts[.RightLowerArm]!.position, end: bodyParts[.RightUpperArm]!.position) > armDistance {
            return true
        }
        return false
    }
    
    func checkLeftArmsClose() -> Bool {
        if distanceBetweenPoints(start: bodyParts[.LeftLowerArm]!.position, end: bodyParts[.LeftUpperArm]!.position) < armDistance {
            return true
        }
        return false
    }
    
    func checkRightArmsClose() -> Bool {
        if distanceBetweenPoints(start: bodyParts[.RightLowerArm]!.position, end: bodyParts[.RightUpperArm]!.position) < armDistance {
            return true
        }
        return false
    }
    
    /*
     * this function checks if a character is below an obstacle.
     * in that case, character should be more force so that it can reach upward
     */
    func characterWantsToReachObs() -> Bool {
        for point in Character.obstacleLowPoints {
            // ensure that body is below the low point
            if point != .zero {
                if abs(point.y - convert(bodyParts[.Body]!.position, to: self.parent!).y) < 0.6 * handToHandDistance  &&
                    abs(point.x - convert(bodyParts[.Body]!.position, to: self.parent!).x) < 0.6 * handToHandDistance  &&
                    point.y - convert(bodyParts[.Body]!.position, to: self.parent!).y > 0
                    && (rightHandGrabbing || leftHandGrabbing) {
                    return true
                }
            }
        }
        return false
    }
    
    /*
     * make the character not applied by gravity
     */
    func turnOffGravity() {
        let bodyPartsKeys = bodyParts.keys
        for key in bodyPartsKeys {
            if let bodyPart = bodyParts[key] {
            bodyPart.physicsBody?.affectedByGravity = false
            }
        }
    }
    
    /*
     * computes distance between two points
     */
    func distanceBetweenPoints(start: CGPoint, end: CGPoint) -> CGFloat {
        return sqrt(start.x - end.x) * (start.x - end.x) + (start.y - end.y) * (start.y - end.y)
    }

   
    /*
     * function that transfers various size of the vector into a normalized size
     */
    func normalizeForce(vector: CGVector) -> CGVector {
        // first parameter should be dy
        if vector == .zero {
            return .zero
        }
        let angle = atan2(vector.dy, vector.dx)
        return CGVector(dx: forceDiagonal * cos(angle), dy: forceDiagonal * sin(angle))
    }
    
    // makes a body part dynamic
    func makeBodyPartDynamic(bodyPart : BodyPart) {
        bodyParts[bodyPart]!.physicsBody?.isDynamic = true
    }
    // makes a body part static
    func makeBodyPartStatic(bodyPart : BodyPart) {
        bodyParts[bodyPart]!.physicsBody?.isDynamic = false
    }
    
    // makes the whole character static
    func makeCharacterStatic() {
        let bodyPartsKeys = bodyParts.keys
        for key in bodyPartsKeys {
            if let bp = bodyParts[key] as? SKShapeNode {
                bp.physicsBody?.isDynamic = false
            }
            if let bp = bodyParts[key] as? SKSpriteNode {
                bp.physicsBody?.isDynamic = false
            }
        }
    }
    // makes the whole character dynamic
    func makeCharacterDynamic() {
        let bodyPartsKeys = bodyParts.keys
        for key in bodyPartsKeys {
            if let bp = bodyParts[key] as? SKShapeNode {
                bp.physicsBody?.isDynamic = true
            }
            if let bp = bodyParts[key] as? SKSpriteNode {
                bp.physicsBody?.isDynamic = true
            }
        }
    }
    
    /*
     * execute grab interaction with an obstacle on a hand
     */
    func makeHandGrabbingObstacle(hand: BodyPart) {
        switch hand {
        case .LeftHand:
            leftHandJustGrabbed = true
            leftHandGrabbing = true
            makeBodyPartStatic(bodyPart: .LeftHand)
        case .RightHand:
            rightHandJustGrabbed = true
            rightHandGrabbing = true
            makeBodyPartStatic(bodyPart: .RightHand)
        default:
            return
        }
    }
    
    /*
     * releases grab interaction with an obstacle on a hand
     */
    func makeHandReleaseFromObstacle(hand: BodyPart) {
        switch hand {
        case .LeftHand:
            if leftHandJustGrabbed {
                leftHandJustGrabbed = false
            }
            leftHandGrabbing = false
            leftHandContacted = false //added this because character kept grabbing in the air
            makeBodyPartDynamic(bodyPart: .LeftHand)
        case .RightHand:
            if leftHandJustGrabbed {
                leftHandJustGrabbed = false
            }
            rightHandGrabbing = false
            rightHandContacted = false //added this because character kept grabbing in the air
            makeBodyPartDynamic(bodyPart: .RightHand)
        default:
            return
        }
    }
    
    
    // changes the appearance of the hand
    func openLeftHand() {
        (bodyParts[.LeftHand] as! SKSpriteNode).texture = leftHandOpenTexture
        if isTutorial { bodyParts[.LeftHand]?.setScale(0.7) }
    }
    
    // changes the appearance of the hand
    func closeLeftHand() {
        (bodyParts[.LeftHand] as! SKSpriteNode).texture = leftHandCloseTexture
        if isTutorial { bodyParts[.LeftHand]?.setScale(0.55) }
    }
    
    // changes the appearance of the hand
    func openRightHand() {
        (bodyParts[.RightHand] as! SKSpriteNode).texture = righttHandOpenTexture
        if isTutorial { bodyParts[.RightHand]?.setScale(0.7) }
    }
    
    // changes the appearance of the hand
    func closeRightHand() {
        (bodyParts[.RightHand] as! SKSpriteNode).texture = rightHandCloseTexture
        if isTutorial { bodyParts[.RightHand]?.setScale(0.55) }
    }
    
    // Generate the data for sending
    func generateCharacterPhysicsSyncData(vector: CGVector) -> CharacterPhysics {
        let physicsData: CharacterPhysics = CharacterPhysics(
            bodyPositionX: bodyParts[.Body]!.position.x,
            bodyPositionY: bodyParts[.Body]!.position.y,
            leftUpperArmPositionX: bodyParts[.LeftUpperArm]!.position.x,
            leftUpperArmPositionY: bodyParts[.LeftUpperArm]!.position.y,
            leftLowerArmPositionX: bodyParts[.LeftLowerArm]!.position.x,
            leftLowerArmPositionY: bodyParts[.LeftLowerArm]!.position.y,
            leftHandPositionX: bodyParts[.LeftHand]!.position.x,
            leftHandPositionY: bodyParts[.LeftHand]!.position.y,
            rightUpperArmPositionX: bodyParts[.RightUpperArm]!.position.x,
            rightUpperArmPositionY: bodyParts[.RightUpperArm]!.position.y,
            rightLowerArmPositionX: bodyParts[.RightLowerArm]!.position.x,
            rightLowerArmPositionY: bodyParts[.RightLowerArm]!.position.y,
            rightHandPositionX: bodyParts[.RightHand]!.position.x,
            rightHandPositionY: bodyParts[.RightHand]!.position.y,
            vector: vector,
            playerID: self.playerIndex
        )
        return physicsData
    }
    
    
    /* server uses this function to process grab logic according
     * to the grab button interaction information received by client
     */
    func applyReceivedGrabInfo(grabInfo : GrabInfo, playerIndex : Int) {
        //let action = SKAction.move(to: grabInfo.grabPosition, duration: 0)
        if grabInfo.fromClient {
            
            //process hand appearance change
            if grabInfo.forChangingHandAppearance {
                if grabInfo.grabHand == .LeftHand {
                    if grabInfo.touchStatus == .TouchBegan {
                        closeLeftHand()
                        leftGrabButtonPressed = true
                    } else {
                        openLeftHand()
                        leftGrabButtonPressed = false
                    }
                } else if grabInfo.grabHand == .RightHand {
                    if grabInfo.touchStatus == .TouchBegan {
                        closeRightHand()
                        rightGrabButtonPressed = true
                    } else {
                        openRightHand()
                        rightGrabButtonPressed = false
                    }
                }
            }
            
            // apply obstacle grab information from clients
            if grabInfo.grabbedObstacle && !grabInfo.forChangingHandAppearance{
                if let newHandPosition = grabInfo.grabPosition {
                    // move the hand to the position where the grab happend
                    let action = SKAction.move(to: newHandPosition, duration: 0)
                    if grabInfo.grabHand == .LeftHand {
                        bodyParts[.LeftHand]?.run(action)
                        makeHandGrabbingObstacle(hand: .LeftHand)
                    } else {
                        bodyParts[.RightHand]?.run(action)
                        makeHandGrabbingObstacle(hand: .RightHand)
                    }
                }
            }
            
            // apply grab release
            if !grabInfo.grabbedObstacle && !grabInfo.forChangingHandAppearance {
                if grabInfo.grabHand == .LeftHand {
                    makeHandReleaseFromObstacle(hand: .LeftHand)
                } else {
                    makeHandReleaseFromObstacle(hand: .RightHand)
                }
            }
        
        }
    }
    
    // Apply new positions from receivedData
    func apply(physicsData: CharacterPhysics) {
        
        bodyParts[.Body]!.position = CGPoint(x: physicsData.bodyPositionX, y: physicsData.bodyPositionY)
        bodyParts[.LeftHand]!.position = CGPoint(x: physicsData.leftHandPositionX, y: physicsData.leftHandPositionY)
        bodyParts[.RightHand]!.position = CGPoint(x: physicsData.rightHandPositionX, y:physicsData.rightHandPositionY)
        bodyParts[.LeftUpperArm]!.position = CGPoint(x: physicsData.leftUpperArmPositionX, y: physicsData.leftUpperArmPositionY)
        bodyParts[.LeftLowerArm]!.position = CGPoint(x: physicsData.leftLowerArmPositionX, y: physicsData.leftLowerArmPositionY)
        bodyParts[.RightUpperArm]!.position = CGPoint(x: physicsData.rightUpperArmPositionX, y: physicsData.rightUpperArmPositionY)
        bodyParts[.RightLowerArm]!.position = CGPoint(x: physicsData.rightLowerArmPositionX, y: physicsData.rightLowerArmPositionY)
    }
    
    // fix joints on character
    func makeJointsNotRotate() {
        let bodyPartsKeys = bodyParts.keys
        for key in bodyPartsKeys {
            if let bp = bodyParts[key] {
                bp.physicsBody?.allowsRotation = false
            }
        }
    }
    
    // allow joint rotaion on character
    func makeJointsRotate() {
        let bodyPartsKeys = bodyParts.keys
        for key in bodyPartsKeys {
            if let bp = bodyParts[key] {
                bp.physicsBody?.allowsRotation = true
            }
        }
    }
    
    // MARK: - CHaracter Customization
    private var characterCustomization: [UIImage]? = nil
    func apply(customization: [UIImage]? ) {
        guard customization != nil else { return }
        guard customization!.count == 9 else { return }
        characterCustomization = customization
        var i = 0 
        for part in BodyPart.allCases {
            let texture : SKTexture! = SKTexture.init(image: customization![i])
            if let _ = bodyParts[part] as? SKShapeNode {
                (bodyParts[part] as! SKShapeNode).strokeColor = UIColor.clear
                (bodyParts[part] as! SKShapeNode).fillTexture = texture
            } else if let _ = bodyParts[part] as? SKSpriteNode {
                (bodyParts[part] as! SKSpriteNode).color = UIColor.clear
                (bodyParts[part] as! SKSpriteNode).texture = texture
            } else {
                print("Could not configure body part: \(part)")
            }
            
            i += 1
        }
        
        leftHandOpenTexture = SKTexture(image: customization![3])
        leftHandCloseTexture = SKTexture(image: customization![7])
        righttHandOpenTexture = SKTexture(image: customization![6])
        rightHandCloseTexture = SKTexture(image: customization![8])

        bodyParts[.LeftHand]?.zPosition = 10
        bodyParts[.RightHand]?.zPosition = 10
    }
    
    func applyPreLoadTextures(for playerIndex: Int) {
        let startIndex = playerIndex * 9
        let textures = CharacterManager.sharedInstance.preloadedTextures[startIndex ..< startIndex + 9]
        var i = startIndex
        for part in BodyPart.allCases {
            let texture : SKTexture = textures[i]
            if let _ = bodyParts[part] as? SKShapeNode {
                (bodyParts[part] as! SKShapeNode).strokeColor = UIColor.clear
                (bodyParts[part] as! SKShapeNode).fillTexture = texture
            } else if let _ = bodyParts[part] as? SKSpriteNode {
                (bodyParts[part] as! SKSpriteNode).color = UIColor.clear
                (bodyParts[part] as! SKSpriteNode).texture = texture
            } else {
                print("Could not configure body part: \(part)")
            }
            
            i += 1
        }
        
        leftHandOpenTexture = textures[startIndex + 3]
        leftHandCloseTexture = textures[startIndex + 7]
        righttHandOpenTexture = textures[startIndex + 6]
        rightHandCloseTexture = textures[startIndex + 8]
            
            
        bodyParts[.LeftHand]?.zPosition = 10
        bodyParts[.RightHand]?.zPosition = 10
    }
    
    func setDefaultHands() {
        leftHandOpenTexture = SKTexture(imageNamed: "lefthand.png")
        leftHandCloseTexture = SKTexture(imageNamed: "lefthandGrab.png")
        righttHandOpenTexture = SKTexture(imageNamed: "righthand.png")
        rightHandCloseTexture = SKTexture(imageNamed: "righthandGrab.png")
    }
    

}

// Class for Factory Method
class CharacterCreater {
    // shared instance
    static let sharedCharacterCreator = CharacterCreater()
    
    init() {
    }
    
    /* Function which initializes an instance of Character with given properties and return a character instance
    * @param    scene: the scene where the character will be placed at
    *           color: color of the character body
    *           bodyRadius: determines the size of the character
    *           position: determines the position of the character, in scene's coordinate
    *
    * @return   a character instance
    */
    func createCharacter(scene : SKScene, color : BodyColor, bodyRadius : CGFloat, position : CGPoint, playerIndex: Int, isMyChracter: Bool, isTutorial: Bool) -> Character {
        return Character(scene: scene, color: color, bodyRadius: bodyRadius, position: position, playerIndex: playerIndex, isMyCharacter: isMyChracter, isTutorial: isTutorial)
    }
}
