//
//  Timer.swift
//  Better Together
//
//  Created by Chan Yong Lee on 05.01.21.
//

import UIKit
import SpriteKit

class Timer: SKLabelNode {
    // variables for saving minute, second, and milliseconds
    var minute : Int = 0
    var second : Int = 0
    var milliSecond : Double = 0
    
    var timerRunning : Bool = false
    
    var minuteText : String = "00" {
        didSet{
            if let label = minuteLabelNode {
                label.text = minuteText
            }
        }
    }
    var secondText : String = ":00:" {
        didSet{
            if let label = secondLabelNode {
                label.text = secondText
            }
        }
    }
    var milliSecondText : String = "00" {
        didSet {
            if let label = milliSecondLabelNode {
                label.text = milliSecondText
            }
        }
    }
    
    var minuteLabelNode : SKLabelNode?
    var secondLabelNode : SKLabelNode?
    var milliSecondLabelNode : SKLabelNode?
    
    override var position: CGPoint {
        didSet {
            // calculate the position of the label nodes depending on the font size
            secondLabelNode?.position = .zero
            minuteLabelNode?.position = CGPoint(x: secondLabelNode!.position.x - secondLabelNode!.fontSize * 1.4, y: secondLabelNode!.position.y)
            milliSecondLabelNode?.position = CGPoint(x: secondLabelNode!.position.x + secondLabelNode!.fontSize * 1.4, y: secondLabelNode!.position.y)
        }
    }
    
    override var fontName: String? {
        didSet {
            minuteLabelNode?.fontName = self.fontName
            secondLabelNode?.fontName = self.fontName
            milliSecondLabelNode?.fontName = self.fontName
        }
    }
    override var fontColor: UIColor? {
        didSet {
            minuteLabelNode?.fontColor = self.fontColor
            secondLabelNode?.fontColor = self.fontColor
            milliSecondLabelNode?.fontColor = self.fontColor
        }
    }
    override var fontSize: CGFloat {
        didSet {
            minuteLabelNode?.fontSize = self.fontSize
            secondLabelNode?.fontSize = self.fontSize
            milliSecondLabelNode?.fontSize = self.fontSize
            
            // reset the postions of the label nodes, because the font size has changed
            secondLabelNode?.position = .zero
            minuteLabelNode?.position = CGPoint(x: secondLabelNode!.position.x - secondLabelNode!.fontSize * 1.4, y: secondLabelNode!.position.y)
            milliSecondLabelNode?.position = CGPoint(x: secondLabelNode!.position.x + secondLabelNode!.fontSize * 1.4, y: secondLabelNode!.position.y)
        }
    }
    
    
    override init() {
        super.init()
        
        minuteLabelNode = SKLabelNode(text: minuteText)
        secondLabelNode = SKLabelNode(text: secondText)
        milliSecondLabelNode = SKLabelNode(text: milliSecondText)
        
        addChild(minuteLabelNode!)
        addChild(secondLabelNode!)
        addChild(milliSecondLabelNode!)
        
        secondLabelNode?.position = .zero
        minuteLabelNode?.position = CGPoint(x: secondLabelNode!.position.x - secondLabelNode!.fontSize * 1.4, y: secondLabelNode!.position.y)
        milliSecondLabelNode?.position = CGPoint(x: secondLabelNode!.position.x + secondLabelNode!.fontSize * 1.4, y: secondLabelNode!.position.y)
    }
    
    func updateTimer() {
        if timerRunning{
            milliSecond += 100/60
            if Int(milliSecond) == 100 {
                second += 1
                milliSecond = 0
                if second == 60 {
                    minute += 1
                    second = 0
                }
            }
            
            if milliSecond == 0 {
                milliSecondText = "00"
            }
            else if milliSecond < 10 {
                milliSecondText = "0" + String(Int(milliSecond))
            }
            else {
                milliSecondText = String(Int(milliSecond))
            }
            
            if second == 0 {
                secondText = ":00:"
            }
            else if second < 10 {
                secondText = ":0" + String(second) + ":"
            }
            else {
                secondText = ":" + String(second) + ":"
            }
            
            if minute == 0 {
                minuteText = "00"
            }
            else if minute < 10 {
                minuteText = "0" + String(minute)
            }
            else {
                minuteText = String(minute)
            }
        }
    }
    
    func getTotalDuration() -> String {
        return minuteText + secondText + milliSecondText
    }
    
    func startTimer() {
        timerRunning = true
    }
    func stopTimer(){
        timerRunning = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
