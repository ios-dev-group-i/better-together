//
//  Obstacle.swift
//  Better Together
//
//  Define protocol, properties and classes for Obstacles in Scene.
//
//  Created by Eunae Jang on 2021/01/05.
//

import SpriteKit

/// enum - ObstacleType: define Obstacle types
enum ObstacleType {
    case StartPoint
    case EndPoint
    case Normal
}

enum ObstacleDirection {
    case horizontal
    case vertical
}

extension UIColor {
    struct ObstacleColor {
        static var endPointColor: UIColor { return UIColor(red: 0.99, green: 0.32, blue: 0.32, alpha: 1.00) }
        static var normalColor: UIColor { return UIColor(red: 0.29, green: 0.33, blue: 0.41, alpha: 1.00) }
    }
}

/// Class - Obstacle: customized SKShapeNode that inherits SKShapeNode
class Obstacle: SKShapeNode {
    var type: ObstacleType = .Normal {
        didSet {
            if type == .EndPoint {
                self.fillColor = UIColor.ObstacleColor.endPointColor
            } else {
                if self.texture != nil {
                    self.fillColor = UIColor.white
                    self.fillTexture = self.texture
                } else {
                    self.fillColor = UIColor.ObstacleColor.normalColor
                }
            }
            lineWidth = 0
        }
    }
    
    /// Obstacle's physicsBody
    override var physicsBody: SKPhysicsBody? {
        didSet {
            self.physicsBody?.isDynamic = false
            
            /// let any obstacle can be grabbed by characters
            self.physicsBody?.contactTestBitMask = Character.ContactType.ObstacleInteraction.rawValue
        }
    }
    
    var direction : ObstacleDirection?
    var texture: SKTexture?
    
    /// check if the point is inside the endPoint obstacle area
    func containsPoint(obstaclePositionInparent: CGPoint, point: CGPoint) -> Bool {
        guard type == .EndPoint else { return false }
        
        let arcRadius: CGFloat = self.frame.size.width * 0.5
        let arc = UIBezierPath()
        arc.addArc(withCenter:  obstaclePositionInparent, radius: arcRadius, startAngle: CGFloat(0.0), endAngle: CGFloat(Float.pi), clockwise: false)
        arc.addLine(to: CGPoint(x: self.frame.minX, y: self.frame.maxY))
        arc.close()
        
        return arc.contains(point)
    }
    
    func isVertical() -> Bool {
        return direction == ObstacleDirection.vertical
    }
}

/// Protocol for creating different shapes of obstacles
protocol ObstacleProtocol {
    func createRectObstacle(rectSize size: CGSize, type: ObstacleType, texture: SKTexture?) -> Obstacle
    func createCircleObstacle(radius: CGFloat, type: ObstacleType, texture: SKTexture?) -> Obstacle
    func createPathObstacle(path: CGPath, type: ObstacleType, texture: SKTexture?) -> Obstacle
}

/// Implement ObstacleProtocol and define properties according to Obstacle shape
class ObstacleCreator: ObstacleProtocol {
    static let sharedInstance = ObstacleCreator()
    
    /// Create different shapes of obstacle
    func createRectObstacle(rectSize size: CGSize, type: ObstacleType, texture: SKTexture?) -> Obstacle {
        let obstacle = Obstacle(rectOf: size)
        
        obstacle.physicsBody = SKPhysicsBody(rectangleOf: obstacle.frame.size)
        obstacle.texture = texture
        obstacle.type = type
        
        if size.width <= size.height {
            obstacle.direction = .vertical
        }
        else{
            obstacle.direction = .horizontal
        }
        
        return obstacle
    }
    
    func createCircleObstacle(radius: CGFloat, type: ObstacleType, texture: SKTexture?) -> Obstacle {
        let obstacle = Obstacle(circleOfRadius: radius)
        
        obstacle.physicsBody = SKPhysicsBody(circleOfRadius: radius)
        obstacle.texture = texture
        obstacle.type = type
        obstacle.direction = .vertical
        
        return obstacle
    }
    
    func createPathObstacle(path: CGPath, type: ObstacleType, texture: SKTexture?) -> Obstacle {
        let obstacle = Obstacle(path: path)
        
        obstacle.physicsBody = SKPhysicsBody(polygonFrom: path)
        obstacle.texture = texture
        obstacle.type = type
        
        if obstacle.frame.width <= obstacle.frame.height {
            obstacle.direction = .vertical
        } else {
            obstacle.direction = .horizontal
        }

        return obstacle
    }
}
