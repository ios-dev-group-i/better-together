# `Character.swift`

## Logis for keeping the arms straight

Flags needed: set to true at the beginning as field variables 
```swift
var leftArmsGotClosureAgain = true
var rightArmsGotClosureAgain = true
```

Variables needed:
```swift
var armDistance : CGFloat = 0
var upperArmBodyDistanceLB : CGFloat = 0
```
`armDistance` should be calculated in `initializeBodyParts(bodyRadius radius: CGFloat)`:
`upperArmBodyDistanceLB` is used to decide whether body and arms are close to each other so that the arms should not be kept straight anymore.
This value should also be calculated in `initializeBodyParts(bodyRadius radius: CGFloat)`:
```swift
func initializeBodyParts(bodyRadius radius: CGFloat) {
  
    // calculate size of the arms and hands relatively to the size of the body
    let upperArmWidth = radius
    let armHeight = radius * 0.25
    let lowerArmWidth = radius * 1.25
    let handRadius = armHeight
    forceDiagonal = radius * 2
    upperArmBodyDistanceLB = radius * 1.6
    
    bodyParts[.Body] = SKShapeNode(circleOfRadius: radius)
    bodyParts[.LeftUpperArm] = SKShapeNode(rectOf: CGSize(width: upperArmWidth, height: armHeight))
    bodyParts[.LeftLowerArm] = SKShapeNode(rectOf: CGSize(width: lowerArmWidth, height: armHeight))
    bodyParts[.LeftHand]  = SKShapeNode(circleOfRadius: handRadius)
    bodyParts[.RightUpperArm] = SKShapeNode(rectOf: CGSize(width: upperArmWidth, height: armHeight))
    bodyParts[.RightLowerArm] = SKShapeNode(rectOf: CGSize(width: lowerArmWidth, height: armHeight))
    bodyParts[.RightHand]  = SKShapeNode(circleOfRadius: handRadius)
    
    armDistance = ((upperArmWidth + lowerArmWidth) / 2) * 1.1
}
```



In `moveBodyPart()`: it requires that the `moveBodyPart()` is called in every `update()`  function in `GameScene.swift` 
```swift
// check if arms are close to each other again
if checkLeftArmsClose() {
    leftArmsGotClosureAgain = true
}
if checkRightArmsClose() {
    rightArmsGotClosureAgain = true
}

// let the arms straight once they get straight
switch bodyPart {
    case bodyParts[.LeftHand]:
        if vector != .zero {
            // keep the arms straight only if got close once again
            if checkLeftArmsStraight() && leftArmsGotClosureAgain {
                joints[.jointLeftUpperLeftLower]!.frictionTorque = 1.0
            }
        }
        else{
            // the straightened arms should be able to be bended again
            leftArmsGotClosureAgain = false
            joints[.jointLeftUpperLeftLower]!.frictionTorque = 0.0
        }
    case bodyParts[.RightHand]:
        if vector != .zero {
            if checkRightArmsStraight() && rightArmsGotClosureAgain {
                joints[.jointRightUpperRightLower]!.frictionTorque = 1.0
            }
        }
        else {
            rightArmsGotClosureAgain = false
            joints[.jointRightUpperRightLower]!.frictionTorque = 0.0
        }
    default:
        debugPrint("nth happend")
    }
    
    // used to let the arms bent again if the character is at the corner of an object and somehow needs to "wrap around" the corner
    if rightHandGrabbing && CGPointDistance(from: bodyParts[.Body]!.position, to: bodyParts[.LeftUpperArm]!.position) < upperArmBodyDistanceLB {
        leftArmsGotClosureAgain = false
        joints[.jointLeftUpperLeftLower]!.frictionTorque = 0.01
    }
    if leftHandGrabbing && CGPointDistance(from: bodyParts[.Body]!.position, to: bodyParts[.RightUpperArm]!.position) < upperArmBodyDistanceLB {
        rightArmsGotClosureAgain = false
        joints[.jointRightUpperRightLower]!.frictionTorque = 0.01
    }
}
```

Auxilirary functions needed are:
```swift
/*
 * these functions are used to keep the arms straight while moving
 */
func checkLeftArmsStraight() -> Bool {
    if distanceBetweenPoints(start: bodyParts[.LeftLowerArm]!.position, end: bodyParts[.LeftUpperArm]!.position) > armDistance {
        return true
    }
    return false
}

func checkRightArmsStraight() -> Bool {
    if distanceBetweenPoints(start: bodyParts[.RightLowerArm]!.position, end: bodyParts[.RightUpperArm]!.position) > armDistance {
        return true
    }
    return false
}

func checkLeftArmsClose() -> Bool {
    if distanceBetweenPoints(start: bodyParts[.LeftLowerArm]!.position, end: bodyParts[.LeftUpperArm]!.position) < armDistance {
        return true
    }
    return false
}

func checkRightArmsClose() -> Bool {
    if distanceBetweenPoints(start: bodyParts[.RightLowerArm]!.position, end: bodyParts[.RightUpperArm]!.position) < armDistance {
        return true
    }
    return false
}

/*
 * computes distance between two points
 */
func distanceBetweenPoints(start: CGPoint, end: CGPoint) -> CGFloat {
    return sqrt(start.x - end.x) * (start.x - end.x) + (start.y - end.y) * (start.y - end.y)
}
```
##

