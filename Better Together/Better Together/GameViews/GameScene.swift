//
//  GameScene.swift
//  Better Together
//
//  Created by Chan Yong Lee on 01.01.21.
//

import SpriteKit
import GameplayKit
import MultipeerConnectivity

class GameScene: SKScene, SKPhysicsContactDelegate {
    // MARK: - Nodes
    // saves characters from each players
    var characters : [Character] = []
    // the movement data of each characters are saved as variables, so that small delay between the latest received movement data and the new movement data doens't result into bouncy behaviour of character
    var characterMovementData : [CGVector] = []
    
    private let obstacleCreator: ObstacleCreator = ObstacleCreator.sharedInstance
    var obstacles : [String : SKNode] = [:]
    var goalObstacle: Obstacle?
    
    static var level: Int {
        get {
            return LevelSelectionViewController.selectedLevel
        }
    }
    //MARK: - grab control
    var leftGrabButtonPressed = false {
        didSet {
            if leftGrabButtonPressed {
                showGreyBox(side: Character.BodyPart.LeftHand)
            } else {
                hideGreyBox(side: Character.BodyPart.LeftHand)
            }
        }
    }
    var rightGrabButtonPressed = false {
        didSet {
            if rightGrabButtonPressed {
                showGreyBox(side: Character.BodyPart.RightHand)
            } else {
                hideGreyBox(side: Character.BodyPart.RightHand)
            }
        }
    }
    var leftGreyBox : SKShapeNode?
    var rightGreyBox : SKShapeNode?
    var greyBoxLeftPosition : CGPoint = .zero
    var greyBoxRightPosition : CGPoint = .zero
    
    // MARK: - My character
    var mySelf: Player = UserDefaults.standard.mySelf
    var myCharacterIndex: Int = 0
    var characterInGoalList: [Bool] = []
    
    // MARK: - Multiplay mode
    let localTeamPlayManager = LocalTeamPlayManager.sharedInstance
    static var multiplayerMode = false
    static var isServer = false
    var gameStarted = false
    var receivedCharactersAction: [[CharacterActionData]] = []
    var canAppendReceivedCharacterAction = true
    let maxQueueCount: Int = 10
    
    // MARK: - Multiplyaer grab interaction
    /// client: saves the movement data and grab appearance change information which have to be sent to server
    var reliableActionsToSend : CharacterActionData?
    var unreliableActionsToSend : CharacterActionData?
    
    // struct that indicates the character index and body part index that is involved in contact between characters
    struct CharacterAndBodypart : Hashable {
        var characterIndex : Int
        var bodyPart : Character.BodyPart
    }
    // struct that indicates which characters are involved in a contact between characters
    struct CharacterPair : Hashable {
        var character1 : CharacterAndBodypart
        var character2 : CharacterAndBodypart
        
        static func ==(lhs: CharacterPair, rhs: CharacterPair) -> Bool {
            return (lhs.character1.bodyPart.rawValue == rhs.character1.bodyPart.rawValue &&
                lhs.character1.characterIndex == rhs.character1.characterIndex &&
                lhs.character2.bodyPart.rawValue == rhs.character2.bodyPart.rawValue &&
                lhs.character2.characterIndex == rhs.character2.characterIndex) ||
                // sequence should not matter
                (lhs.character1.bodyPart.rawValue == rhs.character2.bodyPart.rawValue &&
                    lhs.character1.characterIndex == rhs.character2.characterIndex &&
                    lhs.character2.bodyPart.rawValue == rhs.character1.bodyPart.rawValue &&
                    lhs.character2.characterIndex == rhs.character1.characterIndex
                )
        }
        
        func hash(into hasher: inout Hasher) {
                hasher.combine(character1)
                hasher.combine(character2)
        }
 
    }
    
    /*
    * Saves all the body parts of all characters
    * key: struct
    *       first element : character index
    *       second element: index of body part
    * value : actual bodypart node
    */
    var allCharactersBodyParts : [CharacterAndBodypart : SKNode] = [ : ]
    var reversedAllCharactersBodyParts : [SKNode : CharacterAndBodypart] = [ : ]
    // saves joints between characters' body parts
    var joints : [CharacterPair : SKPhysicsJoint] = [:]
    // saves flags that determin whether a joint can be added
    var jointsAddedFlag : [CharacterPair : Bool] = [:]
    // saved flags that are used to determine whether a grab interaction between characters should be processed
    var bodyPartsContactFlags : [CharacterPair : Bool] = [:]
    // let the clients know that his character has grabbed another character so that they can make grab sound
    static var shouldSendLeftHandGrabSound : [Bool] = []
    static var shouldSendRightHandGrabSound : [Bool] = []
    
    // variables for controlling gyroscope alert message
    var calibrationNode : SKLabelNode?
    var showCalibrationMessage = false
    var alertShowTimer : Int = 0
    var alertShowCounter : Int = 0
    
    //MARK: - GameScene deinit
    deinit {
        debugPrint("Game scene got deallocated.")
    }
    
    override func didMove(to view: SKView) {
        
        view.isMultipleTouchEnabled = true
        //add camera
        let cameraNode = SKCameraNode()
        cameraNode.position = CGPoint(x: 0, y: 0)
        addChild(cameraNode)
        camera = cameraNode
        camera?.setScale(3.0)

        // for contaction detecting
        physicsWorld.contactDelegate = self
        
        // background
        let backgroundNode = SKSpriteNode(imageNamed: "level\(GameScene.level)")
        backgroundNode.zPosition = -100
        backgroundNode.setScale(4.0)
        addChild(backgroundNode)
        
        // sycn GameScene loading state
        if GameScene.multiplayerMode && !GameScene.isServer {
            updateGameSceneLoadState()
        }
        
        // add character manually when in single player mode
        if !GameScene.multiplayerMode {
            
            characters.append(CharacterCreater.sharedCharacterCreator.createCharacter(scene: self,
                                                                                      color: BodyColor(color: UIColor.random),
                                                                                     bodyRadius: 15,
                                                                                     position: CGPoint(x: frame.minX + CGFloat(200 * (1 + myCharacterIndex)), y: frame.maxY),
                                                                                     playerIndex: myCharacterIndex,
                                                                                     isMyChracter: true,
                                                                                     isTutorial: false))
            
            DispatchQueue.main.async {
                let configAsImage = CharacterManager.getImages(for: CharacterManager.sharedInstance.getMyConfigText())
                self.characters.last?.apply(customization:configAsImage)
            }
            characterInGoalList.append(false)
            characterMovementData.append(.zero)
        }
        
        // grab button
        initGrabButtons()
        
        // initialize level obstacles
        if GameScene.level == 1 {
            initObstaclesLevel1()
        } else {
            initObstaclesLevel2()
        }
        
        // initialize calibration alert label
        initializeCalibrationAlertLabel()
    }
    
    private func initObstaclesLevel1() {
        let texture = SKTexture(imageNamed: "foresttexture")
        
        // stair
        let stairBarrier = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 30, height: 100), type: .StartPoint, texture: texture)
        let stair1 = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 400, height: 30), type: .StartPoint, texture: texture)
        let stair2 = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 300, height: 30), type: .StartPoint, texture: texture)
        let stair3 = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 300, height: 30), type: .StartPoint, texture: texture)
        let stairVertical1 = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 30, height: 300), type: .StartPoint, texture: texture)
        let stairVertical2 = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 30, height: 300), type: .StartPoint, texture: texture)
        obstacles["stair1"] = stair1
        obstacles["stair2"] = stair2
        obstacles["stair3"] = stair3
        obstacles["stairVertical1"] = stairVertical1
        obstacles["stairVertical2"] = stairVertical2
        obstacles["startBarrier"] = stairBarrier
        stair1.position = CGPoint(x: -400, y: -200)
        stairBarrier.position = CGPoint(x: stair1.frame.minX + stairBarrier.frame.width / 2, y: stair1.frame.maxY + stairBarrier.frame.height / 2)
        stairVertical1.position = CGPoint(x: stair1.frame.maxX - stairVertical1.frame.width / 2, y: stair1.frame.maxY + stairVertical1.frame.height / 2)
        stair2.position = CGPoint(x: stairVertical1.frame.maxX + stair2.frame.width / 2, y: stairVertical1.frame.maxY - stair2.frame.height / 2)
        stairVertical2.position = CGPoint(x: stair2.frame.maxX - stairVertical2.frame.width / 2, y: stair2.frame.maxY + stairVertical2.frame.height / 2)
        stair3.position = CGPoint(x: stairVertical2.frame.maxX + stair3.frame.width / 2, y: stairVertical2.frame.maxY - stair3.frame.height / 2)
        
        addChild(stairBarrier)
        addChild(stair1)
        addChild(stair2)
        addChild(stair3)
        addChild(stairVertical1)
        addChild(stairVertical2)
        
        // super c
        let superCVertical = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 80, height: 500), type: .Normal, texture: texture)
        let superCHorizontal = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 300, height: 80), type: .Normal, texture: texture)
        obstacles["superCVertical"] = superCVertical
        obstacles["superCHorizontal"] = superCHorizontal
        superCVertical.position = CGPoint(x: stair3.frame.maxX + stair3.frame.width, y: stairVertical2.position.y)
        superCHorizontal.position = CGPoint(x: superCVertical.frame.maxX + superCHorizontal.frame.width / 2 , y: superCVertical.frame.maxY - superCHorizontal.frame.height / 2)
        addChild(superCVertical)
        addChild(superCHorizontal)
        
        // add goal(end point) obstacle
        let innerArcRadius: CGFloat = size.width * 0.3 * 0.5
        let outerArcRadius: CGFloat = size.width * 0.35 * 0.5
        let arc = UIBezierPath()
        arc.addArc(withCenter:  CGPoint(x: 0, y: 0), radius: innerArcRadius, startAngle: CGFloat(0.0), endAngle: CGFloat(Float.pi), clockwise: false)
        arc.addLine(to: CGPoint(x: innerArcRadius + size.width * 0.1 * 0.5, y: 0))
        arc.addArc(withCenter: CGPoint(x:0, y:0), radius: outerArcRadius, startAngle: CGFloat(Float.pi), endAngle: CGFloat(0.0), clockwise: true)
        arc.addLine(to: CGPoint(x: -outerArcRadius + size.width * 0.1 * 0.5, y: 0))
        arc.close()
        goalObstacle = ObstacleCreator.sharedInstance.createPathObstacle(path: arc.cgPath, type: .EndPoint, texture: nil)
        goalObstacle!.position = CGPoint(x: superCHorizontal.frame.maxX + superCHorizontal.frame.width / 2, y: superCVertical.frame.midY)
        
        if let goal = goalObstacle {
            addChild(goal)
            obstacles["goal"] = goal
        }
    }
    
    private func initObstaclesLevel2() {
        let texture = SKTexture(imageNamed: "snowtexture")
        
        // start ground
        let startBarrier = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 80, height: 100), type: .StartPoint, texture: texture)
        let startGround = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 400, height: 80), type: .StartPoint, texture: texture)
        obstacles["startBarrier"] = startBarrier
        obstacles["startGround"] = startGround
        startGround.position = CGPoint(x: -400, y: -200)
        startBarrier.position = CGPoint(x: startGround.frame.minX + startBarrier.frame.width / 2, y: startGround.frame.maxY + startBarrier.frame.height / 2)
        
        let firstSlope = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 800, height: 80), type: .Normal, texture: texture)
        firstSlope.zRotation = deg2rad(degree: 30)
        firstSlope.position = CGPoint(x: 127, y: -3)
        obstacles["firstSlope"] = firstSlope
        
        let secondSlope = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 1000, height: 80), type: .Normal, texture: texture)
        secondSlope.zRotation = deg2rad(degree: -30)
        secondSlope.position.y = startGround.position.y - 200
        secondSlope.position.x = startGround.position.x + 1200
        obstacles["secondSlope"] = secondSlope
        
        let thirdSlope = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 800, height: 80), type: .Normal, texture: texture)
        thirdSlope.zRotation = deg2rad(degree: 30)
        thirdSlope.position.y = firstSlope.position.y - 200
        thirdSlope.position.x = secondSlope.position.x + 700
        obstacles["thirdSlope"] = thirdSlope

        Character.obstacleLowPoints.append(CGPoint(x: thirdSlope.frame.minX, y: thirdSlope.frame.minY))
        
        addChild(startBarrier)
        addChild(startGround)
        addChild(firstSlope)
        addChild(secondSlope)
        addChild(thirdSlope)
        
        // add goal(end point) obstacle
        let innerArcRadius: CGFloat = size.width * 0.3 * 0.5
        let outerArcRadius: CGFloat = size.width * 0.35 * 0.5
        let arc = UIBezierPath()
        arc.addArc(withCenter:  CGPoint(x: 0, y: 0), radius: innerArcRadius, startAngle: CGFloat(0.0), endAngle: CGFloat(Float.pi), clockwise: false)
        arc.addLine(to: CGPoint(x: innerArcRadius + size.width * 0.05 * 0.5, y: 0))
        arc.addArc(withCenter: CGPoint(x:0, y:0), radius: outerArcRadius, startAngle: CGFloat(Float.pi), endAngle: CGFloat(0.0), clockwise: true)
        arc.addLine(to: CGPoint(x: -outerArcRadius + size.width * 0.05 * 0.5, y: 0))
        arc.close()
        goalObstacle = ObstacleCreator.sharedInstance.createPathObstacle(path: arc.cgPath, type: .EndPoint, texture: nil)
        goalObstacle!.position.y = thirdSlope.position.y - 100
        goalObstacle!.position.x = thirdSlope.position.x + 500
        
        if let goal = goalObstacle {
            addChild(goal)
            obstacles["goal"] = goal
        }
    }
    
    // initializing elements for grab buttons
    private func initGrabButtons() {
        leftGreyBox = SKShapeNode(rectOf: CGSize(width: self.frame.width / 2, height: self.frame.height))
        rightGreyBox = SKShapeNode(rectOf: CGSize(width: self.frame.width / 2, height: self.frame.height))
        leftGreyBox!.fillColor = .black
        rightGreyBox!.fillColor = .black
        leftGreyBox!.alpha = 0
        rightGreyBox!.alpha = 0
        camera!.addChild(leftGreyBox!)
        camera!.addChild(rightGreyBox!)
        greyBoxLeftPosition = CGPoint(x: -(self.frame.width / 4), y: 0)
        greyBoxRightPosition = CGPoint(x: self.frame.width / 4, y: 0)
        leftGreyBox?.position = greyBoxLeftPosition
        rightGreyBox?.position = greyBoxRightPosition
    }
    
    // auxilirary functions for rotating obstacls
    func rad2Deg(rad : CGFloat) -> CGFloat {
        return rad * 180 / CGFloat(Double.pi)
    }
    func deg2rad(degree : CGFloat) -> CGFloat {
        return degree * CGFloat(Double.pi) / 180
    }
    
    func initializeCalibrationAlertLabel() {
        calibrationNode = SKLabelNode(text: "Gyroscope calibrated!")
        calibrationNode?.position = convert(CGPoint(x: 0, y: self.frame.width / 4), to: self.camera!)
        calibrationNode?.isHidden = true
        camera?.addChild(calibrationNode!)
    }
    
    /// add characters based on sharedPlayersInfo
    func addCharacters() {
        let playersInfo = localTeamPlayManager.codablePlayersInfo
        for player in playersInfo {
            characters.append(CharacterCreater.sharedCharacterCreator.createCharacter(scene: self,
                                                                                     color: player.characterBodyColor,
                                                                                     bodyRadius: 15,
                                                                                     position: CGPoint(x: frame.minX, y: frame.maxY  + CGFloat(200 * player.characterIndex)),
                                                                                     playerIndex: player.characterIndex,
                                                                                     isMyChracter: player.characterIndex == myCharacterIndex,
                                                                                     isTutorial: false))
            if player.characterIndex == myCharacterIndex {
                let configAsImage = CharacterManager.getImages(for: CharacterManager.sharedInstance.getMyConfigText())
                characters.last?.apply(customization: configAsImage)
            } else {
                if GameScene.isServer {
                    characters.last?.applyPreLoadTextures(for: player.characterIndex)
                } else {
                    characters.last?.applyPreLoadTextures(for: player.characterIndex)
                }
            }
            
            characterMovementData.append(.zero)
            characterInGoalList.append(false)
            GameScene.shouldSendLeftHandGrabSound.append(false)
            GameScene.shouldSendRightHandGrabSound.append(false)
            
            // initialzie a dictionary that contains all the body parts of all of the character
            if GameScene.isServer {
                if !characters.isEmpty {
                    allCharactersBodyParts[CharacterAndBodypart(characterIndex: characters.last!.playerIndex, bodyPart: .Body)] = characters.last!.bodyParts[.Body]
                    allCharactersBodyParts[CharacterAndBodypart(characterIndex: characters.last!.playerIndex, bodyPart: .LeftHand)] = characters.last!.bodyParts[.LeftHand]
                    allCharactersBodyParts[CharacterAndBodypart(characterIndex: characters.last!.playerIndex, bodyPart: .LeftLowerArm)] = characters.last!.bodyParts[.LeftLowerArm]
                    allCharactersBodyParts[CharacterAndBodypart(characterIndex: characters.last!.playerIndex, bodyPart: .LeftUpperArm)] = characters.last!.bodyParts[.LeftUpperArm]
                    allCharactersBodyParts[CharacterAndBodypart(characterIndex: characters.last!.playerIndex, bodyPart: .RightHand)] = characters.last!.bodyParts[.RightHand]
                    allCharactersBodyParts[CharacterAndBodypart(characterIndex: characters.last!.playerIndex, bodyPart: .RightLowerArm)] = characters.last!.bodyParts[.RightLowerArm]
                    allCharactersBodyParts[CharacterAndBodypart(characterIndex: characters.last!.playerIndex, bodyPart: .RightUpperArm)] = characters.last!.bodyParts[.RightUpperArm]
                }
            }
        }
        
        // initialize also reversed dictionary
        if GameScene.isServer {
            if !allCharactersBodyParts.isEmpty {
                for (key, bodyPart) in allCharactersBodyParts {
                    reversedAllCharactersBodyParts[bodyPart] = key
                }

            }
            // set all the flags that indicate whether a body part pair has contacted to false
            initializeBodyPartsContactFlags()
        }
        
        if !GameScene.isServer {
            // turn off all the gravities of charactes if this device is a client,
            // since the clients only receive position updates
            for index in 0..<characters.count {
                characters[index].turnOffGravity()
            }
            
            // initialize an array of actions that will be continuosly sent to server from client
            reliableActionsToSend = CharacterActionData(characterIndex: myCharacterIndex)
            unreliableActionsToSend = CharacterActionData(characterIndex: myCharacterIndex)
        }
    }
    
    /* This function initializes the array which saves the flag between every body parts pair in the scene.
     * Pair of body parts from same character is excluded.
     * These flags are used to determine whether a grab between characters should be applied
     */
    func initializeBodyPartsContactFlags() {
        let allCharactersBodyPartsKeys = allCharactersBodyParts.keys
        for key1 in allCharactersBodyPartsKeys {
            for key2 in allCharactersBodyPartsKeys {
                if key1.characterIndex < key2.characterIndex {
                    let bodyPartsContactFlagsKeys = bodyPartsContactFlags.keys
                    let jointsAddedFlagKeys = jointsAddedFlag.keys
                    if !bodyPartsContactFlagsKeys.contains(CharacterPair(character1: key1, character2: key2)) {
                        bodyPartsContactFlags.updateValue(false, forKey: CharacterPair(character1: key1, character2: key2))
                    }
                    if !jointsAddedFlagKeys.contains(CharacterPair(character1: key1, character2: key2)) {
                        jointsAddedFlag.updateValue(false, forKey: CharacterPair(character1: key1, character2: key2))
                    }
                }
            }
        }
    }
    
    
    /*
     * This function dynamically adds a joint between two characters
     */
    func addJointBetweenCharacters(firstBodyPart : CharacterAndBodypart, secondBodyPart : CharacterAndBodypart) {

        if let firstNode = allCharactersBodyParts[firstBodyPart], let secondNode = allCharactersBodyParts[secondBodyPart] {
            
            // differentiate collisionBitMask to prevent side effects
            firstNode.physicsBody?.collisionBitMask = Character.ContactType.collisionAvoidance.rawValue
            secondNode.physicsBody?.collisionBitMask = Character.ContactType.defaultCollision.rawValue
            
            // create new joint pin
            let newPosition = convert(firstNode.position, from: characters[firstBodyPart.characterIndex])
            let joint = SKPhysicsJointPin.joint(withBodyA: firstNode.physicsBody!, bodyB: secondNode.physicsBody!, anchor: newPosition)
            // add the new joint to the array & the physicsWorld
            self.joints[CharacterPair(character1: firstBodyPart, character2: secondBodyPart)] = joint
            self.physicsWorld.add(joint)
            // set the flag to avoid duplicated joints
            self.jointsAddedFlag[CharacterPair(character1: firstBodyPart, character2: secondBodyPart)] = true
            
            if firstBodyPart.bodyPart == .LeftHand {
                characters[firstBodyPart.characterIndex].leftHandGrabbedCharacter = true
                GameScene.shouldSendLeftHandGrabSound[firstBodyPart.characterIndex] = true
            }
            if firstBodyPart.bodyPart == .RightHand {
                characters[firstBodyPart.characterIndex].rightHandGrabbedCharacter = true
                GameScene.shouldSendRightHandGrabSound[firstBodyPart.characterIndex] = true
            }
            if secondBodyPart.bodyPart == .LeftHand {
                characters[secondBodyPart.characterIndex].leftHandGrabbedCharacter = true
                GameScene.shouldSendLeftHandGrabSound[secondBodyPart.characterIndex] = true
            }
            if secondBodyPart.bodyPart == .RightHand {
                characters[secondBodyPart.characterIndex].rightHandGrabbedCharacter = true
                GameScene.shouldSendRightHandGrabSound[secondBodyPart.characterIndex] = true
            }
            
            if GameScene.isServer {
                if secondBodyPart.characterIndex == myCharacterIndex || firstBodyPart.characterIndex == myCharacterIndex {
                    makeGrabSound()
                }
            }
        }
    }
    
    /*
     * This function dynamically removes a joint between two characters
     */
    func removeJointBetweenCharacters(firstBodyPart : CharacterAndBodypart, secondBodyPart : CharacterAndBodypart) {
        
        // remove joint from physicsWorld and from the array
        physicsWorld.remove(self.joints[CharacterPair(character1: firstBodyPart, character2: secondBodyPart)]!)
        self.jointsAddedFlag[CharacterPair(character1: firstBodyPart, character2: secondBodyPart)] = false
        self.joints[CharacterPair(character1: firstBodyPart, character2: secondBodyPart)] = nil
        
        self.jointsAddedFlag[CharacterPair(character1: firstBodyPart, character2: secondBodyPart)] = false
        
        if firstBodyPart.bodyPart == .LeftHand {
            characters[firstBodyPart.characterIndex].leftHandGrabbedCharacter = false
        }
        if firstBodyPart.bodyPart == .RightHand {
            characters[firstBodyPart.characterIndex].rightHandGrabbedCharacter = false
        }
        if secondBodyPart.bodyPart == .LeftHand {
            characters[secondBodyPart.characterIndex].leftHandGrabbedCharacter = false
        }
        if secondBodyPart.bodyPart == .RightHand {
            characters[secondBodyPart.characterIndex].rightHandGrabbedCharacter = false
        }
        
        // set the collision mask back
        if let firstNode = allCharactersBodyParts[firstBodyPart], let secondNode = allCharactersBodyParts[secondBodyPart] {
            firstNode.physicsBody?.collisionBitMask = Character.ContactType.defaultCollision.rawValue
            secondNode.physicsBody?.collisionBitMask = Character.ContactType.defaultCollision.rawValue
        }
    }
    
    //MARK: - touch input handler
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // allow touch interaction only when the all the begin-status-synchronisation was succesful in the multiplayer mode
        if GameScene.multiplayerMode {
            guard gameStarted else { return }
        }
        for t in touches {
            grabButtonTouchBegan(touchPosition: t.location(in: camera!))
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        // allow touch interaction only when the all the begin-status-synchronisation was succesful in the multiplayer mode
        if GameScene.multiplayerMode {
            guard gameStarted else { return }
        }
        for t in touches {
            grabButtonTouchMoved(touchPosition: t.location(in: camera!))
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        // allow touch interaction only when the all the begin-status-synchronisation was succesful in the multiplayer mode
        if GameScene.multiplayerMode {
            guard gameStarted else { return }
        }
        for t in touches {
            grabButtonTouchEnded(touchPosition: t.location(in: camera!))
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        // allow touch interaction only when the all the begin-status-synchronisation was succesful in the multiplayer mode
        if GameScene.multiplayerMode {
            guard gameStarted else { return }
        }
        for t in touches {
            grabButtonTouchEnded(touchPosition: t.location(in: camera!))
        }
    }
    
    func grabButtonTouchBegan(touchPosition: CGPoint) {
        if touchPosition.x < 0 {
            // let grab when a hand is resting on an obstacle
            if characters[myCharacterIndex].leftHandContacted {
                characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .LeftHand)
                // give haptic feedback
                HapticResponseController.grabSucceedFeedback()
                
                if GameScene.multiplayerMode {
                    // prepare data to send to server, to let the server know that a client's character has grabbed an obstacle in his device while the hand was resting on it
                    if let _actionsToSend = reliableActionsToSend {
                        _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .LeftHand, grabPosition: characters[myCharacterIndex].bodyParts[.LeftHand]?.position, touchStatus: .None, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: true, grabbedCharacter: false, forChangingHandAppearance: false)))
                    }
                }
            }
            
            // client should send the information about whether the user of the client character has pressed the grab button, so that the server can compute grabl logics between character based on this information
            if GameScene.multiplayerMode {
                if let _actionsToSend = unreliableActionsToSend {
                    _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .LeftHand, touchStatus: .TouchBegan, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance: true)))
                }
            }
            
            // set flags
            leftGrabButtonPressed = true
            characters[myCharacterIndex].leftGrabButtonPressed = true
            characters[myCharacterIndex].closeLeftHand()
        } else {
            if characters[myCharacterIndex].rightHandContacted {
                characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .RightHand)
                HapticResponseController.grabSucceedFeedback()
                
                if GameScene.multiplayerMode {
                    if let _actionsToSend = reliableActionsToSend {
                        _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .RightHand, grabPosition: characters[myCharacterIndex].bodyParts[.RightHand]?.position, touchStatus: .None, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: true, grabbedCharacter: false, forChangingHandAppearance: false)))
                    }
                }
            }
            
            if GameScene.multiplayerMode {
                if let _actionsToSend = unreliableActionsToSend {
                    _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .RightHand, touchStatus: .TouchBegan, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance:  true)))
                }
            }
            
            rightGrabButtonPressed = true
            characters[myCharacterIndex].rightGrabButtonPressed = true
            characters[myCharacterIndex].closeRightHand()
        }
    }
    
    func grabButtonTouchMoved(touchPosition: CGPoint) {
        if characters[myCharacterIndex].leftHandGrabbing {
            characters[myCharacterIndex].leftHandJustGrabbed = false
        }
        if characters[myCharacterIndex].rightHandGrabbing {
            characters[myCharacterIndex].rightHandJustGrabbed = false
        }
    }
    
    func grabButtonTouchEnded(touchPosition: CGPoint) {
        if touchPosition.x < 0 { // left hand grab button area
            if characters[myCharacterIndex].leftHandGrabbing {
                characters[myCharacterIndex].makeHandReleaseFromObstacle(hand: .LeftHand)
                
                // prepare data to send to server, to let the server know that a client's character has released a grab an obstacle in his device
                if GameScene.multiplayerMode {
                    if let _actionsToSend = reliableActionsToSend {
                        _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .LeftHand, touchStatus: .None, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance: false)))
                    }
                }
            }
            
            // client should send the info about the pressed button\
            if GameScene.multiplayerMode {
                if let _actionsToSend = reliableActionsToSend {
                    _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .LeftHand, touchStatus: .TouchEnded, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance: true)))
                }
            }
            
            leftGrabButtonPressed = false
            characters[myCharacterIndex].leftGrabButtonPressed = false
            characters[myCharacterIndex].openLeftHand()
        } else {
            if characters[myCharacterIndex].rightHandGrabbing {
                characters[myCharacterIndex].makeHandReleaseFromObstacle(hand: .RightHand)
                
                // prepare data to send to server
                if GameScene.multiplayerMode {
                    if let _actionsToSend = reliableActionsToSend {
                        _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .RightHand, touchStatus: .None, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance: false)))
                    }
                }
            }
            
            // client should send the info to server whether a button is released
            if GameScene.multiplayerMode {
                if let _actionsToSend = reliableActionsToSend {
                    _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .RightHand, touchStatus: .TouchEnded, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance: true)))
                }
            }
            
            rightGrabButtonPressed = false
            characters[myCharacterIndex].rightGrabButtonPressed = false
            characters[myCharacterIndex].openRightHand()
        }
    }
    
    //MARK: - contact detection - delegate function
    func didBegin(_ contact: SKPhysicsContact) {
        if GameScene.multiplayerMode {
            guard gameStarted else { return }
            if GameScene.isServer {
                self.checkForContacts_Multi_server(contact)
            } else {
                self.checkForContacts_Multi_client(contact)
            }
        } else{
            self.checkForContacts_single(contact)
        }
    }
    
    func didEnd(_ contact: SKPhysicsContact) {
        if GameScene.multiplayerMode {
            guard gameStarted else { return }
            //guard GameScene.isServer else { return }
            self.checkForContactsDidEnd_Multi(contact)
        } else{
            self.checkForContactsDidEnd_single(contact)
        }
        
    }
    
    
    //MARK: - auxiliary functions for detecting contact in multiplayer mode
    
    /* Grab logic between a character and a obstacle is computed locally in each client's devices.
     * Grab logic between characters is computed by the server and the result is dispatched to characters
     */
    
    /* function used by server
     * 1. it determine whether server character has contacted with an obstacle
     * 2. it checks whether two character have contacted
     */
    func checkForContacts_Multi_server(_ contact: SKPhysicsContact) {
        let character = characters[myCharacterIndex]
        let rightHand = characters[myCharacterIndex].bodyParts[.RightHand]
        let leftHand = characters[myCharacterIndex].bodyParts[.LeftHand]
        
        // obstacle contact detection
        if let contactNodeA = contact.bodyA.node,
           let contactNodeB = contact.bodyB.node {
            // check lefthhand with obstacle
            // left hand can grab an obstacle only when it is not grabbing another character
            if !character.leftHandGrabbedCharacter {
                if leftHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                    character.leftHandContacted = true
                    
                    // in case when user already press the grab button,
                    // the garbbing interaction is triggered immediately
                    if leftGrabButtonPressed {
                        characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .LeftHand)
                        HapticResponseController.grabSucceedFeedback()
                    }
                } else if leftHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                    character.leftHandContacted = true

                    // in case when user already press the grab button,
                    // the garbbing interaction is triggered immediately
                    if leftGrabButtonPressed {
                        characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .LeftHand)
                        HapticResponseController.grabSucceedFeedback()
                    }
                }
            }
                
            // check right hand with obstacle
            if !character.rightHandGrabbedCharacter {
                if rightHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                    character.rightHandContacted = true
                    
                    // in case when user already press the grab button,
                    // the garbbing interaction is triggered immediately
                    if rightGrabButtonPressed {
                        characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .RightHand)
                        HapticResponseController.grabSucceedFeedback()
                    }
                } else if rightHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                    character.rightHandContacted = true

                    // in case when user already press the grab button,
                    // the garbbing interaction is triggered immediately
                    if rightGrabButtonPressed {
                        characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .RightHand)
                        HapticResponseController.grabSucceedFeedback()
                    }
                }
            }
        }
    
        
        // detecting contact between players
        if let contactNodeA = contact.bodyA.node, let contactNodeB = contact.bodyB.node {
            let allBodyParts = allCharactersBodyParts.values
            if allBodyParts.contains(contactNodeA) && allBodyParts.contains(contactNodeB) {
                if let firstBodyPartIndex = reversedAllCharactersBodyParts[contactNodeA],
                   let secondBodyPartIndex = reversedAllCharactersBodyParts[contactNodeB] {
                    // skip if bodyparts of same character has contacted
                    if firstBodyPartIndex.characterIndex != secondBodyPartIndex.characterIndex {
                 
                        if firstBodyPartIndex.bodyPart == Character.BodyPart.LeftHand  || firstBodyPartIndex.bodyPart == Character.BodyPart.RightHand ||
                            secondBodyPartIndex.bodyPart == Character.BodyPart.LeftHand || secondBodyPartIndex.bodyPart == Character.BodyPart.RightHand {
                            bodyPartsContactFlags[CharacterPair(character1: firstBodyPartIndex, character2: secondBodyPartIndex)] = true
                            return
                        }
                    }
                }
            }
        }
    }
    
    /* function used by server
     * 1. it determine whether server character stopped contacting with an obstacle
     * 2. it checks whether two character have stopped contacting each other
     */
    func checkForContactsDidEnd_Multi(_ contact: SKPhysicsContact) {
        
        let character = characters[myCharacterIndex]
        let rightHand = characters[myCharacterIndex].bodyParts[.RightHand]
        let leftHand = characters[myCharacterIndex].bodyParts[.LeftHand]

        if let contactNodeA = contact.bodyA.node,
           let contactNodeB = contact.bodyB.node {
            // check lefthhand with obstacle
            if !character.leftHandGrabbedCharacter {
                if leftHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                    character.leftHandContacted = false
                } else if leftHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                    character.leftHandContacted = false
                }
            }
            
            if !character.rightHandGrabbedCharacter {
                // check right hand with obstacle
                if rightHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                    character.rightHandContacted = false
                } else if rightHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                    character.rightHandContacted = false
                }
            }
        }
        
        
        // detecting whether contact between players ended
        if let contactNodeA = contact.bodyA.node, let contactNodeB = contact.bodyB.node {
            let allBodyParts = allCharactersBodyParts.values
            if allBodyParts.contains(contactNodeA) && allBodyParts.contains(contactNodeB) {
                if let firstBodyPartIndex = reversedAllCharactersBodyParts[contactNodeA],
                   let secondBodyPartIndex = reversedAllCharactersBodyParts[contactNodeB] {
                    // skip if bodyparts of same character has contacted
                    if firstBodyPartIndex.characterIndex != secondBodyPartIndex.characterIndex {
                        if firstBodyPartIndex.bodyPart == Character.BodyPart.LeftHand || firstBodyPartIndex.bodyPart == Character.BodyPart.RightHand || secondBodyPartIndex.bodyPart == Character.BodyPart.LeftHand || secondBodyPartIndex.bodyPart == Character.BodyPart.RightHand {
                            bodyPartsContactFlags[CharacterPair(character1: firstBodyPartIndex, character2: secondBodyPartIndex)] = false
                            return
                        }
                    }
                }
            }
        }
    }
    
    /* function used by client
     * It determine whether client character has contacted with an obstacle.
     * If yes, it lets the server know that the character's hand has maded to static
     */
    func checkForContacts_Multi_client(_ contact: SKPhysicsContact) {
        let character = characters[myCharacterIndex]
        let rightHand = characters[myCharacterIndex].bodyParts[.RightHand]
        let leftHand = characters[myCharacterIndex].bodyParts[.LeftHand]
        
        // obstacle contace detection
        if let contactNodeA = contact.bodyA.node,
           let contactNodeB = contact.bodyB.node {
            // check lefthhand with obstacle
            // left hand can grab an obstacle only when it is not grabbing another character
            if !character.leftHandGrabbedCharacter {
                if leftHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                    character.leftHandContacted = true
                    
                    // in case when user already press the grab button,
                    // the garbbing interaction is triggered immediately
                    if leftGrabButtonPressed {
                        character.makeHandGrabbingObstacle(hand: .LeftHand)
                        HapticResponseController.grabSucceedFeedback()
                        
                        // let the server know that the client character has grabbed an obstacle
                        if let _actionsToSend = reliableActionsToSend {
                            _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .LeftHand, grabPosition: contactNodeA.position, touchStatus: .None, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: true, grabbedCharacter: false, forChangingHandAppearance: false)))
                        }
                    }
                } else if leftHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                    character.leftHandContacted = true

                    // in case when user already press the grab button,
                    // the garbbing interaction is triggered immediately
                    if leftGrabButtonPressed {
                        character.makeHandGrabbingObstacle(hand: .LeftHand)
                        HapticResponseController.grabSucceedFeedback()
                        
                        // prepare data to send to server
                        if let _actionsToSend = reliableActionsToSend {
                            _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .LeftHand, grabPosition: contactNodeB.position, touchStatus: .None, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: true, grabbedCharacter: false, forChangingHandAppearance: false)))
                        }
                    }
                }
            }
            
            // check right hand with obstacle
            if !character.rightHandGrabbedCharacter {
                if rightHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                    character.rightHandContacted = true
                    
                    // in case when user already press the grab button,
                    // the garbbing interaction is triggered immediately
                    if rightGrabButtonPressed {
                        characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .RightHand)
                        HapticResponseController.grabSucceedFeedback()
                        
                        // prepare data to send to server
                        if let _actionsToSend = reliableActionsToSend {
                            _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .RightHand, grabPosition: contactNodeA.position, touchStatus: .None, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: true, grabbedCharacter: false, forChangingHandAppearance: false)))
                        }
                    }
                } else if rightHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                    character.rightHandContacted = true

                    // in case when user already press the grab button,
                    // the garbbing interaction is triggered immediately
                    if rightGrabButtonPressed {
                        characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .RightHand)
                        HapticResponseController.grabSucceedFeedback()
                        
                        // prepare data to send to server
                        if let _actionsToSend = reliableActionsToSend {
                            _actionsToSend.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .RightHand, grabPosition: contactNodeB.position, touchStatus: .None, makeSound: false, soundMakeCharacterIndex: nil, fromClient: true, grabbedObstacle: true, grabbedCharacter: false, forChangingHandAppearance: false)))
                        }
                    }
                }
            }
        }
    }
    
    //MARK: - auxiliary functions for detecting contact in single player mode
    func checkForContacts_single(_ contact: SKPhysicsContact) {
        let rightHand = characters[myCharacterIndex].bodyParts[.RightHand]
        let leftHand = characters[myCharacterIndex].bodyParts[.LeftHand]
        
        if let contactNodeA = contact.bodyA.node,
           let contactNodeB = contact.bodyB.node {
            // check lefthhand with obstacle
            if leftHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                characters[myCharacterIndex].leftHandContacted = true
                
                // in case when user already press the grab button,
                // the garbbing interaction is triggered immediately
                if leftGrabButtonPressed {
                    characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .LeftHand)
                    HapticResponseController.grabSucceedFeedback()
                }
            } else if leftHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                characters[myCharacterIndex].leftHandContacted = true
                
                // in case when user already press the grab button,
                // the garbbing interaction is triggered immediately
                if leftGrabButtonPressed {
                    characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .LeftHand)
                    HapticResponseController.grabSucceedFeedback()
                }
            }
            
            // check right hand with obstacle
            if rightHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                characters[myCharacterIndex].rightHandContacted = true
                
                // in case when user already press the grab button,
                // the garbbing interaction is triggered immediately
                if rightGrabButtonPressed {
                    characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .RightHand)
                    HapticResponseController.grabSucceedFeedback()
                }
            } else if rightHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                characters[myCharacterIndex].rightHandContacted = true
                
                // in case when user already press the grab button,
                // the garbbing interaction is triggered immediately
                if rightGrabButtonPressed {
                    characters[myCharacterIndex].makeHandGrabbingObstacle(hand: .RightHand)
                    HapticResponseController.grabSucceedFeedback()
                }
            }
        }
    }
    
    func checkForContactsDidEnd_single(_ contact: SKPhysicsContact) {
        let rightHand = characters[myCharacterIndex].bodyParts[.RightHand]
        let leftHand = characters[myCharacterIndex].bodyParts[.LeftHand]
        
        if let contactNodeA = contact.bodyA.node,
           let contactNodeB = contact.bodyB.node {
            // check lefthhand with obstacle
            if leftHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                characters[myCharacterIndex].leftHandContacted = false
            } else if leftHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                characters[myCharacterIndex].leftHandContacted = false
            }
            
            // check right hand with obstacle
            if rightHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                characters[myCharacterIndex].rightHandContacted = false
            } else if rightHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                characters[myCharacterIndex].rightHandContacted = false
            }
        }
    }

    /*
     * This function applies grab interaction depending on the contact status
     */
    func processBodyContacts() {
        // check if a grab should be processed
        let bodyPartsContactFlagsKeys = bodyPartsContactFlags.keys
        for key in bodyPartsContactFlagsKeys {
            // check if two body parts were contacted
            // check if the joints are already added
            if let jointAdded = jointsAddedFlag[key] {
                if !jointAdded {
                    if bodyPartsContactFlags[key] == true {
                        checkIfJointsCanBeAdded(character1: key.character1, character2: key.character2)
                    }
                } else {
                    checkIfJointsCanBeRemoved(character1: key.character1, character2: key.character2)
                    bodyPartsContactFlags[key] = false
                }
            }
        }
    }
    
    func checkIfJointsCanBeAdded(character1 : CharacterAndBodypart, character2 : CharacterAndBodypart) {
        /*
         * 1. check which body parts are contacting each other - one of them should be had to start grab interaction
         * 2. check whether the player of the chacters which the hand belogs to has pressed grab button
         * 3. if yes, then add joint between body parts to start grab interaction
         * 4. return to avoid any fall throuhgs
         */
        if character1.bodyPart == .LeftHand {
            if character2.bodyPart == .LeftHand  {
                if characters[character1.characterIndex].leftGrabButtonPressed {
                    if !characters[character1.characterIndex].leftHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
                
                if characters[character2.characterIndex].leftGrabButtonPressed {
                    if !characters[character2.characterIndex].leftHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            } else if character2.bodyPart == .RightHand {
                if characters[character1.characterIndex].leftGrabButtonPressed {
                    if !characters[character1.characterIndex].leftHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
                
                if characters[character2.characterIndex].rightGrabButtonPressed {
                    if !characters[character2.characterIndex].rightHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            } else {
                if characters[character1.characterIndex].leftGrabButtonPressed {
                    if !characters[character1.characterIndex].leftHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            }
        } else if character1.bodyPart == .RightHand {
            if character2.bodyPart == .LeftHand {
                if characters[character1.characterIndex].rightGrabButtonPressed {
                    if !characters[character1.characterIndex].rightHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
                
                if characters[character2.characterIndex].leftGrabButtonPressed {
                    if !characters[character2.characterIndex].leftHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            } else if character2.bodyPart == .RightHand {
                if characters[character1.characterIndex].rightGrabButtonPressed {
                    if !characters[character1.characterIndex].rightHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
                
                if characters[character2.characterIndex].rightGrabButtonPressed {
                    if !characters[character2.characterIndex].rightHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            } else {
                if characters[character1.characterIndex].rightGrabButtonPressed {
                    if !characters[character1.characterIndex].rightHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            }
        } else {
            if character2.bodyPart == .LeftHand {
                if characters[character2.characterIndex].leftGrabButtonPressed {
                    if !characters[character2.characterIndex].leftHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            } else if character2.bodyPart == .RightHand {
                if characters[character2.characterIndex].rightGrabButtonPressed {
                    if !characters[character2.characterIndex].rightHandGrabbedCharacter {
                        addJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            }
        }
    }
    
    func checkIfJointsCanBeRemoved(character1 : CharacterAndBodypart, character2 : CharacterAndBodypart) {
        /*
         * 1. check which body parts are contacting grabbing other - one of them should be had to start grab interaction
         * 2. check whether the player of the chacters which the hand belogs to has released grab button
         * 3. if yes, then remove joint between body parts to stop grab interaction
         * 4. return to avoid any fall throuhgs
         */
        if character1.bodyPart == .LeftHand {
            if character2.bodyPart == .LeftHand {
                if !characters[character1.characterIndex].leftGrabButtonPressed {
                    if characters[character1.characterIndex].leftHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
                
                if !characters[character2.characterIndex].leftGrabButtonPressed {
                    if characters[character2.characterIndex].leftHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
                
            } else if character2.bodyPart == .RightHand {
                
                if !characters[character1.characterIndex].leftGrabButtonPressed {
                    if characters[character1.characterIndex].leftHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
                
                if !characters[character2.characterIndex].rightGrabButtonPressed {
                    if characters[character2.characterIndex].rightHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            } else {
                if !characters[character1.characterIndex].leftGrabButtonPressed {
                    if characters[character1.characterIndex].leftHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            }
        } else if character1.bodyPart == .RightHand {
            if character2.bodyPart == .LeftHand {
                if !characters[character1.characterIndex].rightGrabButtonPressed {
                    if characters[character1.characterIndex].rightHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
                
                if !characters[character2.characterIndex].leftGrabButtonPressed {
                    if characters[character2.characterIndex].leftHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            } else if character2.bodyPart == .RightHand {
                if !characters[character1.characterIndex].rightGrabButtonPressed {
                    if characters[character1.characterIndex].rightHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
                
                if !characters[character2.characterIndex].rightGrabButtonPressed {
                    if characters[character2.characterIndex].rightHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            } else {
                if !characters[character1.characterIndex].rightGrabButtonPressed {
                    if characters[character1.characterIndex].rightHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            }
        } else {
            if character2.bodyPart == .LeftHand {
                if !characters[character2.characterIndex].leftGrabButtonPressed {
                    if characters[character2.characterIndex].leftHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            } else if character2.bodyPart == .RightHand {
                if !characters[character2.characterIndex].rightGrabButtonPressed {
                    if characters[character2.characterIndex].rightHandGrabbedCharacter {
                        removeJointBetweenCharacters(firstBodyPart: character1, secondBodyPart: character2)
                        return
                    }
                }
            }
        }
    }
    
    // this function provides visual effect of touching the screen
    func showGreyBox(side : Character.BodyPart) {
        if side == .LeftHand {
            if let gb = leftGreyBox {
                gb.alpha = 0.05
            }
        } else {
            if let gb = rightGreyBox {
                gb.alpha = 0.05
            }
        }
    }
    
    // this function provides visual effect of touching the screen
    func hideGreyBox(side : Character.BodyPart) {
        if side == .LeftHand {
            if let gb = leftGreyBox {
                gb.alpha = 0
            }
        } else {
            if let gb = rightGreyBox {
                gb.alpha = 0
            }
        }
    }
    
    func reachesCharacterGoal(character: Character, goalObstacle: Obstacle) -> Bool {
        let bodyPosition = convertCharacterPositionToGameSceneCoordinate(character: character)
        return goalObstacle.containsPoint(obstaclePositionInparent: goalObstacle.position, point: bodyPosition)
    }
    
    func convertCharacterPositionToGameSceneCoordinate(character: Character) -> CGPoint {
        return character.convert(character.bodyParts[.Body]!.position, to: self)
    }
    
    // ------------------------------------------------
    // MARK: - frame update
    // ------------------------------------------------
    override func update(_ currentTime: TimeInterval) {
        
        if gameStarted {
            processInteractions()
            if GameScene.isServer {
                processBodyContacts()
            }
            if GameScene.isServer { checkGoal() }
            checkCharacterIsOutOfScene(character: characters[myCharacterIndex])
            syncPhysics()
            updateCamera()
        } else {
            if GameScene.multiplayerMode && !GameScene.isServer {
                updateGameSceneLoadState()
            }
        }
        
        if !GameScene.multiplayerMode {
            processInteractions()
            checkCharacterIsOutOfScene(character: characters[myCharacterIndex])
            checkGoal()
            updateCamera()
        }
        
        if showCalibrationMessage {
            if alertShowTimer == 0 {
                calibrationNode?.isHidden = false
            }
            if alertShowTimer == 30 {
                calibrationNode?.isHidden = true
                alertShowCounter += 1
            }
            
            alertShowTimer += 1
            alertShowTimer %= 60
            
            if alertShowCounter > 2 {
                showCalibrationMessage = false
                calibrationNode?.isHidden = true
                alertShowCounter = 0
                alertShowTimer = 0
            }
        }
    }
    

    
    /// update camera position to follow character
    func updateCamera() {
        guard characters[myCharacterIndex].isDropped == false else { return }
        let currentMyPosition = characters[myCharacterIndex].bodyParts[.Body]!.position
        if let cameraNode = camera {
            // change the camera scene coordinate system to world
            let convertedCamera = cameraNode.scene?.convert(cameraNode.position, from: self)
            if abs(convertedCamera!.x - currentMyPosition.x) > 50
                && abs(convertedCamera!.y - currentMyPosition.y) > 50 {
                let newPosition = CGPoint(x: min(currentMyPosition.x, size.width * 2), y: max(currentMyPosition.y + size.height * 0.7, -750 + size.height * 0.7))
                let action = SKAction.move(to: newPosition, duration: 0.5)
                cameraNode.run(action)
            }
        }
    }
    
    /// check the charater is out of the scene(drop case)
    func checkCharacterIsOutOfScene(character: Character) {
        guard !character.isDropped else { return }
        
        // if both hands are all out from the scene
        // the character is dropped
        if self.scene?.contains(character.bodyParts[.LeftHand]!.position) == false
            && self.scene?.contains(character.bodyParts[.RightHand]!.position) == false {
            character.isDropped = true
            
            if GameScene.multiplayerMode {
                if GameScene.isServer {
                    NotificationCenter.default.post(name: .gameResultReceived, object: nil, userInfo: ["result": "fail"])
                    localTeamPlayManager.sendToAll(sessionAction: SessionAction.setup(SetupAction.gameOver(true)), isReliable: true)
                } else {
                    let actionData = CharacterActionData(characterIndex: myCharacterIndex)
                    actionData.actions.append(CharacterAction.dropped(character.isDropped))
                    localTeamPlayManager.sendToHost(sessionAction: SessionAction.gamePhysics(actionData), isReliable: true)
                }
            } else {
                NotificationCenter.default.post(name: .gameResultReceived, object: nil, userInfo: ["result": "fail"])
            }
        }
    }
    
    /// Server and Client: rendering interaction events(touch or motion)
    func processInteractions() {
        let myCharacter = characters[myCharacterIndex]
        let vector = GameGyroscopeController.sharedInstance.getlatestRotation() //movementData?.vector ?? .zero
        characterMovementData[myCharacterIndex] = vector
        
        if !GameScene.multiplayerMode { myCharacter.moveCharacter(vector: vector) }
        
        /// Server moves all of the characters according to the gyroscope input from other players
        if GameScene.multiplayerMode && GameScene.isServer {
            if !characters.isEmpty {
                for index in 0..<characters.count {
                    characters[index].moveCharacter(vector: characterMovementData[index])
                }
            }
        }
        
        /// clients sends to server information about the grab button interaction and movement data from user input
        if GameScene.multiplayerMode && !GameScene.isServer {
            // gyroscope data exchange doens't need to be reliable
            unreliableActionsToSend?.actions.append(CharacterAction.physics(myCharacter.generateCharacterPhysicsSyncData(vector: vector)))
            // send all the datas prepared
            if let _actionsToSend = self.reliableActionsToSend {
                if !_actionsToSend.actions.isEmpty {
                    localTeamPlayManager.sendToHost(sessionAction: SessionAction.gamePhysics(_actionsToSend), isReliable: true)
                    self.reliableActionsToSend?.actions.removeAll()
                }
            }
            if let _actionsToSend = self.unreliableActionsToSend {
                if !_actionsToSend.actions.isEmpty {
                    localTeamPlayManager.sendToHost(sessionAction: SessionAction.gamePhysics(_actionsToSend), isReliable: false)
                    self.unreliableActionsToSend?.actions.removeAll()
                }
            }
        }
    }
    
    /// Server and Client: for sync physics between server and clients
    func syncPhysics() {
        if GameScene.multiplayerMode && GameScene.isServer {
            syncAllCharacterAction()
        } else {
            applyReceivedCharactersAction()
        }
    }
    
    /// Server: uses this function to send all characters' actions to all peers for synchronization
    func syncAllCharacterAction() {
        let syncData: SyncData = SyncData()
        // pack all characters' actions into SyncData
        for (index, character) in characters.enumerated() {
            let actionData = CharacterActionData(characterIndex: index)
            actionData.actions.append(CharacterAction.physics(character.generateCharacterPhysicsSyncData(vector: .zero)))
            actionData.actions.append(CharacterAction.reachesGoal(character.reachesGoal))
            
            // send information about whether the hands should be closed
            // needed for 3 > players
            if character.leftGrabButtonPressed {
                actionData.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .LeftHand, touchStatus: .TouchBegan, makeSound: false, soundMakeCharacterIndex: nil, fromClient: false, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance: true)))
            } else {
                actionData.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .LeftHand, touchStatus: .TouchEnded, makeSound: false, soundMakeCharacterIndex: nil, fromClient: false, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance: true)))
            }
            
            if character.rightGrabButtonPressed {
                actionData.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .RightHand, touchStatus: .TouchBegan, makeSound: false, soundMakeCharacterIndex: nil, fromClient: false, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance: true)))
            } else {
                actionData.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .RightHand, touchStatus: .TouchEnded, makeSound: false, soundMakeCharacterIndex: nil, fromClient: false, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance: true)))
            }
            
            // let the client character make sound after he has grabbed another character
            if GameScene.shouldSendLeftHandGrabSound[index] {
                actionData.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .LeftHand, touchStatus: .TouchBegan, makeSound: true, soundMakeCharacterIndex: index, fromClient: false, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance: false)))
                GameScene.shouldSendLeftHandGrabSound[index] = false
            }
            if GameScene.shouldSendRightHandGrabSound[index] {
                actionData.actions.append(CharacterAction.grabInfo(GrabInfo(grabHand: .RightHand, touchStatus: .TouchBegan, makeSound: true, soundMakeCharacterIndex: index, fromClient: false, grabbedObstacle: false, grabbedCharacter: false, forChangingHandAppearance: false)))
                GameScene.shouldSendRightHandGrabSound[index] = false
            }
            
            syncData.data.append(actionData)
 
        }
        // send character action data to all clients
        localTeamPlayManager.sendToAll(sessionAction: SessionAction.syncAllCharacters(syncData), isReliable: false)
    }
    
    func checkGoal() {
        for (index, character) in characters.enumerated() {
            let isReached = reachesCharacterGoal(character: character, goalObstacle: goalObstacle!)
            characterInGoalList[index] = isReached
            character.reachesGoal = isReached

            if (characterInGoalList.allSatisfy{ $0 == true }) {
                // Game is successfully done
                NotificationCenter.default.post(name: .gameResultReceived, object: nil, userInfo: ["result": "success"])

                // Server notifies to all players that game is success
                if GameScene.multiplayerMode && GameScene.isServer {
                    localTeamPlayManager.sendToAll(sessionAction: SessionAction.setup(SetupAction.gameSuccess(true)), isReliable: true)
                }
            }
        }
    }
    
    /// Client: uses this function to send that their GameScene is loaded and ready to start game
    func updateGameSceneLoadState() {
        localTeamPlayManager.sendToHost(sessionAction: SessionAction.setup(SetupAction.gameSceneLoaded(true)), isReliable: true)
    }
    
    /// Client: uses this function to process queued actions
    func applyReceivedCharactersAction() {
        guard !receivedCharactersAction.isEmpty else { return }
                
        canAppendReceivedCharacterAction = false
        
        let tmpReceivedCharactersAction = receivedCharactersAction
        if let oldestActionData = tmpReceivedCharactersAction.first {
            if !oldestActionData.isEmpty {
                for data in oldestActionData {
                    let character = characters[data.characterIndex]
                    if !data.actions.isEmpty {
                        for action in data.actions {
                            switch action {
                            case .grabInfo(let grabInfo):
                                // apply hand close/open motion for other characters
                                if data.characterIndex != myCharacterIndex {
                                    if grabInfo.touchStatus == .TouchBegan {
                                        if grabInfo.grabHand == .LeftHand {
                                            character.closeLeftHand()
                                        }
                                        if grabInfo.grabHand == .RightHand {
                                            character.closeRightHand()
                                        }
                                    }
                                    if grabInfo.touchStatus == .TouchEnded {
                                        if grabInfo.grabHand == .LeftHand {
                                            character.openLeftHand()
                                        }
                                        if grabInfo.grabHand == .RightHand {
                                            character.openRightHand()
                                        }
                                    }
                                }
                                // make grab sound if my character has grabbed another character
                                if grabInfo.makeSound && grabInfo.soundMakeCharacterIndex == myCharacterIndex {
                                    makeGrabSound()
                                }
                            case .physics(let physics):
                                character.apply(physicsData: physics)
                            case .reachesGoal(_):
                                break
                            case .dropped(let isDropped):
                                character.isDropped = isDropped
                                
                            }
                        }
                    }
                }
                receivedCharactersAction.removeFirst()
            }
        }
        canAppendReceivedCharacterAction = true
    }
    
    /// Client: uses this function to queue received characters' actions for rendering
    func queueReceivedCharactersAction(data allCharacterActions: [CharacterActionData]) {
        guard canAppendReceivedCharacterAction else { return }
        receivedCharactersAction.append(allCharacterActions)

        while receivedCharactersAction.count > maxQueueCount {
            receivedCharactersAction.removeLast()
        }
    }
    
    func makeGrabSound() {
        let grabSound = SKAction.playSoundFileNamed("audio_grab.mp3", waitForCompletion: false)
        self.run(grabSound)
    }
}

