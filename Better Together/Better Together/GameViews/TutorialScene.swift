//
//  TutorialScene.swift
//  Better Together
//
//  Created by Eunae Jang on 2021/01/02.
//

import SpriteKit
import GameplayKit

class TutorialScene: SKScene, SKPhysicsContactDelegate {
    
    // MARK: - Nodes
    private let obstacleCreator: ObstacleCreator = ObstacleCreator.sharedInstance
    var obstacles : [String : SKNode] = [:] {
        // logic should be adjusted when using obstacle creator
        didSet {
            for obstacle in obstacles.values {
                if obstacle.frame.size.width <= obstacle.frame.size.height {
                    Character.obstacleLowPoints.append(CGPoint(x: obstacle.frame.midX, y: obstacle.frame.minY))
                }
            }
        }
    }
    private var ground: Obstacle?
    var character : Character = Character()
    
    //MARK: - grab control
    var leftGrabButtonPressed = false {
        didSet {
            if leftGrabButtonPressed {
                showGreyBox(side: 0)
            } else {
                hideGreyBox(side: 0)
            }
        }
    }
    var rightGrabButtonPressed = false {
        didSet {
            if rightGrabButtonPressed {
                showGreyBox(side: 1)
            } else {
                hideGreyBox(side: 1)
            }
        }
    }
    var leftGreyBox : SKShapeNode?
    var rightGreyBox : SKShapeNode?
    var greyBoxLeftPosition : CGPoint = .zero
    var greyBoxRightPosition : CGPoint = .zero
    
    // MARK: - Instruction variables
    var frameCounter : Int = 0
    var showTabToContinue = true {
        didSet {
            if showTabToContinue {
                debugPrint("showTabToCont set true at \(tabCount)")
                tabToCont.isHidden = false
                frameCounter = 0
            } else {
                debugPrint("showTabToCont set false at \(tabCount)")
                tabToCont.isHidden = true
            }
        }
    }
    // label nodes for sentences on the screen
    var tabToCont = SKLabelNode(text: "tab to continue")
    var instruction1 = SKLabelNode()
    var instruction2 = SKLabelNode()
    // variables used to control the instruction show time
    var instructionReadTime = 0
    var startInstructionTimeCounting = false
    var canSkipInstruction = false
    // this variable controls the flow of the tutorial scene
    var tabCount = 0
    // variables used to execute the first challange "moving the hands up"
    var tiltUpInstruction = false
    var tiltUpInstrucitonSucceed = false
    var tiltUpInstrucitonTime = 0
    var startTiltUpTime = false
    var canReachTiltUp = false
    // variables used to execute the challange "pressing grab buttons"
    var grabTouchTryCount = 0
    // variables used to execute the challange "reaching flags"
    var firstFlagReached = false
    var secondFlagReached = false
    var gaveHapticFirstFlag = false
    var gaveHapticSecondFlag = false
    // flags used to turn on/off the grab interaction
    var allowGrab = false
    var allowButtonInteraction = false
    // variables used to achieve certain camera movement effects
    var startZoomOut = false
    var zoomedOut = false
    var zoomOutCounter = 0
    // variables used for executing the last challenge "hanging and swinging"
    var hangingSuccesful = false
    var startMovingPracticeGround = false
    var startSwingPractice = false
    var landed = false
    var resetGrabStatus = true
    var redLineBlinkCounter = 0
    var showRedLine = false
    var leftHandCanSwing = false
    var rightHandCanSwing = false
    var bodyOnSwingPracticeGround = false
    
    // MARK: - Tutorial Scene
    override func didMove(to view: SKView) {
        view.isMultipleTouchEnabled = true
        
        //add camera
        let cameraNode = SKCameraNode()
        cameraNode.position = CGPoint(x: 0, y: 0)
        addChild(cameraNode)
        camera = cameraNode
        camera?.setScale(2.0)
        
        // add character to node
        character = CharacterCreater.sharedCharacterCreator.createCharacter(scene: self,
                                                                            color: BodyColor(color: UIColor.random),
                                                                           bodyRadius: 15,
                                                                           position: CGPoint(x: 0, y: 100),
                                                                           playerIndex: 0,
                                                                           isMyChracter: true,
                                                                           isTutorial: true)
        
        // initial character setting
        character.makeJointsNotRotate()
    
        // add ground to the scene
        self.initObstacles()
        
        // for contact detection
        physicsWorld.contactDelegate = self
        
        // initialize grab button functionality
        initializeGrabButtons()
        
        // add text labels
        tabToCont.position = convert(CGPoint(x: 0, y: -3 * self.frame.width / 8), to: self.camera!)
        tabToCont.fontColor = .white
        //tabToCont.fontSize /= 2
        tabToCont.isHidden = true
        self.camera!.addChild(tabToCont)
        self.camera!.addChild(instruction1)
        self.camera!.addChild(instruction2)
        
        showText(text: "Welcome to Tutorial!")
        startInstructionTimer()
        showTabToContinue = false
        
    }
    
    
    // MARK: - Set Obstacles in the Scene
    private func initObstacles() {
        // Create and add StartPoint
        // MARK: - why is the frame not land scape mode?
        let groundWidth = frame.height * 3 / 4
        let groundHeight = character.bodyParts[.Body]!.frame.width / 2
        self.ground = obstacleCreator.createRectObstacle(rectSize: CGSize(width: groundWidth, height: groundHeight), type: .StartPoint, texture: nil)
        //MARK: why is the position not changed?
        self.ground?.position = CGPoint(x: 0, y: -self.frame.width / 6)
        ground!.name = "ground"
        obstacles["ground"] = ground!
        addChild(self.ground!)
        
        // barriers at the end so that character doesn't fall dow
        let leftBarrier = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 1, height: groundHeight * 20), type: .StartPoint, texture: nil)
        let rightBarrier = obstacleCreator.createRectObstacle(rectSize: CGSize(width: 1, height: groundHeight * 20), type: .StartPoint, texture: nil)
        leftBarrier.position = CGPoint(x: ground!.frame.maxX, y:ground!.position.y)
        rightBarrier.position = CGPoint(x: ground!.frame.minX, y:ground!.position.y)
        leftBarrier.fillColor = .clear
        leftBarrier.strokeColor = .clear
        rightBarrier.fillColor = .clear
        rightBarrier.strokeColor = .clear
        leftBarrier.physicsBody?.contactTestBitMask = 4
        rightBarrier.physicsBody?.contactTestBitMask = 4
        obstacles["leftBarrier"] = leftBarrier
        obstacles["rightBarrier"] = rightBarrier
        addChild(leftBarrier)
        addChild(rightBarrier)
        
        let redLine = SKShapeNode(rectOf: CGSize(width: obstacles["ground"]!.frame.width, height: obstacles["ground"]!.frame.height / 10))
        redLine.strokeColor = .red
        redLine.fillColor = .red
        redLine.position = CGPoint(x:obstacles["ground"]!.frame.midX, y: obstacles["ground"]!.frame.minY)
        obstacles["redLine"] = redLine
        addChild(redLine)
        redLine.isHidden = true
    }
    
    
    //MARK: - touch input handler
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // let the instruction to be shown for at least 2.5 sec
        guard canSkipInstruction || allowButtonInteraction else { return }
        
        tabCount += 1
        
        switch tabCount {
        case 1:
            showText(text: "Your character is moved by tilting your iPhone!")
            startInstructionTimer()
            showTabToContinue = false
            
            character.makeJointsRotate()
        case 2:
            showText(text: "Tilting forward will move your hands up!")
    
            startInstructionTimer()
            showTabToContinue = false
        case 3:
            showText(text: "Try to reach the line with hands!")
            showTabToContinue = false
            
            let groundWidth = frame.height * 3 / 4
            let groundHeight = character.bodyParts[.Body]!.frame.width / 2
            let handsUpLine = obstacleCreator.createRectObstacle(rectSize: CGSize(width: groundWidth / 4, height: groundHeight / 4), type: .StartPoint, texture: nil)
            let action = SKAction.move(to: CGPoint(x: 0, y: -self.frame.width / 32), duration: 1/2)
            handsUpLine.run(action)
            handsUpLine.name = "handsUpLine"
            obstacles["handsUpLine"] = handsUpLine
            addChild(handsUpLine)

            startTiltUpTime = true
            tiltUpInstruction = true
        case 4:
            if !tiltUpInstrucitonSucceed {
                tabCount -= 1
            } else {
                obstacles["handsUpLine"]?.removeFromParent()
                childNode(withName: "handsUpLine")?.physicsBody = nil
                childNode(withName: "handsUpLine")?.removeFromParent()
                
                showText(text: "Now tilt your device left and right!")
                startInstructionTimer()
                showTabToContinue = false
            }
        case 5:
            showText(text: "If the hands get stuck, try to lift them up")
            showText2(text: "by tilting forward, and then tilt to sideways!")
            startInstructionTimer()
            showTabToContinue = false
        case 6:
            showText(text: "To move further, you should grab an ")
            showText2(text: "object, and tilt your iPhone to sides!")
            startInstructionTimer()
            showTabToContinue = false
            
        case 7:
            showText(text: "Try to touch left or right half of the screen!")
            showText2(text: "")
            allowButtonInteraction = true
            showTabToContinue = false
        case 8:
            for t in touches {
                buttonTouchBegan(touchPosition: t.location(in: camera!))
            }
            grabTouchTryCount += 1
            if grabTouchTryCount < 6 {
                tabCount -= 1
            } else {
                allowButtonInteraction = false
                hideGreyBox(side: 0)
                hideGreyBox(side: 1)
                character.openRightHand()
                character.openLeftHand()
                //self.camera!.addChild(tabToCont)
                showText(text: "Did you see how the hands were")
                showText2(text: "closed when pressing and holding?")
                grabTouchTryCount  = 0
                
                startInstructionTimer()
                showTabToContinue = false
                
            }
        case 9:
            showText(text: "Try again!")
            showText2(text: "")
            showTabToContinue = false
            allowButtonInteraction = true
            
            for t in touches {
                buttonTouchBegan(touchPosition: t.location(in: camera!))
            }
            grabTouchTryCount += 1
            if grabTouchTryCount < 6 {
                tabCount -= 1
            } else {
                hideGreyBox(side: 0)
                hideGreyBox(side: 1)
                character.openRightHand()
                character.openLeftHand()
                //self.camera!.addChild(tabToCont)
                showText(text: "Good Job!")
                allowButtonInteraction = false
                //hideTapToCont = false
                startInstructionTimer()
                showTabToContinue = false
            }
        case 10:
            startInstructionTimer()
            showTabToContinue = false
            showText(text: "Now, let me explain how you can ")
            showText2(text: "grab obstacles!")
            
        case 11:
            grabTouchTryCount = 0
            
            startInstructionTimer()
            showTabToContinue = false
            showText(text: "You should press and HOLD, otherwise")
            showText2(text: "the character will release its hands")
            
        case 12:
            startInstructionTimer()
            showTabToContinue = false
            showText(text: "You can either grab when a hand")
            showText2(text: "is resting on an obstacle,")
            
        case 13:
            startInstructionTimer()
            showTabToContinue = false
            showText(text: "or, when you bring a hand to an")
            showText2(text: "obstacle, while pressing and holding!")
            
        case 14:
            
            if resetGrabStatus {
                character.makeHandReleaseFromObstacle(hand: .LeftHand)
                character.makeHandReleaseFromObstacle(hand: .RightHand)
                leftGrabButtonPressed = false
                rightGrabButtonPressed = false
                resetGrabStatus = false
            }
            
            allowButtonInteraction = true
            allowGrab = true
            for t in touches {
                buttonTouchBegan(touchPosition: t.location(in: camera!))
            }
        
            if grabTouchTryCount > 8 {
                hideGreyBox(side: 0)
                hideGreyBox(side: 1)
                
                let firstFlag = SKSpriteNode(imageNamed: "flag.png")
                let secondFlag = SKSpriteNode(imageNamed: "flag.png")
                firstFlag.name = "firstFlag"
                secondFlag.name = "secondFlag"
                firstFlag.setScale(0.25)
                secondFlag.setScale(0.25)
                firstFlag.position = CGPoint(x: ground!.frame.maxX, y: ground!.frame.midY + firstFlag.frame.height / 2)
                firstFlag.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: firstFlag.frame.width / 8, height: firstFlag.frame.height))
                firstFlag.physicsBody?.isDynamic = false
                firstFlag.physicsBody?.contactTestBitMask = Character.ContactType.ObstacleInteraction.rawValue
                obstacles["firstFlag"] = firstFlag
                if !firstFlagReached{ addChild(firstFlag) }
                secondFlag.position = CGPoint(x: -ground!.frame.maxX, y: ground!.frame.midY + firstFlag.frame.height / 2)
                secondFlag.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: firstFlag.frame.width / 8, height: firstFlag.frame.height))
                secondFlag.physicsBody?.isDynamic = false
                secondFlag.physicsBody?.contactTestBitMask = Character.ContactType.ObstacleInteraction.rawValue
                obstacles["secondFlag"] = secondFlag
                tabCount = 15
                fallthrough
            } else {
                grabTouchTryCount += 1
                tabCount -= 1
                
                showTabToContinue = false
                
                showText(text: "Try to grab the ground! We will give")
                showText2(text: "you haptic feedback if you succeed to grab!")
            }
        case 15:
            showText(text: "Good Job!")
            showText2(text: "Now, try to reach the flag!")
            tabCount = 16
            fallthrough
        case 16:
            for t in touches {
                buttonTouchBegan(touchPosition: t.location(in: camera!))
            }
            if !firstFlagReached {
                tabCount -= 1
                showTabToContinue = false
            }
        case 17 :
            for t in touches {
                buttonTouchBegan(touchPosition: t.location(in: camera!))
            }
            if !secondFlagReached {
                tabCount -= 1
                showTabToContinue = false
            }
        case 18 :
            allowButtonInteraction = false
            allowGrab = false
            
            showText(text: "Now, let's learn how to swing!")
            obstacles["leftBarrier"]!.physicsBody = nil
            obstacles["rightBarrier"]!.physicsBody = nil
            let width = obstacles["ground"]!.frame.width * 2
            let height = obstacles["ground"]!.frame.height
            let swingPracticeGround = obstacleCreator.createRectObstacle(rectSize: CGSize(width: width, height: height), type: .StartPoint, texture: nil)
            swingPracticeGround.name = "swingPractice"
            swingPracticeGround.position = CGPoint(x: obstacles["ground"]!.position.x, y: obstacles["ground"]!.position.y * 1.75)
            addChild(swingPracticeGround)
            obstacles["swingPractice"] = swingPracticeGround
            startZoomOut = true
            
            startInstructionTimer()
            showTabToContinue = false
        case 19 :
            showText(text: "Go down to lower obstacle, and try to grab the")
            showText2(text:"bottom side of upper opstacle with the hands!")
            showTabToContinue = false
            tabCount = 20
            fallthrough
        case 20:
            allowButtonInteraction = true
            allowGrab = true
            
            for t in touches {
                buttonTouchBegan(touchPosition: t.location(in: camera!))
            }
            
            startSwingPractice = true
            
            showRedLine = true
            if !hangingSuccesful {
                tabCount -= 1
                
            }
        default:
            break
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if allowButtonInteraction {
            for t in touches {
                buttonTouchMoved(touchPosition: t.location(in: camera!))
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if allowButtonInteraction {
            for t in touches {
                buttonTouchEnded(touchPosition: t.location(in: camera!))
            }
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        if allowButtonInteraction {
            for t in touches {
                buttonTouchEnded(touchPosition: t.location(in: camera!))
            }
        }
    }
    
    func buttonTouchBegan(touchPosition: CGPoint) {
        if touchPosition.x < 0 {
            //print("left grab button touch began")
            if character.leftHandContacted && allowGrab {
                character.makeHandGrabbingObstacle(hand: .LeftHand)
                HapticResponseController.grabSucceedFeedback()
            }
            leftGrabButtonPressed = true
            character.closeLeftHand()
        } else {
            //print("right grab button touch began")
            if character.rightHandContacted && allowGrab {
                character.makeHandGrabbingObstacle(hand: .RightHand)
                HapticResponseController.grabSucceedFeedback()
            }
            rightGrabButtonPressed = true
            character.closeRightHand()
        }
    }
    
    func buttonTouchMoved(touchPosition: CGPoint) {
        if allowGrab {
            if character.leftHandGrabbing {
                character.leftHandJustGrabbed = false
            }
            if character.rightHandGrabbing {
                character.rightHandJustGrabbed = false
            }
        }
    }
    
    func buttonTouchEnded(touchPosition: CGPoint) {
        if touchPosition.x < 0{
            if character.leftHandGrabbing && allowGrab {
                character.makeHandReleaseFromObstacle(hand: .LeftHand)
            }
            leftGrabButtonPressed = false
            //print("left grab button touch ended")
            character.openLeftHand()
        } else {
            if character.rightHandGrabbing && allowGrab {
                character.makeHandReleaseFromObstacle(hand: .RightHand)
            }
            rightGrabButtonPressed = false
            //print("right grab button touch ended")
            
            character.openRightHand()
        }
    }
    
    // ------------------------------------------------
    // MARK: - frame update
    // ------------------------------------------------
    override func update(_ currentTime: TimeInterval) {
        let vector = GameGyroscopeController.sharedInstance.getlatestRotation()
        character.moveCharacter(vector: vector)
        
        //MARK: Instructions & tab to continue
        if startInstructionTimeCounting {
            instructionReadTime += 1
        }
        if instructionReadTime > 180 {
            stopInstructionTimer()
            instructionReadTime = 0
            showTabToContinue = true
        }
        
        if showTabToContinue {
            tabToCont.isHidden = false
            if (30..<60).contains(frameCounter) {
                tabToCont.isHidden = true
            }
            if (0..<30).contains(frameCounter){
                tabToCont.isHidden = false
            }
            
            frameCounter = (frameCounter + 1) % 60
        } else {
            frameCounter = 0
        }
        
        if startTiltUpTime {
            tiltUpInstrucitonTime += 1
            if tiltUpInstrucitonTime > 120 {
                canReachTiltUp = true
            }
        }
        
        if tiltUpInstruction && character.leftHandFirstLineContact && character.rightHandFirstLineContact {
            HapticResponseController.grabSucceedFeedback()
            
            showText(text: "Good Job!")
            startInstructionTimer()
            showTabToContinue = false
            tiltUpInstruction = false
            
            tiltUpInstrucitonSucceed = true
        }
        
        if firstFlagReached {
            if !gaveHapticFirstFlag {
                showText(text: "Good Job!")
                showText2(text: "Try to reach the next one!")
                HapticResponseController.grabSucceedFeedback()
                gaveHapticFirstFlag = true
            
                //remove first flag and add second flag
                obstacles["firstFlag"]!.removeFromParent()
                if obstacles["secondFlag"]!.parent == nil {
                    addChild(obstacles["secondFlag"]!)
                    secondFlagReached = false
                }
                
                tabCount = 16
            }
        }
        
        if secondFlagReached {
            if !gaveHapticSecondFlag {
                hideGreyBox(side: 0)
                hideGreyBox(side: 1)
                
                showText(text: "Good Job!")
                showText2(text: "")
                HapticResponseController.grabSucceedFeedback()
                gaveHapticSecondFlag = true
                //get rid of the second flag
                obstacles["secondFlag"]!.removeFromParent()
                //hideTapToCont = false
                allowButtonInteraction = false
                allowGrab = false
                startInstructionTimer()
                showTabToContinue = false
                
                character.makeHandReleaseFromObstacle(hand: .LeftHand)
                character.makeHandReleaseFromObstacle(hand: .RightHand)
                character.openRightHand()
                character.openLeftHand()
                
                tabCount = 17
            }
        }
        
        if startZoomOut && zoomOutCounter < 30 {
            camera?.xScale *= 1.01
            camera?.yScale *= 1.01
            zoomOutCounter += 1
        }
        
        if showRedLine {
            if redLineBlinkCounter == 30 {
                obstacles["redLine"]?.isHidden = false
            }
            if redLineBlinkCounter == 0 {
                obstacles["redLine"]?.isHidden = true
            }
            
            redLineBlinkCounter = (redLineBlinkCounter + 1) % 60
        }
        
        if startSwingPractice && convert(character.bodyParts[.LeftUpperArm]!.position, from: character).y < ground!.position.y &&
            convert(character.bodyParts[.RightUpperArm]!.position, from: character).y < ground!.position.y {
            if leftHandCanSwing && rightHandCanSwing && !bodyOnSwingPracticeGround {
                
                hangingSuccesful = true
                
                showText(text: "Now, release one hand to swing the character and")
                showText2(text: "release the grab, to let the character fly")
                
                character.canGetSwingForce = true

                obstacles["redLine"]?.removeFromParent()
                
                startMovingPracticeGround = true
            }
        }
        
        if startMovingPracticeGround && zoomOutCounter < 90 {
            obstacles["swingPractice"]!.position.y *= 1.01
            camera?.position.y -= 1
            zoomOutCounter += 1
            
            if character.leftHandGrabbingAt == obstacles["swingPractice"] {
                character.leftHandGrabbingAt = nil
                character.makeHandReleaseFromObstacle(hand: .LeftHand)
            }
            if character.rightHandGrabbingAt == obstacles["swingPractice"] {
                character.rightHandGrabbingAt = nil
                character.makeHandReleaseFromObstacle(hand: .RightHand)
            }
        }
        
        if landed {
            showText(text: "Good Job!")
            showText2(text: "Now you are ready to play!")
            allowGrab = false
            allowButtonInteraction = true
        }
    }
    
    //MARK: - contact detection - delegate function
    func didBegin(_ contact: SKPhysicsContact) {
        self.checkForContacts(contact)
    }
    
    func didEnd(_ contact: SKPhysicsContact) {
        self.checkForContactsDidEnd(contact)
    }
    
    func checkForContacts(_ contact: SKPhysicsContact) {
        let rightHand = character.bodyParts[.RightHand]
        let leftHand = character.bodyParts[.LeftHand]
        
        if let contactNodeA = contact.bodyA.node,
           let contactNodeB = contact.bodyB.node {
            // check lefthhand with obstacle
            if leftHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                
                // let the character not grab the hidden barrier that lets character not fall
                guard !( contactNodeB == obstacles["leftBarrier"] || contactNodeB == obstacles["rightBarrier"] ) else {
                    return
                }
                
                // in case when user already press the grab button,
                // the grabbing interaction is triggered immediately
                if leftGrabButtonPressed && allowGrab && allowButtonInteraction {
                    character.leftHandContacted = true
                    character.makeHandGrabbingObstacle(hand: .LeftHand)
                    character.leftHandGrabbingAt = contactNodeB as? SKShapeNode
                    HapticResponseController.grabSucceedFeedback()
                    
                    if contactNodeB.name == "ground" {
                        leftHandCanSwing = true
                    }
                }
                
                if contactNodeB.name == "handsUpLine" && canReachTiltUp {
                    character.leftHandFirstLineContact = true
                }
                
                if contactNodeB.name == "firstFlag" {
                    firstFlagReached = true
                }
                
                if contactNodeB.name == "secondFlag" {
                    secondFlagReached = true
                }
                
            } else if leftHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                
                guard !( contactNodeA == obstacles["leftBarrier"] || contactNodeA == obstacles["rightBarrier"] ) else {
                    return
                }
                
                // in case when user already press the grab button,
                // the garbbing interaction is triggered immediately
                if leftGrabButtonPressed && allowGrab && allowButtonInteraction {
                    character.leftHandContacted = true
                    character.makeHandGrabbingObstacle(hand: .LeftHand)
                    HapticResponseController.grabSucceedFeedback()
                    character.leftHandGrabbingAt = contactNodeB as? SKShapeNode
                    
                    if contactNodeA.name == "ground" {
                        leftHandCanSwing = true
                    }
                }
                
                if contactNodeA.name == "handsUpLine" && canReachTiltUp {
                    character.leftHandFirstLineContact = true
                
                }
                
                if contactNodeA.name == "firstFlag" {
                    firstFlagReached = true
                }
                if contactNodeA.name == "secondFlag" {
                    secondFlagReached = true
                }
            }
            
            // check right hand with obstacle
            if rightHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                
                guard !( contactNodeB == obstacles["leftBarrier"] || contactNodeB == obstacles["rightBarrier"] ) else {
                    return
                }
                
                // in case when user already press the grab button,
                // the garbbing interaction is triggered immediately
                if rightGrabButtonPressed && allowGrab && allowButtonInteraction {
                    character.rightHandContacted = true
                    character.makeHandGrabbingObstacle(hand: .RightHand)
                    character.rightHandGrabbingAt = contactNodeB as? SKShapeNode
                    HapticResponseController.grabSucceedFeedback()
                    
                    if contactNodeB.name == "ground" {
                        rightHandCanSwing = true
                    }
                }
                
                if contactNodeB.name == "handsUpLine" && canReachTiltUp {
                    character.rightHandFirstLineContact = true
                }
                
                if contactNodeB.name == "firstFlag" {
                    firstFlagReached = true
                }
                
                if contactNodeB.name == "secondFlag" {
                    secondFlagReached = true
                }

            } else if rightHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                
                guard !( contactNodeA == obstacles["leftBarrier"] || contactNodeA == obstacles["rightBarrier"] ) else {
                    return
                }
                
                // in case when user already press the grab button,
                // the garbbing interaction is triggered immediately
                if rightGrabButtonPressed && allowGrab && allowButtonInteraction {
                    character.rightHandContacted = true
                    character.makeHandGrabbingObstacle(hand: .RightHand)
                    character.rightHandGrabbingAt = contactNodeB as? SKShapeNode
                    HapticResponseController.grabSucceedFeedback()
                    
                    if contactNodeA.name == "ground" {
                        rightHandCanSwing = true
                    }
                }
                
                if contactNodeA.name == "handsUpLine" && canReachTiltUp {
                    character.rightHandFirstLineContact = true
                }
                
                if contactNodeA.name == "firstFlag" {
                    firstFlagReached = true
                }
                
                if contactNodeA.name == "secondFlag" {
                    secondFlagReached = true
                }
            }
        }
            
        if contact.bodyA.node! == character.bodyParts[.Body]! {
            if obstacles.values.contains(contact.bodyB.node!) {
                if contact.bodyB.node!.name == "firstFlag" {
                    firstFlagReached = true
                }
                
                if contact.bodyB.node!.name == "secondFlag" {
                    secondFlagReached = true
                }
                
                if contact.bodyB.node!.name == "swingPractice" {
                    bodyOnSwingPracticeGround = true
                }
                
                if contact.bodyB.node!.name == "swingPractice" && startMovingPracticeGround && hangingSuccesful {
                    landed = true
                }
            }
        } else if contact.bodyB.node! == character.bodyParts[.Body]! {
            if obstacles.values.contains(contact.bodyA.node!) {
                
                if contact.bodyA.node!.name == "firstFlag" {
                    firstFlagReached = true
                }
                
                if contact.bodyA.node!.name == "secondFlag" {
                    secondFlagReached = true
                }
                
                if contact.bodyB.node!.name == "swingPractice" {
                    bodyOnSwingPracticeGround = true
                }
                
                if contact.bodyA.node!.name == "swingPractice" && startMovingPracticeGround && hangingSuccesful {
                    landed = true
                }
            }
        }
        
    }
    
    func checkForContactsDidEnd(_ contact: SKPhysicsContact) {
        let rightHand = character.bodyParts[.RightHand]
        let leftHand = character.bodyParts[.LeftHand]
        
        if let contactNodeA = contact.bodyA.node,
           let contactNodeB = contact.bodyB.node {
            // check lefthhand with obstacle
            if leftHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                character.leftHandContacted = false
                character.leftHandGrabbingAt = nil
                if contactNodeB.name == "handUpLine" {
                    character.leftHandFirstLineContact = false
                }
                leftHandCanSwing = false
                
            } else if leftHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                character.leftHandContacted = false
                character.leftHandGrabbingAt = nil
                if contactNodeA.name == "handUpLine" {
                    character.leftHandFirstLineContact = false
                }
                leftHandCanSwing = false
            }
            
            // check right hand with obstacle
            if rightHand == contactNodeA && obstacles.values.contains(contactNodeB) {
                character.rightHandContacted = false
                character.rightHandGrabbingAt = nil
                if contactNodeB.name == "handUpLine" {
                    character.rightHandFirstLineContact = false
                }
                rightHandCanSwing = false
            } else if rightHand == contactNodeB && obstacles.values.contains(contactNodeA) {
                character.rightHandContacted = false
                character.rightHandGrabbingAt = nil
                if contactNodeA.name == "handUpLine" {
                    character.rightHandFirstLineContact = false
                }
                rightHandCanSwing = false
            }
        }
        
        if contact.bodyA.node! == character.bodyParts[.Body]! {
            if obstacles.values.contains(contact.bodyB.node!) {
                if contact.bodyB.node!.name == "swingPractice" {
                    bodyOnSwingPracticeGround = false
                }
            }
        } else if contact.bodyB.node! == character.bodyParts[.Body]! {
            if obstacles.values.contains(contact.bodyA.node!) {
                if contact.bodyB.node!.name == "swingPractice" {
                    bodyOnSwingPracticeGround = false
                }
            }
        }
    }
    
    
    
    // MARK: - button appearance functions
    func initializeGrabButtons() {
        leftGreyBox = SKShapeNode(rectOf: CGSize(width: self.frame.width / 2, height: self.frame.height))
        rightGreyBox = SKShapeNode(rectOf: CGSize(width: self.frame.width / 2, height: self.frame.height))
        leftGreyBox!.fillColor = .white
        rightGreyBox!.fillColor = .white
        leftGreyBox!.alpha = 0
        rightGreyBox!.alpha = 0
        camera!.addChild(leftGreyBox!)
        camera!.addChild(rightGreyBox!)
        greyBoxLeftPosition = CGPoint(x: -(self.frame.width / 4), y: 0)
        greyBoxRightPosition = CGPoint(x: self.frame.width / 4, y: 0)
        leftGreyBox?.position = greyBoxLeftPosition
        rightGreyBox?.position = greyBoxRightPosition
    }
    
    func showGreyBox(side : Int) {
        if side == 0 {
            if let gb = leftGreyBox {
                gb.alpha = 0.05
            }
        } else {
            if let gb = rightGreyBox {
                gb.alpha = 0.05
            }
        }
    }
    
    func hideGreyBox(side : Int) {
        if side == 0 {
            if let gb = leftGreyBox {
                gb.alpha = 0
            }
        } else {
            if let gb = rightGreyBox {
                gb.alpha = 0
            }
        }
    }
    
    func showText(text : String) {
        //MARK: can't understand width height
        instruction1.text = text
        instruction1.position = convert(CGPoint(x: 0, y: self.frame.width / 4), to: self.camera!)
        instruction1.fontColor = .white
        tabToCont.isHidden = true
    }
    
    func showText2(text : String) {
        //MARK: can't understand width height
        instruction2.text = text
        instruction2.position = convert(CGPoint(x: 0, y: self.frame.width / 4 - instruction1.fontSize * 2), to: self.camera!)
        if camera?.xScale != 2.0 {
            instruction2.position = convert(CGPoint(x: 0, y: self.frame.width / 4 - instruction1.fontSize * 3), to: self.camera!)
        }
        instruction2.fontColor = .white
    }
    
    func startInstructionTimer() {
        startInstructionTimeCounting = true
        canSkipInstruction = false
    }
    
    func stopInstructionTimer() {
        startInstructionTimeCounting = false
        canSkipInstruction = true
    }

}

